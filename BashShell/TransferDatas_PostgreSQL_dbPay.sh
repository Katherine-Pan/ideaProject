#!/bin/sh
FILE="dbPay.py"
LOG_SW=1
START_DATE=$1 #"--startdate 20190420"
END_DATE=$2 #"--enddate 20190422"

export HADOOP_USER_NAME=hdfs

DATE=`date +%Y%m%d`
echo ${DATE}
YEAR=`date +%Y`
echo ${YEAR}
MONTH=`date +%m`
echo ${MONTH}

FILE_HOME="/root/HZTL/Orange/TransferDatas/PostgreSQL/"
echo "`date`----------Run pyspark app ${FILE}"


command="spark-submit --conf spark.blacklist.enabled=false --conf spark.rpc.message.maxSize=256 --jars /home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-common.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-client.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-server.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/jars/spark-examples_2.11-2.4.0-cdh6.1.1.jar --driver-cores 2 --driver-memory 4096M --num-executors 20 --executor-cores 2 --executor-memory 10240M"

#日志文件目录，日志命名方式为文件名+运行日期
parentPath="/root/logs/HZTL/Orange/TransferDatas/PostgreSQL/${YEAR}"
if [ ! -d $parentPath  ];then
  mkdir $parentPath
fi

path=$parentPath/$MONTH
if [ ! -d $path  ];then
  mkdir $path
fi

LOG=" 1>${path}/${FILE}-${DATE}.log 2>&1 "

#判断是否记录日志文件
if [[ ${LOG_SW} -eq "1" ]]
  then
    command+=" "${FILE_HOME}${FILE}" "${START_DATE}" "${END_DATE}" "${LOG}
else
    command+=" "${FILE_HOME}${FILE}" "${START_DATE}" "${END_DATE}
fi

echo "$command"
eval $command