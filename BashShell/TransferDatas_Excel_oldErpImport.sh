#!/bin/sh
FILE="oldErpImport.py"
LOG_SW=1
WEBSERVER=$1
TABLES=$2

export HADOOP_USER_NAME=hdfs

DATE=`date +%Y%m%d`
echo ${DATE}
YEAR=`date +%Y`
echo ${YEAR}
MONTH=`date +%m`
echo ${MONTH}
YESTERDATE=`date +%Y%m%d -d -2day`
echo ${YESTERDATE}

oldPath="/data/ftpdata/5_${YESTERDATE}"*
echo ${oldPath}
newPath="/data/ftpdata/history/"
echo ${newPath}


FILE_HOME="/root/HZTL/Orange/TransferDatas/Excel/"
echo "`date`----------Run pyspark app ${FILE}"


command="spark-submit --conf spark.blacklist.enabled=false --conf spark.rpc.message.maxSize=512 --jars /home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-common.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-client.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-server.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/jars/spark-examples_2.11-2.4.0-cdh6.1.1.jar --driver-cores 8 --driver-memory 8192M --num-executors 20 --executor-cores 8 --executor-memory 10240M"

#日志文件目录，日志命名方式为文件名+运行日期
parentPath="/root/logs/${YEAR}"
if [ ! -d $parentPath  ];then
  mkdir $parentPath
fi

path=$parentPath/$MONTH
if [ ! -d $path  ];then
  mkdir $path
fi

LOG=" 1>${path}/${FILE}-${DATE}.log 2>&1 "
echo ${LOG}


#判断是否记录日志文件
if [[ ${LOG_SW} -eq "1" ]]
  then
    command+=" "${FILE_HOME}${FILE}" "${DATE}" "${WEBSERVER}" "${TABLES}" "${LOG}
else
    command+=" "${FILE_HOME}${FILE}" "${DATE}" "${WEBSERVER}" "${TABLES}
fi

if [ ! -f oldPath  ];then
  echo "文件不存在"
else
  mv ${oldPath} ${newPath}
fi