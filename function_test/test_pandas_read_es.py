from pyspark import SparkConf,HiveContext,SparkContext
from elasticsearch import Elasticsearch
import pandas as pd
#Spark入口
conf = SparkConf().setAppName("将list转化为dataframe")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

def aggWithKey(x,y):
    x.extend(y)
    return x

def get_from_elasticsearch():
    #链接es数据
    es = Elasticsearch(
        ['http://es.hztl3.xyz'],
        port = 80,
    )
    ##读取对应index下数据，query中为查询条件
    esDict = es.search(doc_type ='_doc',index = 'logc-common-kafka-stocks-log.2019.03')#,body={"query":query_json})

    es = Elasticsearch.Elasticsearch(['http://es.hztl3.xyz'], port=80)
    query_data = {
        "query": {
            "bool": {
                "must": {
                    "match": {
                        "path_s.keyword": "/company-parts"
                    }
                }
            }
        }
    }
    res = es.search(
        doc_type='_doc',
        index='logc-production-http-log.*',
        body=query_data,

    )
    print(res)

    return esDict

#将es的dic数据转化为可以转化df的Dict
def esDict_to_dfDict(esDict):
    label_num = '4'  # 总字段数量
    #设置一个嵌套列表
    list_total = []
    for i in range(0, int(label_num)):
        list_total.append([])
    #将esDict数据添加进入嵌套列表
    for hit in esDict['hits']['hits']:
        list.total[0].append(hit['_id'])
        list_total[1].append(hit['_source']['at_s'])
        list_total[2].append(hit['_source']['data']['stocks'])
        list_total[3].append(hit['_source']['data']['companyId_i'])
    #将列表转化为字典，字典key为转化df的column值
    dfDict = {
        "id": list_total[0],
        "at_s": list_total[1],  # at_s,
        "stocks": list_total[2], # env_s
        "companyId_i":list_total[3]
    }
    return dfDict

#将Dict转化为df
def Dict_to_df(dfDict):
    data = pd.DataFrame(dfDict)
    df = sqlContext.createDataFrame(data)
    df.show()
    return df

#将es的Dic数据转化为一个完整的list
def esDict_to_dflist(esDict):
    dflist = []
    for hit in esDict['hits']['hits']:
        dflist.append((hit['_id'],hit['_source']['at_s'],hit['_source']['data']['companyId_i'],hit['_source']['data']['stocks']))
    return dflist

def list_trans_to_rdd(dflist):
    #将数据库结果转化为rdd
    inf_rdd = sc.parallelize(dflist)
    print(type(inf_rdd))
    print(inf_rdd.take(1))
    return inf_rdd

def mapgetabc(p):
    id=p[0]
    at_s=p[1]
    companyId_i=p[2]
    stocks=p[3]
    return id,at_s,companyId_i,stocks


if __name__ == '__main__':
    print("main function")
    esDict = get_from_elasticsearch()
    """
    整理两种方法（主要关键在于将esDict整理成为什么样的list）
    1、将esdict整理成为一个嵌套list，list中每个值为一个column完整的数值，
       然后将此list整理为目标dict，dict的value为一个独立非嵌套list。再从目标dict转化为df
    2、将esdict整理成为一个完整的list，list每个值为一个完整的包含所有column的值
       通过rdd以及map，reduce将list整理成为一个df
    """
    """方法一"""
    #dfDict = esDict_to_dfDict(esDict)
    #df = Dict_to_df(dfDict)

    """方法二"""
    dflist = esDict_to_dflist(esDict)
    inf_rdd = list_trans_to_rdd(dflist)
    # 将rdd转化为df，一一对应字段名
    inf_rdd = inf_rdd.map(mapgetabc)
    df = sqlContext.createDataFrame(inf_rdd, ['id', 'at_s','companyId_i','stocks'])\
        .map(lambda p:((p.id,p.at_s,p.companyId_i),p.stocks))\
        .reduceByKey(aggWithKey)\
        .map(lambda x:(x[0],x[1]))
    df.show()
    print(df.dtypes)
    sc.stop()
    print("program is done")
