import datetime
import numpy as np
import pandas as pd

if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    #十进制转2进制,bNumber是str类型，前两位是符号位
    bNumber = bin(12)[2:]
    print(bNumber)
    print("program is done at: "  + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))