import psycopg2
from pyspark import SparkContext,SparkConf,HiveContext
#spark入口
conf = SparkConf().setAppName("读取postgre数据并转化为dataframe")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)
print("program is starting")

def opendatabase():
    #打开一个链接到PostgreSQL数据库，连接成功返回一个数据对象
    conn = psycopg2.connect(database='accounts_qa',#数据库名
                        user='pgdata',
                        password='Tonglianinfo2018_db',
                        host='113.31.135.250',
                        port='21770')
    print("Opened database successfull")
    return conn

def closedatabase(conn):
    #关闭数据库链接
    conn.close()
    print("Closed database successfull")

def getinfo(conn):
    #创建一个光标，用于整个数据库使用Python编程
    cursor = conn.cursor()
    # select语句
    cursor.execute("select jsonb_out(veh_brands),jsonb_pretty(veh_brands) from customers where id=26")
    info_list = cursor.fetchall()
    print(info_list)
    return  info_list

def list_trans_to_rdd(info_list):
    #将数据库结果转化为rdd
    inf_rdd = sc.parallelize(info_list)
    return inf_rdd

if __name__ == '__main__':
    print("main function")
    conn = opendatabase()
    info_list = getinfo(conn)
    inf_rdd = list_trans_to_rdd(info_list)
    #将rdd转化为df，一一对应字段名
    df = sqlContext.createDataFrame(inf_rdd,['jsonb_col','jsonb_col1'])
    df.show(5,truncate=False)
    print(df.dtypes)
    sc.stop()
    print("program was done")
