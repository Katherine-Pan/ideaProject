from pyspark import SparkContext,SparkConf,HiveContext
import pandas as pd
from pyspark.sql.functions import monotonically_increasing_id

#spark入口
conf = SparkConf().setAppName("Transfer_PostgreSql_AccountsDB")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)


#读取对应csv文件
#encoding='gbk'以gbk编码
#pddf = pd.DataFrame.from_csv('../Files/test.csv',header=None,sep=str(format(0x03,'c'),))
pddf = pd.read_csv('../Files/9_20190530_b_province.csv',
                   sep=str(format(0x03,'c'),),
                   names=['machinenum','PKID','Province','Pzjf','CNo','City','CZjf','Area','AZjf'],
                   keep_default_na=False)
#将pandas的dataframe转化为spark的dataframe
df = sqlContext.createDataFrame(pddf)
#打印结果it
df.where(df.Province=='四川').show(10)

#给df增加列
#添加id列
df = df.withColumn("id",monotonically_increasing_id())