import urllib.request as request
import json
#在python中，首先要用到urllib的包，其次对其进行读取的格式为json。



if __name__ == '__main__':
    #接下来，我们获取相应的路径请求，并用urlopen打开请求的文件
    req = request.Request("http://es.hztl3.xyz/logc-production-http-log.2019.06/_search?q=path_s:company-parts")
    resp = request.urlopen(req)
    #对得到的resp,我们需要用json的格式迭代输出：（注意是字符串类型）
    jsonstr = ""
    for line in resp:
        jsonstr += line.decode()
    data = json.loads(jsonstr)
    print(data)
    print(jsonstr)
