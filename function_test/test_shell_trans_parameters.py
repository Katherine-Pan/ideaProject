import sys

#获取shell传入第一个参数
parameters = str(sys.argv[1])
#打印获取到的参数
print(parameters)

#通过定义名称传递参数
import argparse
PARAMS = None
parser = argparse.ArgumentParser()
parser.add_argument('--flag', type=int, default=0,
                        help='print 0 run 1')
PARAMS, _ = parser.parse_known_args()
print(PARAMS.flag)