import datetime
import pyspark.sql.functions as F
import numpy as np
from pyspark.sql import SparkSession,Window
from pyspark.sql.types import IntegerType, ArrayType, StringType, FloatType, StructField, StructType, MapType

spark = SparkSession \
    .builder \
    .appName("taihuan") \
    .enableHiveSupport() \
    .config("hive.exec.dynamic.partition", "true") \
    .config("hive.exec.dynamic.partition.mode", "nonstrict") \
    .config("hive.exec.max.dynamic.partitions", "10000") \
    .getOrCreate()

spark.sparkContext.setLogLevel('WARN')

# 基础函数
def date_now():
    now = datetime.datetime.now()
    return now.strftime("%Y%m%d")


def date_diff(now_str, d):
    s = datetime.datetime.strptime(now_str, "%Y%m%d")
    diff = datetime.timedelta(days=d)
    return (s+diff).strftime("%Y%m%d")


def fast_join(left_df, right_df, on, how='inner'):
    join_debug = True
    if join_debug:
        lcols, rcols = left_df.columns, right_df.columns
        print("lcols:%s, rcols:%s" % (lcols, rcols))
        ucols = list(set(lcols) & set(rcols) & set(on))
        assert len(ucols) == len(on), "ucols:%s, on:%s" % (len(ucols), len(on))

    n_partitions = 240
    left_df = left_df.repartition(n_partitions, on)
    right_df = right_df.repartition(n_partitions, on)
    return left_df.join(right_df, on, how).cache()


def safe_union(left_df, right_df):
    lcols, rcols = left_df.columns, right_df.columns
    ucols = list(set(lcols) & set(rcols))
    union_debug = True
    if union_debug:
        print("safe_union(): lcols:%s, rcols:%s, ucols:%s" % (lcols, rcols, ucols))
        assert len(lcols) == len(ucols) and len(rcols) == len(ucols), "lcols:%s, rcols:%s, ucols:%s" % (len(lcols), len(rcols), len(ucols))

    return left_df.select(ucols).union(right_df.select(ucols)).cache()

def savehive(df, partitionCols, partitionKV, dbtable):
    # NOTE: 由于平台权限限制不能用CREATE SQL创建表，这里利用了技巧确保表的自动创建
    df.filter('NULL = NULL') \
        .write \
        .mode('append') \
        .partitionBy(partitionCols) \
        .saveAsTable(dbtable)
    # FIXME: 将df分区字段drop掉，INSERT OVERWRITE已经包含了分区字段KV（无奈的做法!!!）
    for col in partitionCols:
        df = df.drop(col)
    df.registerTempTable("_temptable_")
    insertsql = "INSERT OVERWRITE TABLE {0} PARTITION({1}) SELECT * FROM _temptable_".format(dbtable, partitionKV)
    spark.sql(insertsql)
    df.unpersist()

def savehiveToExistTable(df, partitionCols, partitionKV, dbtable):
    for col in partitionCols:
        df = df.drop(col)
    df.registerTempTable("_temptable_")
    insertsql = "INSERT OVERWRITE TABLE {0} PARTITION({1}) SELECT * FROM _temptable_".format(dbtable, partitionKV)
    spark.sql(insertsql)
    df.unpersist()

def main():
    print ("main ....")
    data = [(1,),(2,),(5,)]
    df = spark.createDataFrame(data,['score'])
    df.show()
    cnt = df.rdd.map(lambda p:p.score).reduce(lambda x,y:x+y)
    cnt = df.rdd.reduce(lambda x,y:x.score + y.score)
    print (cnt)
    """
    myList = [
            ['2019-03-01T22:52:03.648499775+08:00',
             {'ArrivalDepot_s': '', 'ECID_s': '',
			  'Parts_o': [{'FactMark_s': '', 'Model_s': '', 'PartNo_s': '4E0 941 158 A'}], 'PayType_s': '现付', 'SalerPlatformId_s': '3'
			 },
			 134.286194,
			 'qa',
			 'info',
			 'qa250',
			 ],
             [
			 '2019-03-01T22:52:03.648499775+08:00',
             {'ArrivalDepot_s': '', 'ECID_s': '',
			  'Parts_o': [{'FactMark_s': '', 'Model_s': '', 'PartNo_s': '4E0 941 158 A'}], 'PayType_s': '现付', 'SalerPlatformId_s': '3'
			 },
			 134.286194,
			 'qa',
			 'info',
			 'qa250',
			 ]
           ]
    df = spark.createDataFrame(myList,['c2','c2','c3','c4','c5','c6'])
    df.show()
    print (df.take(2))
    """

if __name__ == "__main__":
    print("program start run at time:%s" % (datetime.datetime.now().strftime("%Y%m%d %H:%M:%S")))
    main()
    print("program end at time:%s" % (datetime.datetime.now().strftime("%Y%m%d %H:%M:%S")))
