"""
读取在另一个文件夹
Files中otherpythonfile.py
中的a变量，并打印
"""
import sys
sys.path.append(r'../Files')
from otherPythonFile import a as h
print(h)

from otherPythonFile import function as g
print(g(h))