# -*- coding: utf-8 -*-
import sys,time
import datetime
import pandas as pd
import numpy as np
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.feature_extraction import DictVectorizer
reload(sys) 
sys.setdefaultencoding('utf8')
import pickle

# 通过OE号预测其对应的汽车品牌
def oe_new(row):
	z_cnt = 0
	oe_train_str = row.oe_code
	while z_cnt<15-len(row.oe_code):
		oe_train_str = oe_train_str + 'A'
		z_cnt+=1
	return oe_train_str

def accuracy(predict_values, actual):  
    correct = 0  
    for i in range(len(actual)):  
        if actual[i] == predict_values[i]:  
            correct += 1  
    return 'accuracy_score:  ',correct / float(len(actual)) ,correct,len(actual)

def brand_test():
	data_org = pd.read_csv('../hive_test/data/oebrand.csv')
	data_org['oe_new'] = data_org.apply(oe_new,axis=1)
	model_train(data_org,'oe_new','veh_brand')

def category_test():
	def oe_handle(row):
		oe_code = row.oe_code.split('=')[0].split('/')[0]
		oe_code_list = [i.upper() for i in oe_code if i.isalnum()]
		res = ''
		for s in oe_code_list:
			res = res + s
		return res
	def oename_handle(row):
		oename = row.oe_name
		return oename.split('|')[0]
	df_parts_category = pd.read_csv('../hive_test/data/parts_category.csv')
	df_parts_basic = pd.read_csv('../hive_test/data/oe_name_dazhong.csv')
	df_parts_basic['oe_handle'] = df_parts_basic.apply(oe_handle,axis=1)
	df_parts_basic = df_parts_basic[['oe_handle','oe_name']]
	df_parts_basic.rename(columns={'oe_handle':'oe_code'}, inplace = True)
	df_parts_basic['oe_new'] = df_parts_basic.apply(oe_new,axis=1)
	df_parts_basic['name_new'] = df_parts_basic.apply(oename_handle,axis=1)
	df_parts_basic = df_parts_basic[['oe_new','name_new']]
	df_parts_basic.rename(columns={'oe_new':'oe_code','name_new':'oe_name'}, inplace = True)
	df_parts_basic = df_parts_basic.merge(df_parts_category,how='left',left_on='oe_name',right_on='part_name')
	df_parts_basic = df_parts_basic[['oe_code','group_name']]   # group_name  or  sub_group
	model_train(df_parts_basic,'oe_code','group_name')

def model_train(df,pred_dim_name,label_name):
	dic = {'0':'0','1':'1','2':'2','3':'3','4':'4','5':'5','6':'6','7':'7','8':'8','9':'9','A':'10','B':'11','C':'12','D':'13',	'E':'14','F':'15','G':'16','H':'17','I':'18','J':'19','K':'20','L':'21','M':'22','N':'23','O':'24','P':'25','Q':'26','R':'27','S':'28','T':'29','U':'30','V':'31','W':'32','X':'33','Y':'34','Z':'35'}
	train_list = []
	train_label = []
	for row in df.iterrows():
		# print str(row[1][pred_dim_name])
		sub_list = []
		for i in range(len(row[1][pred_dim_name])):
			sub_list.append(dic[str(row[1][pred_dim_name][i].upper())])
		# print sub_list
		train_list.append(sub_list)
		train_label.append(row[1][label_name])
	x_train, x_test, y_train, y_test = train_test_split(train_list, train_label, test_size=0.33, random_state=42)
	# rnd_clf = RandomForestClassifier(n_estimators=21, max_leaf_nodes=651,oob_score=True, n_jobs=-1)   # brand 99.9%
	# rnd_clf = RandomForestClassifier(n_estimators=51, max_leaf_nodes=10001,oob_score=True, n_jobs=-1)   # category 99%
	rnd_clf = RandomForestClassifier(n_estimators=91, max_leaf_nodes=50001,oob_score=True, n_jobs=-1)   # vin 
	rnd_clf.fit(x_train, y_train)
	y_predict_rf = rnd_clf.predict(x_test)
	for i in zip(y_test,y_predict_rf):
		print(i[0],i[1])
	print(accuracy(y_predict_rf,y_test))

def vin_test():
	def vin_handle(row):
		return row.vin[:8]
	df_vin = pd.read_csv('../hive_test/data/vins.txt')
	df_vin['vin_code'] = df_vin.apply(vin_handle,axis=1)
	model_train(df_vin,'vin_code','swept_volume')  # brand,model,swept_volume,transmission

if __name__ == '__main__':
	brand_test()
	# category_test()
	# vin_test()


