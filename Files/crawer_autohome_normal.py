# -*- coding: utf-8 -*-
import re
import sys,time
import urllib3
import datetime,random
import MySQLdb
# import ssl
# ssl._create_default_https_context = ssl._create_unverified_context
reload(sys) 
sys.setdefaultencoding('utf8')


def get_conn():
    db_params = {'host':'127.0.0.1', 'user':'root', 'passwd':'root', 'db':'autohome', 'charset':'utf8'}
    conn = MySQLdb.connect(**db_params)
    return conn

def persist(sql,data=[]):
    conn = get_conn()
    cur = conn.cursor()
    if len(data)==0:
        cur.execute(sql)
        info = cur.fetchall()
    else:
        cur.executemany(sql,data)
        info = True
    cur.close()
    conn.commit()
    conn.close()
    return info

def html_release(url):
    print(url)
    time.sleep(random.randint(1,3))
    user_agent = [
    "mozilla/5.0 (windows nt 6.1; wow64) applewebkit/537.36 (khtml, like gecko) chrome/27.0.1453.94 safari/537.36",
    "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:23.0) Gecko/20100101 Firefox/23.0",
    "Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US; rv:1.9.1.6) Gecko/20091201 Firefox/3.5.6",
    "Mozilla/5.0 (Windows NT 6.2; rv:16.0) Gecko/20100101 Firefox/16.0",
    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.2)",
    "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)",
    "Mozilla/5.0 (compatible; U; ABrowse 0.6; Syllable) AppleWebKit/420+ (KHTML, like Gecko)",
    "Mozilla/5.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser 1.98.744; .NET CLR 3.5.30729)",
    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Acoo Browser; GTB5; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; InfoPath.1; .NET CLR 3.5.30729; .NET CLR 3.0.30618)",
    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 5.1; Trident/4.0; SV1; Acoo Browser; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729; Avant Browser)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; GTB5; Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1) ; InfoPath.1; .NET CLR 3.5.30729; .NET CLR 3.0.30618)",
    "Mozilla/4.0 (compatible; MSIE 7.0; America Online Browser 1.1; Windows NT 5.1; (R1 1.5); .NET CLR 2.0.50727; InfoPath.1)",
    "Mozilla/4.0 (compatible; MSIE 7.0; America Online Browser 1.1; rev1.5; Windows NT 5.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
    "Mozilla/4.0 (compatible; MSIE 6.0; America Online Browser 1.1; Windows NT 5.1; SV1; .NET CLR 1.1.4322; HbTools 4.7.1)",
    "Mozilla/4.0 (compatible; MSIE 6.0; America Online Browser 1.1; Windows NT 5.1; SV1; .NET CLR 1.1.4322)",
    "Mozilla/4.0 (compatible; MSIE 6.0; America Online Browser 1.1; Windows NT 5.1; SV1)",
    "Mozilla/4.0 (compatible; MSIE 8.0; AOL 9.7; AOLBuild 4343.27; Windows NT 5.1; Trident/4.0; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)",
    "Mozilla/4.0 (compatible; MSIE 8.0; AOL 9.7; AOLBuild 4343.19; Windows NT 5.1; Trident/4.0; GTB7.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.4506.2152; .NET CLR 3.5.30729)",
    "Mozilla/4.0 (compatible; MSIE 8.0; AOL 9.6; AOLBuild 4340.12; Windows NT 6.1; Trident/4.0; SLCC2; .NET CLR 2.0.50727; .NET CLR 3.5.30729; .NET CLR 3.0.30729; Media Center PC 6.0)",
    "Mozilla/4.0 (compatible; MSIE 7.0; AOL 9.6; AOLBuild 4340.128; Windows NT 5.1; Trident/4.0; .NET CLR 1.1.4322; .NET CLR 2.0.50727; .NET CLR 3.0.04506.648; .NET CLR 3.5.21022)",
    "Mozilla/4.0 (compatible; MSIE 6.0; AOL 7.0; Windows NT 5.1; Hotbar 4.2.8.0)",
    "Mozilla/5.0 (X11; U; Linux; pt-PT) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3) Arora/0.4",
    "Mozilla/5.0 (X11; U; Linux; en-GB) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3) Arora/0.3 (Change: 239 52c6958)",
    "Mozilla/5.0 (X11; U; Linux; sk-SK) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3) Arora/0.2 (Change: 0 )",
    "Mozilla/5.0 (Windows; U; Windows NT 6.0; de-DE) AppleWebKit/523.15 (KHTML, like Gecko, Safari/419.3) Arora/0.2",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.34 (KHTML, like Gecko) Arora/0.11.0 Safari/534.34",
    "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-MY) AppleWebKit/527+ (KHTML, like Gecko, Safari/419.3) Arora/0.10.0",
    "Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.0; Trident/4.0; Avant Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506; .NET CLR 3.5.21022; InfoPath.2)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Avant Browser; Avant Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506; Tablet PC 2.0)",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Avant Browser; .NET CLR 1.0.3705; .NET CLR 1.1.4322; .NET CLR 2.0.50727)",
    "Mozilla/5.0 (Windows; U; WinNT; en; Preview) Gecko/20020603 Beonex/0.8-stable",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1b2) Gecko/20060821 BonEcho/2.0b2",
    "Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.8.1a2) Gecko/20060512 BonEcho/2.0a2",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.4pre) Gecko/20070416 BonEcho/2.0.0.4pre",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.3pre) Gecko/20070302 BonEcho/2.0.0.3pre",
    "Mozilla/5.0 (Windows; U; Windows NT 5.2; en-GB; rv:1.8.1.2pre) Gecko/20070226 BonEcho/2.0.0.2pre",
    "Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en-US; rv:1.8.1.2) Gecko/20070223 BonEcho/2.0.0.2",
    "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1pre) Gecko/20061203 BonEcho/2.0.0.1pre",
    "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-US; rv:1.8.1.12pre) Gecko/20080103 BonEcho/2.0.0.12pre",
    "Mozilla/5.0 (X11; U; Linux ppc; en-US; rv:1.8.1.1) Gecko/20061219 BonEcho/2.0.0.1",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1) Gecko/20061031 BonEcho/2.0",
    "Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; XH; rv:8.578.498) fr, Gecko/20121021 Camino/8.723+ (Firefox compatible)",
    "Mozilla/5.0 (Macintosh; U; PPC Mac OS X Mach-O; en; rv:1.8.1.4pre) Gecko/20070511 Camino/1.6pre",
    "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.13 (KHTML, like Gecko) Chrome/24.0.1290.1 Safari/537.13",
    "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.6 Safari/537.11",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/536.3 (KHTML, like Gecko) Chrome/19.0.1063.0 Safari/536.3",
    "Mozilla/5.0 (X11; CrOS i686 1193.158.0) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.75 Safari/535.7",
    "Mozilla/5.0 ArchLinux (X11; U; Linux x86_64; en-US) AppleWebKit/534.30 (KHTML, like Gecko) Chrome/12.0.742.100 Safari/534.30",
    "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/534.30 (KHTML, like Gecko) Comodo_Dragon/12.1.0.0 Chrome/12.0.742.91 Safari/534.30",
    "Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Deepnet Explorer 1.5.3; Smart 2x2; .NET CLR 2.0.50727; .NET CLR 1.1.4322; InfoPath.1)",
    "Mozilla/5.0 (X11; U; Linux i686; en; rv:1.8.0.7) Gecko/20060928 Epiphany/2.14 (Ubuntu)",
    "Mozilla/5.0 (X11; U; SunOS 5.11; en-US; rv:1.8.0.2) Gecko/20050405 Epiphany/1.7.1",
    "Mozilla/5.0 (X11; U; Linux i686; pl-pl) AppleWebKit/525.1+ (KHTML, like Gecko, Safari/525.1+) epiphany-browser",
    "Mozilla/5.0 (Windows; U; Windows NT 5.0; de-DE; rv:1.5a) Gecko/20030728 Mozilla Firebird/0.6.1",
    "Mozilla/5.0 (Windows NT 10.0; WOW64; rv:45.66.18) Gecko/20177177 Firefox/45.66.18",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:24.0) Gecko/20100101 Firefox/24.0",
    "Mozilla/5.0 (Windows NT 6.1; rv:21.0) Gecko/20130328 Firefox/21.0",
    "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:17.0) Gecko/20100101 Firefox/17.0.6",
    "Mozilla/5.0 (X11; U; Linux amd64; rv:5.0) Gecko/20100101 Firefox/5.0 (Debian)",
    "Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:2.2a1pre) Gecko/20110208 Firefox/4.2a1pre",
    "Mozilla/5.0 (X11; U; Linux x86_64; fr; rv:1.9.0.11) Gecko/2009060309 Ubuntu/9.04 (jaunty) Firefox/3.0.11",
    "Mozilla/5.0 (X11; U; Linux i686 (x86_64); en-GB; rv:1.8.1.9) Gecko/20071025 Firefox/2.0.0.9",
    "Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.1.7) Gecko/20070914 Firefox/2.0.0.7 (Ubuntu-feisty)"
    ]
    agent = user_agent[random.randint(0,len(user_agent)-1)]
    send_headers = {
 # 'Host':'www.atobo.com.cn',
 'User-Agent':agent,
 'Accept':'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
 'Connection':'keep-alive'
}
    try:
        req = urllib3.Request(url,headers=send_headers)
        data=urllib3.urlopen(req,timeout=30)
        html = data.read()
        # return html
        return html.decode('gb2312','replace').encode(sys.getfilesystemencoding())
    except urllib3.HTTPError as e:
        print(e.getcode())
        return e.getcode()

def year_model_handle(html,series_id):
    turbo_text = html.split('<div class="interval01-title title-cont"><div class="interval01-list-cars"><span')
    series_name_seg = re.findall(r'(?<=target="_blank"><span class="font-bold">).+?(?=</span>)',html)
    series_name = series_name_seg[0]
    turbo_text = turbo_text[1:]
    for t in turbo_text:
        turbo_names = re.findall(r'(?<=class="interval01-list-cars-text">).+?(?=</span></div>)',t)
        year_list = t.split('<div class="interval01-list-cars-infor">')
        year_list = year_list[1:]
        for year in year_list:
            year_model_seg = re.findall(r'(?<=/#pvareaid=2042128").+?(?=<)',year)
            if len(year_model_seg)==0:
                year_model_seg = re.findall(r'(?<=target="_blank">).+?(?=</a></p><p><span class="label label-gray">)',year)
                if len(year_model_seg)==0:
                    # year_model_seg = re.findall(r'(?<=target="_blank">).+?(?=</a></p><p><span class="label label-gray">)',year)  
                    year_model_seg = re.findall(r'(?<=target="_blank">).+?(?=</a></p><p>)',year) 
            engine_structure_list = re.findall(r'(?<=</p><p><span).+?(?=</span>)',year)
            transmission_list = re.findall(r'(?<=</span><span>).+?(?=</span></p></div>)',year)  
            engine_structure,transmission = '','' 
            if len(transmission_list)>0 and len(engine_structure_list)>0:
                if len(transmission_list[0])<50 and len(transmission_list[0])>0 and len(engine_structure_list[0])<50:
                    if '</span><span>' in transmission_list[0]:
                        engine_structure,transmission = transmission_list[0].split('</span><span>')
                    else:
                        engine_structure,transmission = engine_structure_list[0].replace('>',''),transmission_list[0]
            year_model = year_model_seg[0].split('>')[-1]
            check_sql = "select 1 from year_model where series_id='{0}' and turbo_info='{1}' and year_model='{2}'".format(series_id,turbo_names[0],year_model)
            # print check_sql
            checked = persist(check_sql)
            if len(checked)==0:
                SQL = "insert into year_model(series_id,series_name,turbo_info,year_model,engine_structure,transmission) values('{0}','{1}','{2}','{3}','{4}','{5}')".format(series_id,series_name,turbo_names[0],year_model,engine_structure,transmission)
                print(SQL)
                persist(SQL)
    persist("update series set grabed=1 where series_id='"+str(series_id)+"'")
    
def year_model_geter():
    SQL = "select distinct series_id from series where grabed is null"
    series_list = persist(SQL)
    # 有中分隔符的车系表示全车系都停售，没有中分隔符的还要循环检查一下是否有车型
    take_list = []
    if len(series_list)<300:
        for s in range(len(series_list)):
            take_list.append(series_list[s][0])
    else:
        sp = int(str(int(time.time()))[-1])
        for s in range(len(series_list)):
            if s%sp==0:
                take_list.append(series_list[s][0])
    print(take_list)
    for series_id in take_list:
        url = "https://car.autohome.com.cn/price/series-"
        if '-' in series_id:
            page = 1
            url = 'https://car.autohome.com.cn/price/series-' + str(series_id)+'-0-0-0-0-'+str(page)+'.html'
            html = html_release(url)
            year_model_handle(html,series_id)
            while """.html">下一页</a></div></div>""" in html:
                page = page + 1
                url = 'https://car.autohome.com.cn/price/series-' + str(series_id)+'-0-0-0-0-'+str(page)+'.html'
                html = html_release(url)
                year_model_handle(html,series_id)
        else:
            for types in [1,2,3]:
                page = 1
                url = 'https://car.autohome.com.cn/price/series-' + str(series_id)+'-0-'+str(types)+'-0-0-0-0-'+str(page)+'.html'
                html = html_release(url)
                year_model_handle(html,series_id)
                while """.html">下一页</a></div></div>""" in html:
                    page = page + 1
                    url = 'https://car.autohome.com.cn/price/series-' + str(series_id)+'-0-'+str(types)+'-0-0-0-0-'+str(page)+'.html'
                    html = html_release(url)
                    year_model_handle(html,series_id)


def series_geter(brand_id=36,brand_name='奔驰'):
    for types in [1,2,3]:
        page = 1
        url = 'https://car.autohome.com.cn/price/brand-'+str(brand_id)+'-0-'+str(types)+'-'+str(page)+'.html'
        # url = 'https://car.autohome.com.cn/price/brand-36-0-0-3.html'
        html = html_release(url)
        has_nextpage = series_handle(html)
        while has_nextpage:
            page = page + 1
            url = 'https://car.autohome.com.cn/price/brand-'+str(brand_id)+'-0-'+str(types)+'-'+str(page)+'.html'
            html = html_release(url)
            has_nextpage = series_handle(html)

def series_handle(html):
        res = []
        if html:
            # year_model_geter(html)
            seg = re.findall(r'(?<=<div class="uibox-title">).+?(?=</dl></div>)',html)
            brand_seg =  re.findall(r'(?<=<a href="/price/brand-).+?(?=</a></div></dd>)',seg[0]) 
            brand_info = re.findall(r'(?<=).+?(?=</a></h2></div>)',brand_seg[0]) 
            brand_id,brand_name = brand_info[0].split('.html">')
            for brand in brand_seg:
                factory_seg_id = re.findall(r'.+?(?=.html)',brand)
                factory_seg_name = re.findall(r'(?<=">).+?(?<=</a></dt>)',brand)
                # 抓取厂牌
                factory_seg_name_list = re.findall(r'(?<=pvareaid=2042205">).+?(?=</a><)',brand)
                factory_name = factory_seg_name_list[0]
                # 抓取车系
                series_ids = re.findall(r'(?<=<a href="/price/series-).+?(?=.html#)',brand)
                series_names = re.findall(r'(?<=title=").+?(?=">)',brand)
                for i in range(len(series_ids)):
                    # print brand_id,brand_name,factory_name,series_ids[i],series_names[i]
                    check_sql = "select 1 from series where series_id={0}".format(series_ids[i])
                    checked = persist(check_sql)
                    if len(checked)==0:
                        brand_id = str(brand_id).split('-')[0]
                        SQL = "insert into series(brand_id,brand_name,factory_name,series_id,series_name) values('{0}','{1}','{2}','{3}','{4}')".format(brand_id,brand_name,factory_name,series_ids[i],series_names[i])
                        print(SQL)
                        persist(SQL)
            if """.html">下一页</a></div></div>""" in html:
                return True
            else:
                return False
        else:
            return False

def brand_geter():
    url = "https://car.autohome.com.cn/price/brand-33.html"
    html=html_release(url)
    brands = re.findall(r'(?<=/price/list-0-0-0-0-0-0-0-0-).+?(?=</a>)',html)
    brand_ids = []
    dict_brand = {}
    for brand in brands:
        brand_seg = brand.split('-0-0-0-0-0-0-1.html">')
        if len(brand_seg)==2:
            if int(brand_seg[0])>0:
                brand_id,brand_name = brand_seg[0],brand_seg[1]
                brand_ids.append(brand_id)
                dict_brand[brand_id] = brand_name
    brand_ids_existed_org = persist("select distinct brand_id from series")
    brand_ids_existed=[]
    for b in brand_ids_existed_org:
        brand_ids_existed.append(str(b[0]))
    brand_id_new = set(brand_ids) - set(brand_ids_existed)
    print(brand_id_new)
    for brand_id in brand_id_new:
        series_geter(brand_id,dict_brand[brand_id])
        # time.sleep(1)

if __name__ == "__main__":
    """
    # brand_geter()
    # year_model_geter()
    # url = 'https://car.autohome.com.cn/price/series-364-0-0-0-0-1.html'
    # html = html_release(url)
    # year_model_handle(html,series_id)
    """
