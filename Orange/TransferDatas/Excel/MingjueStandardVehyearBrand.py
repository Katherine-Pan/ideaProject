'''
by:pkh
此代码用于读取csv数据中数据并转化存储于hive表中
分区为版本：静态数据
目前为2019-1
'''
from pyspark import SparkContext,SparkConf,HiveContext
from pyspark.sql.functions import *
import pandas as pd
import datetime

#spark入口
conf = SparkConf().setAppName("TransferDatas_Excel_明觉标准汽车年款.csv")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

'''
是否是测试数据库:
1：正常环境
2：测试环境
'''
testdatabase = 1

"""
读取csv文件数据相关参数 = {Dict}
"""
csvInfo = {
    'csvFilePath' : '/root/HZTL/Files/MingjueStandardVehyearBrand.csv',#csv文件路径
    'hiveTableTest' : ['ODS_Csv_MingjueStandardVehyearBrand_Test','orange_test'], # 测试表，测试库名
    'hiveTable' : ['ODS_Csv_MingjueStandardVehyearBrand','orange'], # 正式表，正式库名
    'hiveColName' : "(brand STRING,sub_brand STRING,mj_vehicle_sys STRING,displacement STRING,transsmission STRING,year INT)",#存入hive列名
    'version' : "2019-1"
}

"""
读取csv文件数据并转化为dataframe
Param：csv文件路径={str}
Return: csvDf={dataframe}
"""
def read_csv(csvFilePath):
    #读取对应csv文件
    pdDf = pd.read_csv(csvFilePath,encoding='gbk')
    #将pandas的dataframe转化为spark的dataframe
    csvDf = sqlContext.createDataFrame(pdDf)
    return csvDf

"""
存为hive表
Param:  hive表名={Str}
        分区日期字符串={Str}
        需要存储的df={DataFrame}
        hive表内完整字段名及类型={Str}
        hive库名={Str}
"""
def saveHiveDatabase(hiveTableName,version,df,hiveTableColName,databasename):
    print("start save:" + hiveTableName)
    # 设置动态分区
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
    # 动态分区上线设置
    sqlContext.setConf("hive.exec.max.dynamic.partitions", "10000")
    # 使用库
    sqlContext.sql("use " + databasename)
    # 表若不存在新建Hive表
    sqlContext.sql("CREATE TABLE IF NOT EXISTS " + hiveTableName + hiveTableColName + " PARTITIONED BY (version STRING)")
    # 增加日期分区字段
    dfTemp = df.withColumn("version", lit(version))
    #存为临时表
    dfTemp.registerTempTable("tempTable")
    #从临时表读取数据存入hive
    sqlContext.sql("INSERT OVERWRITE TABLE " + hiveTableName + " PARTITION(version) select * from tempTable")
    print("Saving data to " + hiveTableName + " was done!")

#主函数
if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    csvDf = read_csv(csvInfo['csvFilePath'])
    if testdatabase == 1:
        saveHiveDatabase(csvInfo['hiveTable'][0],csvInfo['version'],csvDf,csvInfo['hiveColName'],csvInfo['hiveTable'][1])
    else:
        saveHiveDatabase(csvInfo['hiveTableTest'][0],csvInfo['version'],csvDf,csvInfo['hiveColName'],csvInfo['hiveTableTest'][1])
    sc.stop()
    print("program is done at: "  + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))