oldErpImport_colList = {
    #分店信息表
    'b_corp':['customermark','CorpID','parentID','CorpNo','CorpName',
              'CorpShortName','CorpheadName','TelNo','Mobile','Addr',
              'Remarks','FaxNo','ZipCode','OrderModule','SaleModle',
              'HsCorp','CorpMark','SelfMark','VDisCount','UnDisCountPart',
              'AllotPriceType','corpLogo','CorpType','LicenceCode','OpeningLicenceCode',
              'CreditCode','LicencesPic','CorpheadName','TelNo','LinkMan2',
              'phone2','LinkMan3','Phone3','Email','Wechat',
              'Website','PlatFormID','SRAreaID'],
    #库房表
    'b_depot':['customermark','PKID','CorpID','DepotNo','DepotName',
               'IsUse','Zjf','SortBySale','SortByExport','PrinterID'],
    #货位表
    'b_wareplace':['customermark','PKID','Depot','Ware','sWareProperty'],
    #员工信息表
    'b_power':['customermark','LoginId','CorpID','sPerCode','sPerName',
               'sDuty','sSex','dBirthday','mSalary','sDegree',
               'sPapers','sAddr','sTel','sMobile','dEmploy',
               'Pwd','FlowNo','Type','MenuStr','DataStr',
               'AuthorStr','DepotStr','CorpStr','bSys','bOperate',
               'bSales','IsUse','fDiscount','PriceType','fScale',
               'VenCorpStr','MgeDepotStr','FinanceCorpStr','Department','WorkGroup',
               'WorkClass','Technical','Quotale','bWorks','fPartScale',
               'fWorkDiscount','fPartDiscount','RoleStr','Zjf','TfDisCount',
               'bLowIprc','bAccredit','bWordchecker','PlatFormUserID','bCorpSys'],
    #基本资料种类（共享表）
    'b_baseconn':['customermark','PKID','BaseName','BaseType','BaseClass',
                  'MaxLength'],
    #基本资料种类详细信息表(共享表)
    'b_baseinfo':['customermark','PKID','IType','Name','Zjf',
                  'SysBZ','Orposi','Remarks','DisCount','sNo'],
    #省市字典
    'b_province':['customermark','PKID','Province','Pzjf','CNo',
                  'City','CZjf','Area','AZjf'],
    #配件资料表
    'b_part':['customermark','PartInno','PartNo','NameC','NameE',
              'Zjf','Model','Unit','Stype','Factory',
              'PartType','PartNo_A','UnitCode','FactMark','LotSize',
              'Weight','Lenth','Width','Height','InCub',
              'Ctjjb','Uqty','CurType','Sengineno','ToteMark',
              'DiscountMark','NoorderMark','MainLand','Prices','BargainPrice',
              'Iprc','Oprc','Prices_O','Prices_N','MarketPrc',
              'RmbPrc1','RmbPrc2','Prices2','TaxRate','TaxNum',
              'MoneyKind1','MoneyKind2','Part_Th','Part_Tx','BarCode',
              'TopNum','BotNum','CalcBottom','Wbh','VendorInno',
              'Remarks','ModiVersion','FixPriceMark','FixPrice','CarSeries',
              'WrtPeriod','Integral','ConsumeQty','UseIntegral','ChangeQty',
              'PartSort','FixRate','Brand','FixTRate','FixOprc',
              'bAdd','Define1','Define2','Define3','Define4',
              'Define5','Define6','Define7','Define8','Define9',
              'Define10','FormatPartNo','sPartimageUrl','OECode','OEName',
              'StandardName','CarBrand','Similarity','bToPlatForm'],
    #互换码表
    'b_pr_nno':['customermark','PKID','Exta_Code','PartInno','ModiVersion'],
    #库存表
    'p_stock':['customermark','PKID','PurchaseNo','Partinno','Depot',
               'Ware','Qty','Vqty','Iprc','Oprc',
               'Oprp','Oprd','Fdate','modiversion','TransetPrc',
               'Remarks','Oprp1','Oprp2','Oprp3','Oprp4',
               'Niprc','Oprs','Vendorinno','Wrtperiod','Stockflags',
               'Originno','Sorderpkid','CIFprc','TExRate','CExRate',
               'TTaxRate','TariffCurr','AddTaxCurr'],
    #需求表
    'p_require':['customermark','PKID','CorpID','PartInno','Reqdate',
                 'Reqqty','Purchasetype','Purchaseno','Operator','Vennamec',
                 'Remarks','Ordered','Modiversion'],
    #采购单主表
    'p_receipt':['customermark','PurchaseID','PurchaseNo','CorpID','PurchaseDate',
                 'PurchaseType','Vendorinno','Privi','PayCode','InvoiceCode',
                 'InvoiceNo','ManiputeCode','sumQty','sumCur','sumCb',
                 'sumTax','settleQty','settleCurr','Operator','Receiver',
                 'ConfirmDate','Remarks','VeriMark','Status','sumTranset',
                 'InTranset','PackNo','TransNo','sendType','sumDisCur',
                 'sumDisCb','CutCur','sumOriCur','sumOriCb','PrintCount',
                 'RepairBy','OutPriceType','ACurr','ZCurr','InvoiceCurr',
                 'InputQty','VeriDate','ModiVersion','JJReceiver','JJconfirmDate',
                 'RetSettleQty','RetSettleCurr','LastPaidDate','Verifier'],
    #采购单细表
    'p_receipt_sub':['customermark','PKID','PurchaseID','PartInno','Depot',
                     'Ware','Qty','Iprc','NIprc','TaxRate',
                     'SettleQty','SettleCurr','Oprc','Oprp','Oprd',
                     'ContractID','Fpje','RetQty','RetFromPurchaseID','InputNo',
                     'Remarks','TransetPrc','RetFromPKID','Oprp1','Oprp2',
                     'Oprp3','Oprp4','CbPrc','DisIprc','DisNiprc',
                     'DisCount','wrtPeriod','OriIprc','OriNiprc','CbNIprc',
                     'StockFlags','InputQty','Oprs','CIFprc','TExRate',
                     'CExRate','TTaxRate','TariffCurr','AddTaxCurr','RetSettleQty',
                     'RetSettleCurr','ProductNo','JJPKID','sWareProperty','PTPrice'],
    #销售表
    'p_sell':['customermark','PurchaseID','Purchaseno','CorpID','Purchasedate',
              'Purchasetype','Privi','Vendorinno','Paycode','Invoicecode',
              'Invoiceno','maniputecode','Packno','Transno','SumTrans',
              'SumQy','SumCurr','SumVCurr','SumhCb','SumTax',
              'SumCurcut','SettleQty','SettleCurr','CutRate','RepairNo',
              'RepairBy','Operator','Receiver','ConfirmDate','Remarks',
              'VeriMark','Status','SendType','InTransact','SumIntegral',
              'FareCurr','PaidCurr','LogNo','SumHCb1','SumDiscur',
              'CutCurr','Zcurr','SumNcur','Accredit','Cardno',
              'SumOricur','SumNCurCut','bVoucher','Vdiscount','Outputqty',
              'Veridate','InvoiceCurr','Accreditdate','RemoteOrderNo','VAcurr',
              'ModiVersion','Tertiary'],
    #销售细表
    'p_sell_sub':['customermark','PKID','PurchaseID','Partinno','Depot',
                  'Ware','S_stype','Qty','Oprc','Oprccl',
                  'Cbprc','TaxRate','SettleQty','SettleCurr','Fpje',
                  'NegMark','ContractID','RetQty','RetfrompurchaseID','InPutNo',
                  'Jjno','Prc_Way_No','GroupNo','SeqNo','Remarks',
                  'TransetPrc','WarnLevel','bPrompt','PmtPartinno','SaleOrderID',
                  'FuturesMark','RetPKID','CbNiprc','StockFlags','Disoprc',
                  'Noprc','Orioprc','Bvoucher','Vdiscount','OutputQty',
                  'JJCbPrc','OrderingID','CIFprc','TExRate','CExRate',
                  'TTaxRate','TariffCurr','AddTaxCurr','Tertiary']
}

oldErpImport_hiveCol={
#分店信息表
    'b_corp':"(customermark      STRING,"
             "CorpID             INT,"
             "parentID           INT,"
             "CorpNo             STRING,"
             "CorpName           STRING,"
             "CorpShortName      STRING,"
             "CorpheadName       STRING,"
             "TelNo              STRING,"
             "Mobile             STRING,"
             "Addr               STRING,"
             "Remarks            STRING,"
             "FaxNo              STRING,"
             "ZipCode            STRING,"
             "OrderModule        STRING,"
             "SaleModule         STRING,"
             "HsCorp             INT,"
             "CorpMark           STRING,"
             "SelfMark           STRING,"
             "VDisCount          DECIMAL,"
             "UnDisCountPart     STRING,"
             "AllotPriceType     INT,"
             "corpLogo           STRING,"
             "CorpType           STRING,"
             "LicenceCode        STRING,"
             "OpeningLicenceCode STRING,"
             "CreditCode         STRING,"
             "LicencesPic        STRING,"
             "LinkMan1           STRING,"
             "Phone1             STRING,"
             "LinkMan2           STRING,"
             "Phone2             STRING,"
             "LinkMan3           STRING,"
             "Phone3             STRING,"
             "Email              STRING,"
             "Wechat             STRING,"
             "Website            STRING,"
             "PlatFormID         Int,"
             "SRAreaID           Int)",
    #库房表
    'b_depot':"(customermark STRING,"
              "PKID          INT,"
              "CorpID        INT,"
              "DepotNo       STRING,"
              "DepotName     STRING,"
              "IsUse         STRING,"
              "Zjf           STRING,"
              "SortBySale    INT,"
              "SortByExport  INT,"
              "PrinterID     INT)",
    #货位表
    'b_wareplace':"(customermark STRING,"
                  "PKID          INT,"
                  "Depot         STRING,"
                  "Ware          STRING,"
                  "sWareProperty STRING)",
    #员工信息表
    'b_power':"(customermark  STRING,"
              "LoginID        INT,"
              "CorpID         INT,"
              "sPerCode       STRING,"
              "sPerName       STRING,"
              "sDuty          STRING,"
              "sSex           STRING,"
              "dBirthday      STRING,"
              "mSalary        DECIMAL,"
              "sDegree        STRING,"
              "sPapers        STRING,"
              "sAddr          STRING,"
              "sTel           STRING,"
              "sMobile        STRING,"
              "dEmploy        STRING,"
              "Pwd            STRING,"
              "FlowNo         INT,"
              "Type           INT,"
              "MenuStr        STRING,"
              "DataStr        STRING,"
              "AuthorStr      STRING,"
              "DepotStr       STRING,"
              "CorpStr        STRING,"
              "bSys           STRING,"
              "bOperate       STRING,"
              "bSales         STRING,"
              "IsUse          STRING,"
              "fDiscount      DECIMAL,"
              "PriceType      STRING,"
              "fScale         DECIMAL,"
              "VenCorpStr     STRING,"
              "MgeDepotStr    STRING,"
              "FinanceCorpStr STRING,"
              "Department     STRING,"
              "WorkGroup      STRING,"
              "WorkClass      STRING,"
              "Technical      STRING,"
              "Quotale        DECIMAL,"
              "bWorks         STRING,"
              "fPartScale     DECIMAL,"
              "fWorkDiscount  DECIMAL,"
              "fPartDiscount  DECIMAL,"
              "RoleStr        STRING,"
              "Zjf            STRING,"
              "TfDisCount     DECIMAL,"
              "bLowIprc       STRING,"
              "bAccredit      STRING,"
              "bWordchecker   STRING,"
              "PlatFormUserID INT,"
              "bCorpSys       STRING)",
    #基本资料种类（共享表）
    'b_baseconn':"(customermark STRING,"
                 "PKID          INT,"
                 "BaseName      STRING,"
                 "BaseType      INT,"
                 "BaseClass     STRING,"
                 "MaxLength     INT)",
    #基本资料种类详细信息表(共享表)
    'b_baseinfo':"(customermark STRING,"
                 "PKID          INT,"
                 "IType         INT,"
                 "Name          STRING,"
                 "Zjf           STRING,"
                 "SysBZ         INT,"
                 "Orposi        DECIMAL,"
                 "Remarks       STRING,"
                 "DisCount      DECIMAL,"
                 "sNo           STRING)",
    #省市字典
    'b_province':"(customermark STRING,"
                 "PKID          INT,"
                 "Province      STRING,"
                 "Pzjf          STRING,"
                 "CNo           STRING,"
                 "City          STRING,"
                 "CZjf          STRING,"
                 "Area          STRING,"
                 "AZjf          STRING)",
    #配件资料表
    'b_part':"(customermark STRING,"
             "PartInno      INT,"
             "PartNo        STRING,"
             "NameC         STRING,"
             "NameE         STRING,"
             "Zjf           STRING,"
             "Model         STRING,"
             "Unit          STRING,"
             "Stype         STRING,"
             "Factory       STRING,"
             "PartType      STRING,"
             "PartNo_A      STRING,"
             "UnitCode      STRING,"
             "FactMark      STRING,"
             "LotSize       DECIMAL,"
             "Weight        DECIMAL,"
             "Lenth         DECIMAL,"
             "Width         DECIMAL,"
             "Height        DECIMAL,"
             "InCub         DECIMAL,"
             "Ctjjb         STRING,"
             "Uqty          DECIMAL,"
             "CurType       STRING,"
             "Sengineno     STRING,"
             "ToteMark      STRING,"
             "DiscountMark  STRING,"
             "NoorderMark   STRING,"
             "MainLand      STRING,"
             "Prices        DECIMAL,"
             "BargainPrice  DECIMAL,"
             "Iprc          DECIMAL,"
             "Oprc          DECIMAL,"
             "Prices_O      DECIMAL,"
             "Prices_N      DECIMAL,"
             "MarketPrc     DECIMAL,"
             "RmbPrc1       DECIMAL,"
             "RmbPrc2       DECIMAL,"
             "Prices2       DECIMAL,"
             "TaxRate       DECIMAL,"
             "TaxNum        STRING,"
             "MoneyKind1    STRING,"
             "MoneyKind2    STRING,"
             "Part_Th       STRING,"
             "Part_Tx       STRING,"
             "BarCode       STRING,"
             "TopNum        DECIMAL,"
             "BotNum        DECIMAL,"
             "CalcBottom    DECIMAL,"
             "Wbh           STRING,"
             "VendorInno    INT,"
             "Remarks       STRING,"
             "ModiVersion   STRING,"
             "FixPriceMark  STRING,"
             "FixPrice      STRING,"
             "CarSeries     STRING,"
             "WrtPeriod     INT,"
             "Integral      INT,"
             "ConsumeQty    INT,"
             "UseIntegral   INT,"
             "ChangeQty     DECIMAL,"
             "PartSort      STRING,"
             "FixRate       DECIMAL,"
             "Brand         STRING,"
             "FixTRate      DECIMAL,"
             "FixOprc       DECIMAL,"
             "bAdd          STRING,"
             "Define1       STRING,"
             "Define2       STRING,"
             "Define3       STRING,"
             "Define4       STRING,"
             "Define5       STRING,"
             "Define6       STRING,"
             "Define7       STRING,"
             "Define8       STRING,"
             "Define9       STRING,"
             "Define10      STRING,"
             "FormatPartNo  STRING,"
             "sPartimageUrl STRING,"
             "OECode        STRING,"
             "OEName        STRING,"
             "StandardName  STRING,"
             "CarBrand      STRING,"
             "Similarity    STRING,"
             "bToPlatForm   STRING)",
    #互换码表
    'b_pr_nno':"(customermark STRING,"
               "PKID          INT,"
               "Exta_Code     INT,"
               "PartInno      INT,"
               "ModiVersion   STRING)",
    #库存表
    'p_stock':"(customermark STRING,"
              "PKID          INT,"
              "PurchaseNo    STRING,"
              "Partinno      INT,"
              "Depot         STRING,"
              "Ware          STRING,"
              "Qty           DECIMAL,"
              "Vqty          DECIMAL,"
              "Iprc          DECIMAL,"
              "Oprc          DECIMAL,"
              "Oprp          DECIMAL,"
              "Oprd          DECIMAL,"
              "Fdate         STRING,"
              "modiversion   STRING,"
              "TransetPrc    DECIMAL,"
              "Remarks       STRING,"
              "Oprp1         DECIMAL,"
              "Oprp2         DECIMAL,"
              "Oprp3         DECIMAL,"
              "Oprp4         DECIMAL,"
              "Niprc         DECIMAL,"
              "Oprs          DECIMAL,"
              "Vendorinno    INT,"
              "Wrtperiod     INT,"
              "Stockflags    STRING,"
              "Originno      STRING,"
              "Sorderpkid    INT,"
              "CIFprc        DECIMAL,"
              "TExRate       DECIMAL,"
              "CExRate       DECIMAL,"
              "TTaxRate      DECIMAL,"
              "TariffCurr    DECIMAL,"
              "AddTaxCurr    DECIMAL)",
    #需求表
    'p_require':"(customermark STRING,"
                "PKID          INT,"
                "CorpID        INT,"
                "PartInno      INT,"
                "Reqdate       STRING,"
                "Reqqty        DECIMAL,"
                "Purchasetype  STRING,"
                "Purchaseno    STRING,"
                "Operator      STRING,"
                "Vennamec      STRING,"
                "Remarks       STRING,"
                "Ordered       INT,"
                "Modiversion   STRING)",
    #采购单主表
    'p_receipt':"(customermark   STRING,"
                "PurchaseID      INT,"
                "PurchaseNo      STRING,"
                "CorpID          INT,"
                "PurchaseDate    STRING,"
                "PurchaseType    STRING,"
                "Vendorinno      INT,"
                "Privi           DECIMAL,"
                "PayCode         STRING,"
                "InvoiceCode     STRING,"
                "InvoiceNo       STRING,"
                "ManiputeCode    STRING,"
                "sumQty          DECIMAL,"
                "sumCur          DECIMAL,"
                "sumCb           DECIMAL,"
                "sumTax          DECIMAL,"
                "settleQty       DECIMAL,"
                "settleCurr      DECIMAL,"
                "Operator        STRING,"
                "Receiver        STRING,"
                "ConfirmDate     STRING,"
                "Remarks         STRING,"
                "VeriMark        INT,"
                "Status          INT,"
                "sumTranset      DECIMAL,"
                "InTranset       INT,"
                "PackNo          STRING,"
                "TransNo         STRING,"
                "sendType        STRING,"
                "sumDisCur       DECIMAL,"
                "sumDisCb        DECIMAL,"
                "CutCur          DECIMAL,"
                "sumOriCur       DECIMAL,"
                "sumOriCb        DECIMAL,"
                "PrintCount      INT,"
                "RepairBy        STRING,"
                "OutPriceType    INT,"
                "ACurr           DECIMAL,"
                "ZCurr           DECIMAL,"
                "InvoiceCurr     DECIMAL,"
                "InputQty        DECIMAL,"
                "VeriDate        STRING,"
                "ModiVersion     STRING,"
                "JJReceiver      STRING,"
                "JJconfirmDate   STRING,"
                "RetSettleQty    DECIMAL,"
                "RetSettleCurr   DECIMAL,"
                "LastPaidDate    STRING,"
                "Verifier        STRING)",
    #采购单细表
    'p_receipt_sub':"(customermark      STRING,"
                    "PKID               INT,"
                    "PurchaseID         INT,"
                    "PartInno           INT,"
                    "Depot              STRING,"
                    "Ware               STRING,"
                    "Qty                DECIMAL,"
                    "Iprc               DECIMAL,"
                    "NIprc              DECIMAL,"
                    "TaxRate            DECIMAL,"
                    "SettleQty          DECIMAL,"
                    "SettleCurr         DECIMAL,"
                    "Oprc               DECIMAL,"
                    "Oprp               DECIMAL,"
                    "Oprd               DECIMAL,"
                    "ContractID         INT,"
                    "Fpje               DECIMAL,"
                    "RetQty             DECIMAL,"
                    "RetFromPurchaseID  INT,"
                    "InputNo            STRING,"
                    "Remarks            STRING,"
                    "TransetPrc         DECIMAL,"
                    "RetFromPKID        INT,"
                    "Oprp1              DECIMAL,"
                    "Oprp2              DECIMAL,"
                    "Oprp3              DECIMAL,"
                    "Oprp4              DECIMAL,"
                    "CbPrc              DECIMAL,"
                    "DisIprc            DECIMAL,"
                    "DisNiprc           DECIMAL,"
                    "DisCount           DECIMAL,"
                    "wrtPeriod          INT,"
                    "OriIprc            DECIMAL,"
                    "OriNiprc           DECIMAL,"
                    "CbNIprc            DECIMAL,"
                    "StockFlags         STRING,"
                    "InputQty           DECIMAL,"
                    "Oprs               DECIMAL,"
                    "CIFprc             DECIMAL,"
                    "TExRate            DECIMAL,"
                    "CExRate            DECIMAL,"
                    "TTaxRate           DECIMAL,"
                    "TariffCurr         DECIMAL,"
                    "AddTaxCurr         DECIMAL,"
                    "RetSettleQty       DECIMAL,"
                    "RetSettleCurr      DECIMAL,"
                    "ProductNo          STRING,"
                    "JJPKID             INT,"
                    "sWareProperty      STRING,"
                    "PTPrice            DECIMAL)",
    #销售表
    'p_sell':"(customermark  STRING,"
             "PurchaseID     INT,"
             "Purchaseno     STRING,"
             "CorpID         INT,"
             "Purchasedate   STRING,"
             "Purchasetype   STRING,"
             "Privi          DECIMAL,"
             "Vendorinno     INT,"
             "Paycode        STRING,"
             "Invoicecode    STRING,"
             "Invoiceno      STRING,"
             "maniputecode   STRING,"
             "Packno         STRING,"
             "Transno        STRING,"
             "SumTrans       DECIMAL,"
             "SumQy          DECIMAL,"
             "SumCurr        DECIMAL,"
             "SumVCurr       DECIMAL,"
             "SumhCb         DECIMAL,"
             "SumTax         DECIMAL,"
             "SumCurcut      DECIMAL,"
             "SettleQty      DECIMAL,"
             "SettleCurr     DECIMAL,"
             "CutRate        INT,"
             "RepairNo       STRING,"
             "RepairBy       STRING,"
             "Operator       STRING,"
             "Receiver       STRING,"
             "ConfirmDate    STRING,"
             "Remarks        STRING,"
             "VeriMark       INT,"
             "Status         INT,"
             "SendType       STRING,"
             "InTransact     INT,"
             "SumIntegral    INT,"
             "FareCurr       DECIMAL,"
             "PaidCurr       DECIMAL,"
             "LogNo          STRING,"
             "SumHCb1        DECIMAL,"
             "SumDiscur      DECIMAL,"
             "CutCurr        DECIMAL,"
             "Zcurr          DECIMAL,"
             "SumNcur        DECIMAL,"
             "Accredit       STRING,"
             "Cardno         STRING,"
             "SumOricur      DECIMAL,"
             "SumNCurCut     DECIMAL,"
             "bVoucher       STRING,"
             "Vdiscount      DECIMAL,"
             "Outputqty      DECIMAL,"
             "Veridate       STRING,"
             "InvoiceCurr    DECIMAL,"
             "Accreditdate   STRING,"
             "RemoteOrderNo  STRING,"
             "VAcurr         DECIMAL,"
             "ModiVersion    STRING,"
             "Tertiary       INT)",
    #销售细表
    'p_sell_sub':"(customermark      STRING,"
                 "PKID               INT,"
                 "PurchaseID         INT,"
                 "Partinno           INT,"
                 "Depot              STRING,"
                 "Ware               STRING,"
                 "S_stype            STRING,"
                 "Qty                DECIMAL,"
                 "Oprc               DECIMAL,"
                 "Oprccl             DECIMAL,"
                 "Cbprc              DECIMAL,"
                 "TaxRate            DECIMAL,"
                 "SettleQty          DECIMAL,"
                 "SettleCurr         DECIMAL,"
                 "Fpje               DECIMAL,"
                 "NegMark            STRING,"
                 "ContractID         INT,"
                 "RetQty             DECIMAL,"
                 "RetfrompurchaseID  INT,"
                 "InPutNo            STRING,"
                 "Jjno               STRING,"
                 "Prc_Way_No         STRING,"
                 "GroupNo            STRING,"
                 "SeqNo              INT,"
                 "Remarks            STRING,"
                 "TransetPrc         DECIMAL,"
                 "WarnLevel          STRING,"
                 "bPrompt            STRING,"
                 "PmtPartinno        INT,"
                 "SaleOrderID        INT,"
                 "FuturesMark        STRING,"
                 "RetPKID            INT,"
                 "CbNiprc            DECIMAL,"
                 "StockFlags         STRING,"
                 "Disoprc            DECIMAL,"
                 "Noprc              DECIMAL,"
                 "Orioprc            DECIMAL,"
                 "Bvoucher           STRING,"
                 "Vdiscount          DECIMAL,"
                 "OutputQty          DECIMAL,"
                 "JJCbPrc            DECIMAL,"
                 "OrderingID         INT,"
                 "CIFprc             DECIMAL,"
                 "TExRate            DECIMAL,"
                 "CExRate            DECIMAL,"
                 "TTaxRate           DECIMAL,"
                 "TariffCurr         DECIMAL,"
                 "AddTaxCurr         DECIMAL,"
                 "Tertiary           INT)"
}


oldErpImport_dtype={
#分店信息表
    'b_corp':{"customermark":"object",
             "CorpID":"object",
             "parentID":"object",
             "CorpNo":"object",
             "CorpName":"object",
             "CorpShortName":"object",
             "CorpheadName":"object",
             "TelNo":"object",
             "Mobile":"object",
             "Addr":"object",
             "Remarks":"object",
             "FaxNo":"object",
             "ZipCode":"object",
             "OrderModule":"object",
             "SaleModule":"object",
             "HsCorp":"object",
             "CorpMark":"object",
             "SelfMark":"object",
             "VDisCount":"object",
             "UnDisCountPart":"object",
             "AllotPriceType":"object",
             "corpLogo":"object",
             "CorpType":"object",
             "LicenceCode":"object",
             "OpeningLicenceCode":"object",
             "CreditCode":"object",
             "LicencesPic":"object",
             "LinkMan1":"object",
             "Phone1":"object",
             "LinkMan2":"object",
             "Phone2":"object",
             "LinkMan3":"object",
             "Phone3":"object",
             "Email":"object",
             "Wechat":"object",
             "Website":"object",
             "PlatFormID":"object",
             "SRAreaID":"object"},
    #库房表
    'b_depot':{"customermark":"object",
              "PKID":"object",
              "CorpID":"object",
              "DepotNo":"object",
              "DepotName":"object",
              "IsUse":"object",
              "Zjf":"object",
              "SortBySale":"object",
              "SortByExport":"object",
              "PrinterID":"object"},
    #货位表
    'b_wareplace':{"customermark":"object",
                  "PKID":"object",
                  "Depot":"object",
                  "Ware":"object",
                  "sWareProperty":"object"},
    #员工信息表
    'b_power':{"customermark":"object",
              "LoginID":"object",
              "CorpID":"object",
              "sPerCode":"object",
              "sPerName":"object",
              "sDuty":"object",
              "sSex":"object",
              "dBirthday":"object",
              "mSalary":"object",
              "sDegree":"object",
              "sPapers":"object",
              "sAddr":"object",
              "sTel":"object",
              "sMobile":"object",
              "dEmploy":"object",
              "Pwd":"object",
              "FlowNo":"object",
              "Type":"object",
              "MenuStr":"object",
              "DataStr":"object",
              "AuthorStr":"object",
              "DepotStr":"object",
              "CorpStr":"object",
              "bSys":"object",
              "bOperate":"object",
              "bSales":"object",
              "IsUse":"object",
              "fDiscount":"object",
              "PriceType":"object",
              "fScale":"object",
              "VenCorpStr":"object",
              "MgeDepotStr":"object",
              "FinanceCorpStr":"object",
              "Department":"object",
              "WorkGroup":"object",
              "WorkClass":"object",
              "Technical":"object",
              "Quotale":"object",
              "bWorks":"object",
              "fPartScale":"object",
              "fWorkDiscount":"object",
              "fPartDiscount":"object",
              "RoleStr":"object",
              "Zjf":"object",
              "TfDisCount":"object",
              "bLowIprc":"object",
              "bAccredit":"object",
              "bWordchecker":"object",
              "PlatFormUserID":"object",
              "bCorpSys":"object"},
    #基本资料种类（共享表）
    'b_baseconn':{"customermark":"object",
                 "PKID":"object",
                 "BaseName":"object",
                 "BaseType":"object",
                 "BaseClass":"object",
                 "MaxLength":"object"},
    #基本资料种类详细信息表(共享表)
    'b_baseinfo':{"customermark":"object",
                 "PKID":"object",
                 "IType":"object",
                 "Name":"object",
                 "Zjf":"object",
                 "SysBZ":"object",
                 "Orposi":"object",
                 "Remarks":"object",
                 "DisCount":"object",
                 "sNo":"object"},
    #省市字典
    'b_province':{"customermark":"object",
                 "PKID":"object",
                 "Province":"object",
                 "Pzjf":"object",
                 "CNo":"object",
                 "City":"object",
                 "CZjf":"object",
                 "Area":"object",
                 "AZjf":"object"},
    #配件资料表
    'b_part':{"customermark":"object",
             "PartInno":"object",
             "PartNo":"object",
             "NameC":"object",
             "NameE":"object",
             "Zjf":"object",
             "Model":"object",
             "Unit":"object",
             "Stype":"object",
             "Factory":"object",
             "PartType":"object",
             "PartNo_A":"object",
             "UnitCode":"object",
             "FactMark":"object",
             "LotSize":"object",
             "Weight":"object",
             "Lenth":"object",
             "Width":"object",
             "Height":"object",
             "InCub":"object",
             "Ctjjb":"object",
             "Uqty":"object",
             "CurType":"object",
             "Sengineno":"object",
             "ToteMark":"object",
             "DiscountMark":"object",
             "NoorderMark":"object",
             "MainLand":"object",
             "Prices":"object",
             "BargainPrice":"object",
             "Iprc":"object",
             "Oprc":"object",
             "Prices_O":"object",
             "Prices_N":"object",
             "MarketPrc":"object",
             "RmbPrc1":"object",
             "RmbPrc2":"object",
             "Prices2":"object",
             "TaxRate":"object",
             "TaxNum":"object",
             "MoneyKind1":"object",
             "MoneyKind2":"object",
             "Part_Th":"object",
             "Part_Tx":"object",
             "BarCode":"object",
             "TopNum":"object",
             "BotNum":"object",
             "CalcBottom":"object",
             "Wbh":"object",
             "VendorInno":"object",
             "Remarks":"object",
             "ModiVersion":"object",
             "FixPriceMark":"object",
             "FixPrice":"object",
             "CarSeries":"object",
             "WrtPeriod":"object",
             "Integral":"object",
             "ConsumeQty":"object",
             "UseIntegral":"object",
             "ChangeQty":"object",
             "PartSort":"object",
             "FixRate":"object",
             "Brand":"object",
             "FixTRate":"object",
             "FixOprc":"object",
             "bAdd":"object",
             "Define1":"object",
             "Define2":"object",
             "Define3":"object",
             "Define4":"object",
             "Define5":"object",
             "Define6":"object",
             "Define7":"object",
             "Define8":"object",
             "Define9":"object",
             "Define10":"object",
             "FormatPartNo":"object",
             "sPartimageUrl":"object",
             "OECode":"object",
             "OEName":"object",
             "StandardName":"object",
             "CarBrand":"object",
             "Similarity":"object",
             "bToPlatForm":"object"},
    #互换码表
    'b_pr_nno':{"customermark":"object",
               "PKID":"object",
               "Exta_Code":"object",
               "PartInno":"object",
               "ModiVersion":"object"},
    #库存表
    'p_stock':{"customermark":"object",
              "PKID":"object",
              "PurchaseNo":"object",
              "Partinno":"object",
              "Depot":"object",
              "Ware":"object",
              "Qty":"object",
              "Vqty":"object",
              "Iprc":"object",
              "Oprc":"object",
              "Oprp":"object",
              "Oprd":"object",
              "Fdate":"object",
              "modiversion":"object",
              "TransetPrc":"object",
              "Remarks":"object",
              "Oprp1":"object",
              "Oprp2":"object",
              "Oprp3":"object",
              "Oprp4":"object",
              "Niprc":"object",
              "Oprs":"object",
              "Vendorinno":"object",
              "Wrtperiod":"object",
              "Stockflags":"object",
              "Originno":"object",
              "Sorderpkid":"object",
              "CIFprc":"object",
              "TExRate":"object",
              "CExRate":"object",
              "TTaxRate":"object",
              "TariffCurr":"object",
              "AddTaxCurr":"object"},
    #需求表
    'p_require':{"customermark":"object",
                "PKID":"object",
                "CorpID":"object",
                "PartInno":"object",
                "Reqdate":"object",
                "Reqqty":"object",
                "Purchasetype":"object",
                "Purchaseno":"object",
                "Operator":"object",
                "Vennamec":"object",
                "Remarks":"object",
                "Ordered":"object",
                "Modiversion":"object"},
    #采购单主表
    'p_receipt':{"customermark":"object",
                "PurchaseID":"object",
                "PurchaseNo":"object",
                "CorpID":"object",
                "PurchaseDate":"object",
                "PurchaseType":"object",
                "Vendorinno":"object",
                "Privi":"object",
                "PayCode":"object",
                "InvoiceCode":"object",
                "InvoiceNo":"object",
                "ManiputeCode":"object",
                "sumQty":"object",
                "sumCur":"object",
                "sumCb":"object",
                "sumTax":"object",
                "settleQty":"object",
                "settleCurr":"object",
                "Operator":"object",
                "Receiver":"object",
                "ConfirmDate":"object",
                "Remarks":"object",
                "VeriMark":"object",
                "Status":"object",
                "sumTranset":"object",
                "InTranset":"object",
                "PackNo":"object",
                "TransNo":"object",
                "sendType":"object",
                "sumDisCur":"object",
                "sumDisCb":"object",
                "CutCur":"object",
                "sumOriCur":"object",
                "sumOriCb":"object",
                "PrintCount":"object",
                "RepairBy":"object",
                "OutPriceType":"object",
                "ACurr":"object",
                "ZCurr":"object",
                "InvoiceCurr":"object",
                "InputQty":"object",
                "VeriDate":"object",
                "ModiVersion":"object",
                "JJReceiver":"object",
                "JJconfirmDate":"object",
                "RetSettleQty":"object",
                "RetSettleCurr":"object",
                "LastPaidDate":"object",
                "Verifier":"object"},
    #采购单细表
    'p_receipt_sub':{"customermark":"object",
                    "PKID":"object",
                    "PurchaseID":"object",
                    "PartInno":"object",
                    "Depot":"object",
                    "Ware":"object",
                    "Qty":"object",
                    "Iprc":"object",
                    "NIprc":"object",
                    "TaxRate":"object",
                    "SettleQty":"object",
                    "SettleCurr":"object",
                    "Oprc":"object",
                    "Oprp":"object",
                    "Oprd":"object",
                    "ContractID":"object",
                    "Fpje":"object",
                    "RetQty":"object",
                    "RetFromPurchaseID":"object",
                    "InputNo":"object",
                    "Remarks":"object",
                    "TransetPrc":"object",
                    "RetFromPKID":"object",
                    "Oprp1":"object",
                    "Oprp2":"object",
                    "Oprp3":"object",
                    "Oprp4":"object",
                    "CbPrc":"object",
                    "DisIprc":"object",
                    "DisNiprc":"object",
                    "DisCount":"object",
                    "wrtPeriod":"object",
                    "OriIprc":"object",
                    "OriNiprc":"object",
                    "CbNIprc":"object",
                    "StockFlags":"object",
                    "InputQty":"object",
                    "Oprs":"object",
                    "CIFprc":"object",
                    "TExRate":"object",
                    "CExRate":"object",
                    "TTaxRate":"object",
                    "TariffCurr":"object",
                    "AddTaxCurr":"object",
                    "RetSettleQty":"object",
                    "RetSettleCurr":"object",
                    "ProductNo":"object",
                    "JJPKID":"object",
                    "sWareProperty":"object",
                    "PTPrice":"object"},
    #销售表
    'p_sell':{"customermark":"object",
             "PurchaseID":"object",
             "Purchaseno":"object",
             "CorpID":"object",
             "Purchasedate":"object",
             "Purchasetype":"object",
             "Privi":"object",
             "Vendorinno":"object",
             "Paycode":"object",
             "Invoicecode":"object",
             "Invoiceno":"object",
             "maniputecode":"object",
             "Packno":"object",
             "Transno":"object",
             "SumTrans":"object",
             "SumQy":"object",
             "SumCurr":"object",
             "SumVCurr":"object",
             "SumhCb":"object",
             "SumTax":"object",
             "SumCurcut":"object",
             "SettleQty":"object",
             "SettleCurr":"object",
             "CutRate":"object",
             "RepairNo":"object",
             "RepairBy":"object",
             "Operator":"object",
             "Receiver":"object",
             "ConfirmDate":"object",
             "Remarks":"object",
             "VeriMark":"object",
             "Status":"object",
             "SendType":"object",
             "InTransact":"object",
             "SumIntegral":"object",
             "FareCurr":"object",
             "PaidCurr":"object",
             "LogNo":"object",
             "SumHCb1":"object",
             "SumDiscur":"object",
             "CutCurr":"object",
             "Zcurr":"object",
             "SumNcur":"object",
             "Accredit":"object",
             "Cardno":"object",
             "SumOricur":"object",
             "SumNCurCut":"object",
             "bVoucher":"object",
             "Vdiscount":"object",
             "Outputqty":"object",
             "Veridate":"object",
             "InvoiceCurr":"object",
             "Accreditdate":"object",
             "RemoteOrderNo":"object",
             "VAcurr":"object",
             "ModiVersion":"object",
             "Tertiary":"object"},
    #销售细表
    'p_sell_sub':{"customermark":"object",
                 "PKID":"object",
                 "PurchaseID":"object",
                 "Partinno":"object",
                 "Depot":"object",
                 "Ware":"object",
                 "S_stype":"object",
                 "Qty":"object",
                 "Oprc":"object",
                 "Oprccl":"object",
                 "Cbprc":"object",
                 "TaxRate":"object",
                 "SettleQty":"object",
                 "SettleCurr":"object",
                 "Fpje":"object",
                 "NegMark":"object",
                 "ContractID":"object",
                 "RetQty":"object",
                 "RetfrompurchaseID":"object",
                 "InPutNo":"object",
                 "Jjno":"object",
                 "Prc_Way_No":"object",
                 "GroupNo":"object",
                 "SeqNo":"object",
                 "Remarks":"object",
                 "TransetPrc":"object",
                 "WarnLevel":"object",
                 "bPrompt":"object",
                 "PmtPartinno":"object",
                 "SaleOrderID":"object",
                 "FuturesMark":"object",
                 "RetPKID":"object",
                 "CbNiprc":"object",
                 "StockFlags":"object",
                 "Disoprc":"object",
                 "Noprc":"object",
                 "Orioprc":"object",
                 "Bvoucher":"object",
                 "Vdiscount":"object",
                 "OutputQty":"object",
                 "JJCbPrc":"object",
                 "OrderingID":"object",
                 "CIFprc":"object",
                 "TExRate":"object",
                 "CExRate":"object",
                 "TTaxRate":"object",
                 "TariffCurr":"object",
                 "AddTaxCurr":"object",
                 "Tertiary":"object"}
}