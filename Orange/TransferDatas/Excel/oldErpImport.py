from pyspark import SparkContext,SparkConf,HiveContext
from pyspark.sql.functions import *
import pandas as pd
import argparse
import datetime
from ExcelColLost import oldErpImport_hiveCol as hiveCol
from ExcelColLost import oldErpImport_colList as colList
from ExcelColLost import oldErpImport_dtype
import os

#spark入口
conf = SparkConf().setAppName("Transfer_PostgreSql_AccountsDB")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

#日期处理函数群
def date_str(date):
    return date.strftime("%Y%m%d")
def str_date(str):
    return datetime.datetime.strptime(str, "%Y%m%d").date()
def date_str_sep(date):
    return date.strftime("%Y-%m-%d")
def exeHql(sql):
    return sqlContext.sql(sql)

'''
是否是测试数据库:
1：正常环境（数据库：orange
2：测试环境（数据库：test
'''
testHdatabase = 1

def saveHiveDatabase(tableName,df,hiveCol,dateStr,WebServerID,flag):
    print("start save:" + tableName)
    # 设置动态分区
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
    # 动态分区上线设置
    sqlContext.setConf("hive.exec.max.dynamic.partitions", "10000")
    # 使用orange库
    if testHdatabase == 1:
        sqlContext.sql("use orange")
    else:
        sqlContext.sql("use test")
    # 表若不存在新建Hive表
    sqlContext.sql("CREATE TABLE IF NOT EXISTS " + tableName + hiveCol + " PARTITIONED BY (day STRING,webserverid STRING)")
    dfTemp = df.withColumn("day", lit(dateStr)).withColumn("webserverid",lit(WebServerID))
    #存为临时表
    dfTemp.registerTempTable("tempTable")
    #从临时表读取数据存入hive
    if flag == 1:
        sqlContext.sql("INSERT INTO TABLE " + tableName + " select * from tempTable")
    else:
        sqlContext.sql("INSERT OVERWRITE TABLE " + tableName + " PARTITION(day,webserverid) select * from tempTable")
    print("Saving data to " + tableName + " was done!")


def readFromCsv(WebServerID,tableName,dateStr):
    #文件地址
    print('csv file path:/data/ftpdata/' + WebServerID + '_' + dateStr + '_' + tableName + '.csv')
    # 读取对应csv文件
    reader = pd.read_csv('/data/ftpdata/' + WebServerID + '_' + dateStr + '_' + tableName + '.csv',
                       sep=str(format(0x03, 'c'), ),
                       names=colList[tableName],
                       keep_default_na=False,
                       dtype=oldErpImport_dtype[tableName],
                       iterator=True)
                       #nrows=5)#只取前5列
    loop = True
    chunkSize = 10000000
    chunks = []
    if tableName in ('p_stock','b_part'):
        while loop:
            try:
                chunk = reader.get_chunk(chunkSize)
                pddf = pd.concat(chunk, ignore_index=True)
                df = sqlContext.createDataFrame(pddf)
                # 保存对应数据进入hive表
                saveHiveDatabase("ods_erpcsv_" + tableName, dfCsv, hiveCol[tableName], PARAMS.date, WebServerID,1)
            except StopIteration:
                loop = False
    else:
        while loop:
            try:
                chunk = reader.get_chunk(chunkSize)
                chunks.append(chunk)
            except StopIteration:
                loop = False
        pddf = pd.concat(chunks,ignore_index=True)
        # 将pandas的dataframe转化为spark的dataframe
        df = sqlContext.createDataFrame(pddf)
        # 保存对应数据进入hive表
        saveHiveDatabase("ods_erpcsv_" + tableName, dfCsv, hiveCol[tableName], PARAMS.date, WebServerID,0)
    return df

def dropDatasFromTable(tableName,dateStr):
    TableName = "ods_erpcsv_"+ tableName
    DateStr =date_str(str_date(dateStr)-datetime.timedelta(1))
    print("start drop " + TableName + " datas before " + DateStr)
    sqlContext.sql('ALTER TABLE ' + TableName + ' drop if exists partition (day = \'' + DateStr + '\')')
    print("drop " + DateStr + "'s datas successful")
    print("")



#主函数
if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    defualt_date = date_str(datetime.date.today()-datetime.timedelta(1))
    defualt_web_servers = ['5']
    defualt_tables = ['b_baseconn','b_baseinfo','b_depot','b_corp','b_wareplace',
                      'b_power','b_province','b_pr_nno','p_require','p_receipt',
                      'p_receipt_sub','p_sell','p_sell_sub','p_stock','b_part'
                      ]
    parser = argparse.ArgumentParser()
    parser.add_argument('--date', type=str, default=defualt_date,
                        help='date')
    parser.add_argument('--webserver', type=list, default=defualt_web_servers,
                        help='webserver')
    parser.add_argument('--tables', type=list, default=defualt_tables,
                        help='tables')
    PARAMS, _ = parser.parse_known_args()
    list=[]
    for TableName in PARAMS.tables:
        for WebServerID in PARAMS.webserver:
            if os.path.exists('/data/ftpdata/' + WebServerID + '_' + PARAMS.date + '_' + TableName + '.csv'):
                #查找对应表，读取数据
                dfCsv = readFromCsv(WebServerID,TableName,PARAMS.date)
            else:
                list.append(TableName)
        #删除该表头一天数据
        dropDatasFromTable(TableName,PARAMS.date)
    print("")
    print(list,"is not exist")
    sc.stop()
    print("program is done at: "  + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))