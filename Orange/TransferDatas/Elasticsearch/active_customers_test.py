from pyspark import SparkConf,HiveContext,SparkContext
from elasticsearch import Elasticsearch
from pyspark.sql.functions import *
import datetime
import pandas as pd
import argparse
#Spark入口
conf = SparkConf().setAppName("活跃客户量统计")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

#日期处理函数群
def date_str(date):
    return date.strftime("%Y%m%d")
def str_date(str):
    return datetime.datetime.strptime(str, "%Y%m%d").date()
def date_str_sep(date):
    return date.strftime("%Y-%m-%d")


'''
是否是测试数据库:
1：正常环境（数据库：orange
2：测试环境（数据库：test
'''
testHdatabase = 1

filterlist = ['aggregations.userId_i.buckets.key']

hiveCol="(user_id STRING)"

#根据条件，查出es中数据，返回dicts
def getEs(filterlist,es,keyword,today,yesterday):
    res = es.search(
        index='logc-production-http-log.*',
        body={
            "query" : {
                "bool" : {
                    "must" : [
                        {"match" : {"path_s.keyword" : keyword}},
                        {"match" : {"method_s.keyword" : "POST"}},
                        {"range" : {"at_s" : {"gte" : date_str_sep(yesterday),"lt" : date_str_sep(today) }}},
                        {"exists": {'field':"tags_o.userId_i"}},
                    ]
                }
            },
            "aggs":{
                "userId_i" : {
                    "terms" : {"field" : "tags_o.userId_i"},
                }
            }
        },
        filter_path=filterlist
    )
    return res

#将es的dicts转化为df
def esToDf(esDict):
    dfDict={}
    flag=0
    dfDict['user_id'] = []
    if len(esDict) != 0:
        for user_id in esDict['aggregations']['userId_i']['buckets']:
            dfDict['user_id'].append(user_id['key'])
        data = pd.DataFrame(dfDict)
        df = sqlContext.createDataFrame(data)
    else:
        df=dfDict
        flag=1
    return df,flag

def saveHiveDatabase(tableName,df,hiveCol,dateStr):
    print("start save:" + tableName)
    # 设置动态分区
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
    # 动态分区上线设置
    sqlContext.setConf("hive.exec.max.dynamic.partitions", "10000")
    # 使用orange库
    if testHdatabase == 1:
        sqlContext.sql("use orange")
    else:
        sqlContext.sql("use test")
    # 表若不存在新建Hive表
    sqlContext.sql("CREATE TABLE IF NOT EXISTS " + tableName + hiveCol + " PARTITIONED BY (day STRING)")
    dfTemp = df.withColumn("day", lit(dateStr))
    #存为临时表
    dfTemp.registerTempTable("tempTable")
    #从临时表读取数据存入hive
    sqlContext.sql("INSERT OVERWRITE TABLE " + tableName + " PARTITION(day) select * from tempTable")
    print("Saving data to " + tableName + " was done!")

if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    PARAMS = None
    """
    默认开始及结束日期日期={Str}
    均为昨天，只跑昨天一天数据
    """
    defualt_date = date_str(datetime.date.today())
    parser = argparse.ArgumentParser()
    parser.add_argument('--startdate', type=str, default=defualt_date,
                        help='start date')
    parser.add_argument('--enddate', type=str, default=defualt_date,
                        help='end date')
    PARAMS, _ = parser.parse_known_args()
    # 定义今天为开始日期
    today = date_str(str_date(PARAMS.startdate))
    es = Elasticsearch(['https://es.hztl3.xyz'], port=80)
    # 当开始日期<=结束日期时，运行该程序。如果开始日期=结束日期，break种植
    while (today <= PARAMS.enddate):
        print("start reading " + today + " datas")
        keywordsList = ['/password-login','/register-new','/register','/token-login','/software/login','/quickly-register']
        plus_flag = 0
        yesterday = date_str(str_date(today) - datetime.timedelta(days=1))
        for keywords in keywordsList:
            esDict = getEs(filterlist,es,keywords,str_date(today),str_date(yesterday))
            df,flag = esToDf(esDict)
            #合并所有path获取的数据
            if flag == 0 and plus_flag == 0 :
                plus_flag+=1
                dfAll = df
                print(dfAll.count())
            elif flag == 0:
                dfAll=dfAll.unionAll(df)
                print(dfAll.count())
            #如果是最后一个path读取，开始保存数据
            #如果当天无数据，plus——flag=0，则不保存
            if len(keywordsList)-1 == keywordsList.index(keywords):
                if plus_flag != 0:
                    saveHiveDatabase("ods_es_active_customer", dfAll.distinct(), hiveCol, yesterday)
                else:
                    print("today have no active_customer")
        if today == PARAMS.enddate:
            break
        print("read " + today + " datas finished")
        today = date_str(str_date(today) + datetime.timedelta(days=1))
        print("")
        print("")
    sc.stop()
    print("program is done at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
