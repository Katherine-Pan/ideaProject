from pyspark import SparkConf,HiveContext,SparkContext
from elasticsearch import Elasticsearch
from pyspark.sql.functions import *
import datetime
import pandas as pd
import argparse
#Spark入口
conf = SparkConf().setAppName("上下架数据保存")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

#日期处理函数群
def date_str(date):
    return date.strftime("%Y%m%d")
def str_date(str):
    return datetime.datetime.strptime(str, "%Y%m%d").date()
def date_str_sep(date):
    return date.strftime("%Y-%m-%d")

'''
是否是测试数据库:
1：正常环境（数据库：orange
2：测试环境（数据库：test
'''
testHdatabase = 1

def getquery_data(today,yesterday):
    query_data = {
        #下架
        "outshelve" : {
            "query" : {
                "bool" : {
                    "must" : [
                        {"match" : {"path_s.keyword" : "/company-parts/delete"}},
                        {"match" : {"method_s.keyword" : "POST"}},
                        {"range" : {"at_s" : {"gte" : date_str_sep(yesterday),"lt" : date_str_sep(today) }}},
                        {"exists": {'field':"tags_o.deleted_o"}}
                ]
            }
        },
        "aggs":{
            "companyId" : {
                    "terms" : {"field" : "tags_o.companyId_i"},
                    "aggs" : {
                        "allianceId" : {
                            "terms" : {"field" : "tags_o.deleted_o.allianceId_i",},
                            "aggs" : {
                            "outshelve_sum" : {"sum" : {"field" :"tags_o.deleted_o.count_i"}}
                            }
                        }
                    }
                }
            }
        },
        #上架
        "onshelve":{
            "query": {
                "bool": {
                    "must": [
                       {"match" : {"path_s.keyword" : "/company-parts"}},
                        {"match" : {"method_s.keyword" : "POST"}},
                        {"range" : {"at_s" : {"gte" : date_str_sep(yesterday),"lt" : date_str_sep(today) }}},
                        {"exists": {'field':"tags_o.created_o"}}
                    ],
                }
            },
        "aggs":{
            "companyId" : {
                "terms" : {"field":"tags_o.companyId_i"},
                    "aggs" : {
                        "allianceId" : {
                            "terms" : {"field" : "tags_o.created_o.allianceId_i",},
                            "aggs" : {
                                "onshelve_sum" : {"sum" : {"field" : "tags_o.created_o.count_i"}}
                            }
                        }
                    }
                }
            }
        }
    }
    return query_data

filterlist = {
    # 下架
    "outshelve":['aggregations.companyId.buckets.key',
               'aggregations.companyId.buckets.allianceId.buckets.key',
               'aggregations.companyId.buckets.allianceId.buckets.outshelve_sum.value'],
    # 上架
    "onshelve":['aggregations.companyId.buckets.key',
               'aggregations.companyId.buckets.allianceId.buckets.key',
               'aggregations.companyId.buckets.allianceId.buckets.onshelve_sum.value']
}

hiveCol={
    # 下架
    "outshelve":"(company_id STRING,"
                "alliance_id STRING,"
                "outshelve_sum DECIMAL)",
    # 上架
    "onshelve":"(company_id STRING,"
                "alliance_id STRING,"
                "onshelve_sum DECIMAL)",
}

#根据条件，进行es查询，返回dict
def getEs(query_data,filterlist,es):
    res = es.search(
        index='logc-production-http-log.*',
        body=query_data,
        filter_path=filterlist
    )
    return res

#将查出的esDict转化为df
def esToDf(keywords,esDict):
    dfDict={}
    dfDict['company_id'] = []
    dfDict['alliance_id'] = []
    if keywords == "onshelve":
        dfDict['onshelve_sum'] = []
        if len(esDict['aggregations']['companyId']['buckets'])!=0:
            for company_id in esDict['aggregations']['companyId']['buckets']:
                if len(company_id['allianceId']['buckets'])!=0:
                    for value in company_id['allianceId']['buckets']:
                        dfDict['company_id'].append(company_id['key'])
                        dfDict['alliance_id'].append(value['key'])
                        dfDict['onshelve_sum'].append(value['onshelve_sum']['value'])
    elif keywords == "outshelve":
        dfDict['outshelve_sum'] = []
        if len(esDict['aggregations']['companyId']['buckets'])!=0:
            for company_id in esDict['aggregations']['companyId']['buckets']:
                if len(company_id['allianceId']['buckets'])!=0:
                    for value in company_id['allianceId']['buckets']:
                        dfDict['company_id'].append(company_id['key'])
                        dfDict['alliance_id'].append(value['key'])
                        dfDict['outshelve_sum'].append(value['outshelve_sum']['value'])
    data = pd.DataFrame(dfDict)
    df = sqlContext.createDataFrame(data)
    return df

def saveHiveDatabase(tableName,df,hiveCol,dateStr):
    print("start save:" + tableName)
    # 设置动态分区
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
    # 动态分区上线设置
    sqlContext.setConf("hive.exec.max.dynamic.partitions", "10000")
    # 使用orange库
    if testHdatabase == 1:
        sqlContext.sql("use orange")
    else:
        sqlContext.sql("use test")
    # 表若不存在新建Hive表
    sqlContext.sql("CREATE TABLE IF NOT EXISTS " + tableName + hiveCol + " PARTITIONED BY (day STRING)")
    dfTemp = df.withColumn("day", lit(dateStr))
    #存为临时表
    dfTemp.registerTempTable("tempTable")
    #从临时表读取数据存入hive
    sqlContext.sql("INSERT OVERWRITE TABLE " + tableName + " PARTITION(day) select * from tempTable")
    print("Saving data to " + tableName + " was done!")

if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    PARAMS = None
    """
    默认开始及结束日期日期={Str}
    均为昨天，只跑昨天一天数据
    """
    defualt_date = date_str(datetime.date.today())
    parser = argparse.ArgumentParser()
    parser.add_argument('--startdate', type=str, default=defualt_date,
                        help='start date')
    parser.add_argument('--enddate', type=str, default=defualt_date,
                        help='end date')
    PARAMS, _ = parser.parse_known_args()
    # 定义今天为开始日期
    today = date_str(str_date(PARAMS.startdate))
    es = Elasticsearch(['https://es.hztl3.xyz'], port=80)
    # 当开始日期<=结束日期时，运行该程序。如果开始日期=结束日期，break种植
    while (today <= PARAMS.enddate):
        print("start reading " + today + " datas")
        keywordsList = ['onshelve','outshelve']
        yesterday = date_str(str_date(today) - datetime.timedelta(days=1))
        for keywords in keywordsList:
            query_data = getquery_data(str_date(today), str_date(yesterday))
            esDict = getEs(query_data[keywords],filterlist[keywords],es)
            if len(esDict) == 0:
                print("today " + keywords + "have no data")
            else:
                df = esToDf(keywords,esDict)
                saveHiveDatabase("ods_es_"+keywords, df, hiveCol[keywords], yesterday)
        if today == PARAMS.enddate:
            break
        print("read " + today + " datas finished")
        today = date_str(str_date(today) + datetime.timedelta(days=1))
        print("")
        print("")
    sc.stop()
    print("program is done at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))

