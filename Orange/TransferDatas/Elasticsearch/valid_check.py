from pyspark import SparkConf,HiveContext,SparkContext
from elasticsearch import Elasticsearch,helpers
from pyspark.sql.functions import *
import datetime
import pandas as pd
from flask import json
import argparse
#Spark入口
conf = SparkConf().setAppName("上下架数据保存")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

#日期处理函数群
def date_str(date):
    return date.strftime("%Y%m%d")
def str_date(str):
    return datetime.datetime.strptime(str, "%Y%m%d").date()
def date_str_sep(date):
    return date.strftime("%Y-%m-%d")

'''
是否是测试数据库:
1：正常环境（数据库：orange
2：测试环境（数据库：test
'''
testHdatabase = 1

def getquery_data(today,yesterday):
    query_data = {
        #单个添加
            "single" : {
                "query" : {
                    "bool" : {
                        "must" : [
                            {"match" : {"path_s.keyword" : "/carts/create"}},
                            {"match" : {"method_s.keyword" : "POST"}},
                            {"match" : {"resBody_s" : "ok"}},
                            {"range" : {"at_s" : {"gte" : date_str_sep(yesterday),"lt" : date_str_sep(today) }}},
                        ]
                    }
                }
            },
            #批量添加
            "batch":{
                "query" : {
                    "bool" : {
                        "must" : [
                            {"match" : {"path_s.keyword" : "/carts"}},
                            {"match" : {"method_s.keyword" : "POST"}},
                            {"range" : {"at_s" : {"gte" : date_str_sep(yesterday),"lt" : date_str_sep(today) }}},
                        ]
                    }
                }
            }
        }

    return query_data

source_data = {
    #单个添加
    "single" : ['reqBody_s','session_o.UserId_i'],
    #批量添加
    "batch" : ['resBody_s','reqBody_s','session_o.UserId_i']
}

hiveCol="(seller_company_id STRING,sku_id STRING,buyer_user_id STRING,price DECIMAL,carts_qty_sum DECIMAL,checkcount INT)"

#获取es的值
def getEs(query_data,es,source_data):
    #使用helpers.scan返回的是迭代器
    res = helpers.scan(es,
                        index='logc-production-http-log.*',
                        query=query_data,
                        scroll="1m",
                        preserve_order=True,
                        _source=source_data)
    list = []
    #当迭代器结束前，将值赋给list
    while True:
        try:
            list.append(res.__next__())
        except StopIteration:
            break
    #必须在scan使用结束后关闭，不然无法正常运行
    res.close()
    return list

#判断key值是否存在，若不存在，赋给默认值，并且修改参数类型
def dictListAppend(esdict,jsondata,key,listKey):
    if key in ('sellerCompanyId','skuId','UserId_i'):
        if key in jsondata:
            esdict[listKey].append(str(jsondata[key]))
        else:
            esdict[listKey].append("")
    else:
        if key in jsondata:
            esdict[listKey].append(float(jsondata[key]))
        else:
            esdict[listKey].append(0.0)
    return esdict

#将es的list转化为df
def transEsListToDf(esList,keywords):
    esDict = {}
    flag = 0
    esDict['seller_company_id'] = []
    esDict['sku_id'] = []
    esDict['price'] = []
    esDict['qty'] = []
    esDict['buyer_user_id'] = []
    if keywords == 'single' and len(esList) != 0:
        for reqitem in esList:
            #将json格式字符串转化为dict
            reqBody_s = json.loads(reqitem['_source']['reqBody_s'])
            #将值赋给esDict
            esDict = dictListAppend(esDict,reqBody_s,'sellerCompanyId','seller_company_id')
            esDict = dictListAppend(esDict, reqBody_s, 'skuId','sku_id')
            esDict = dictListAppend(esDict, reqBody_s, 'price','price')
            esDict = dictListAppend(esDict, reqBody_s, 'qty','qty')
            esDict = dictListAppend(esDict, reqitem['_source']['session_o'],'UserId_i','buyer_user_id')
    elif keywords == 'batch' and len(esList) != 0:
        for item in esList:
            # 将json格式字符串转化为dict
            resBody_s= json.loads(item['_source']['resBody_s'])
            reqBody_s = json.loads(item['_source']['reqBody_s'])
            notOkSellerCompanylist = []
            notOkSkulist = []
            notOkIdList = []
            #判断有误添加失败的，并加入list
            if resBody_s['code'] != 'ok':
                notOkIdList.append(item['_id'])
            else:
                for resitem in resBody_s['data']:
                    if resitem['code'] != 'ok':
                        notOkSellerCompanylist.append(resitem['sellerCompanyId'])
                        notOkSkulist.append(resitem['skuId'])
            for reqitem in reqBody_s['data']:
                #去掉添加失败的商品信息，把添加购物车成功的商品信息录入esDict
                if reqitem['sellerCompanyId'] not in notOkSellerCompanylist and reqitem['sellerCompanyId'] not in notOkSkulist and item['_id'] not in notOkIdList:
                    esDict = dictListAppend(esDict, reqitem, 'sellerCompanyId', 'seller_company_id')
                    esDict = dictListAppend(esDict, reqitem, 'skuId', 'sku_id')
                    esDict = dictListAppend(esDict, reqitem, 'price', 'price')
                    esDict = dictListAppend(esDict, reqitem, 'qty', 'qty')
                    esDict = dictListAppend(esDict, item['_source']['session_o'],'UserId_i','buyer_user_id')
    else:
        flag = 1
        df = ""
    if flag == 0:
        data = pd.DataFrame(esDict)
        df = sqlContext.createDataFrame(data)
    return df,flag

def saveHiveDatabase(tableName,df,hiveCol,dateStr):
    print("start save:" + tableName)
    # 设置动态分区
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
    # 动态分区上线设置
    sqlContext.setConf("hive.exec.max.dynamic.partitions", "10000")
    # 使用orange库
    if testHdatabase == 1:
        sqlContext.sql("use orange")
    else:
        sqlContext.sql("use test")
    # 表若不存在新建Hive表
    sqlContext.sql("CREATE TABLE IF NOT EXISTS " + tableName + hiveCol + " PARTITIONED BY (day STRING)")
    dfTemp = df.withColumn("day", lit(dateStr))
    #存为临时表
    dfTemp.registerTempTable("tempTable")
    #从临时表读取数据存入hive
    sqlContext.sql("INSERT OVERWRITE TABLE " + tableName + " PARTITION(day) select * from tempTable")
    print("Saving data to " + tableName + " was done!")

if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    PARAMS = None
    """
    默认开始及结束日期日期={Str}
    均为昨天，只跑昨天一天数据
    """
    defualt_date = date_str(datetime.date.today())
    parser = argparse.ArgumentParser()
    parser.add_argument('--startdate', type=str, default=defualt_date,
                        help='start date')
    parser.add_argument('--enddate', type=str, default=defualt_date,
                        help='end date')
    PARAMS, _ = parser.parse_known_args()
    # 定义今天为开始日期
    today = date_str(str_date(PARAMS.startdate))
    es = Elasticsearch(['https://es.hztl3.xyz'], port=80)
    # 当开始日期<=结束日期时，运行该程序。如果开始日期=结束日期，break种植
    while (today <= PARAMS.enddate):
        print("start reading " + today + " datas")
        keywordsList = ['single','batch']#
        plus_flag = 0
        yesterday = date_str(str_date(today) - datetime.timedelta(days=1))
        for keywords in keywordsList:
            query_data = getquery_data(str_date(today),str_date(yesterday))
            esList = getEs(query_data[keywords], es, source_data[keywords])
            esDf, flag = transEsListToDf(esList, keywords)
            # 合并所有path获取的数据
            if flag == 0 and plus_flag == 0:
                plus_flag += 1
                dfAll = esDf
                print(dfAll.count())
            elif flag == 0:
                dfAll = dfAll.unionAll(esDf)
                print(dfAll.count())
            # 如果是最后一个path读取，开始保存数据
            # 如果当天无数据，plus——flag=0，则不保存
            if len(keywordsList) - 1 == keywordsList.index(keywords):
                if plus_flag != 0:
                    # 合并当日数据并计算
                    dfAll = dfAll \
                        .groupBy("seller_company_id", "sku_id", "buyer_user_id", "price") \
                        .agg(sum('qty').alias('carts_qty_sum'),
                             count('qty').alias('checkcount'))  # \
                    saveHiveDatabase("ods_es_valid_check", dfAll, hiveCol, yesterday)
                else:
                    print("today have no parts add to carts")
        if today == PARAMS.enddate:
            break
        print("read " + today + " datas finished")
        today = date_str(str_date(today) + datetime.timedelta(days=1))
        print("")
        print("")
    sc.stop()
    print("program is done at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))



