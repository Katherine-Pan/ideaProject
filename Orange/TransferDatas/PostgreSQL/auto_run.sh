#!/bin/sh
FILE=$1
START_DATE=$2
END_DATE=$3


DATE=`date +%Y%m%d`
echo ${DATE}

FILE_HOME="./"
echo "`date`----------Run pyspark app ${FILE}"


command="spark-submit --conf spark.blacklist.enabled=false --conf spark.rpc.message.maxSize=1024 --jars /home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-common.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-client.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-server.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/jars/spark-examples_2.11-2.4.0-cdh6.1.1.jar --driver-cores 4 --driver-memory 10240M --num-executors 20 --executor-cores 4 --executor-memory 10240M "


command+=" "${FILE_HOME}${FILE}" "${START_DATE}" "${END_DATE}


echo "$command"
eval $command