"""
by:pkh
此代码用于读取postgresql中chats库数据并转化存储于hive表中
因为库最好不要频繁开关，所以打开一次处理所有表格完成再关闭
"""

import psycopg2
from pyspark import SparkContext,SparkConf,HiveContext
from pyspark.sql.functions import *
import datetime
import argparse
from colList import dbChats_colList as listDfColName
from hiveCol import dbChats_hiveCol as listHiveColName

#spark入口
conf = SparkConf().setAppName("Transfer_PostgreSql_ChatsDB")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

#日期处理函数群
def date_str(date):
    return date.strftime("%Y%m%d")
def str_date(str):
    return datetime.datetime.strptime(str, "%Y%m%d").date()
def date_str_sep(date):
    return date.strftime("%Y-%m-%d")
def exeHql(sql):
    return sqlContext.sql(sql)

'''
是否是测试数据库:
1：正常环境（数据库：orange
2：测试环境（数据库：test
'''
testHdatabase = 1
'''
是否是测试数据库:
1：正常环境（数据库：basic
2：测试环境（数据库：basic_qa
'''
testPdatabase = 1

"""
读取PostgreSql的表名={Dict}
key：数据源表名={Str}
value:对应读取Postgre表名创建开关,1存储={Int}
"""
listPostgreSwitch = {
    'contacts' : 1,#联系人记录表
    'messages' : 1,#聊天记录表
}

"""
存储Hive的表名={Dict}
key：数据源表名={Str}
value:创建hive表开关及存为hive表名={list}
    Parem：[是否启动存储function，1存储={Int}，
            存入hiv的表名：{Str}]
"""
listHiveTable = {
    'contacts' : [1,'ODS_Psql_Contacts'],#联系人记录表
    'messages' : [1,'ODS_Psql_Messages'],#聊天记录表
}

"""
打开一个链接到PostgreSQL数据库，连接成功返回一个数据对象
return: conn数据对象 = {psycopg2.extensions.connection}
"""
def open_qa_database(databaseName):
    conn = psycopg2.connect(database=databaseName,
                            user='pgdata',
                            password='Tonglianinfo2018_db',
                            host='113.31.135.250',
                            port='21770')
    print("Opened database successfull")
    print("")
    return conn
def open_database(databaseName):
    conn = psycopg2.connect(database=databaseName,
                            user='cdhuser',
                            password='Hztl&2019@238Cdh',
                            host='113.31.135.238',
                            port='21768')
    print("Opened database successfull")
    print("")
    return conn

"""
关闭数据库链接
Param: conn数据对象 = {psycopg2.extensions.connection}
"""
def close_database(conn):
    conn.close()
    print("Closed database successfull")

"""
处理读出的数据list整理成为df,并且存入hive
Param:  读取出的数据list列表={list}
        df列名={list}
        hive表存储开关={Int}
        hive表存储表名={Str}
        分区日期={Str}
        hive表列名={Str}
"""
def reorganizeAndSave(dataList,dfColName,hiveSwitch,hiveTableName,dateStr,hiveTableColName,testHdatabase):
    #判断数据集合是否为空
    if len(dataList) != 0:
        print("Postgre reading was done")
        """
        将list转化为rdd
        Param：list数据集合={list}
        Return: rdd={RDD}
        """
        rdd = sc.parallelize(dataList)
        """
        将rdd转化为df
        Param： rdd数据集合={RDD}
                listColName转化df的列名={list}
        Return: df={DataFrame}
        """
        df = sqlContext.createDataFrame(rdd,dfColName)
        if hiveSwitch == 1:
            """
            存为hive表
            Param:  hive表名={Str}
                    分区日期字符串={Str}
                    需要存储的df={DataFrame}
                    hive表内完整字段名及类型={Str}
            """
            print("start save:" + hiveTableName)
            # 设置动态分区
            sqlContext.setConf("hive.exec.dynamic.partition", "true")
            sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
            # 动态分区上线设置
            sqlContext.setConf("hive.exec.max.dynamic.partitions", "10000")
            # 使用orange库
            if testHdatabase == 1:
                sqlContext.sql("use orange")
            else:
                sqlContext.sql("use test")
            # 表若不存在新建Hive表
            sqlContext.sql(
                "CREATE TABLE IF NOT EXISTS " + hiveTableName + hiveTableColName + " PARTITIONED BY (day STRING)")
            # 增加日期分区字段
            dfTemp = df.withColumn("day", lit(dateStr))
            # 存为临时表
            dfTemp.registerTempTable("tempTable")
            # 从临时表读取数据存入hive
            sqlContext.sql("INSERT OVERWRITE TABLE " + hiveTableName + " PARTITION(day) select * from tempTable")
            print("Saving data to " + hiveTableName + " was done!")
    else:
        print("date:" + dateStr + " have no data")

"""
读取数据并存储数据主代码
Param: conn数据对象 = {psycopg2.extensions.connection}
"""
def get_save_info(conn,today):
    #创建一个光标，用于整个数据库使用Python编程
    cursor = conn.cursor()
    #联系人记录表读取，并存储
    if listPostgreSwitch['contacts'] == 1:
        print("Starting read postgre.chats.public.contacts")
        cursor.execute(
            'select id,me,contact,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),jsonb_out(last_msg),last_read_max_id,deleted '
            ' from contacts '
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listContacts = cursor.fetchall()
        reorganizeAndSave(
            listContacts,
            listDfColName['contacts'],
            listHiveTable['contacts'][0],
            listHiveTable['contacts'][1],
            today,
            listHiveColName['contacts'],
            testHdatabase
        )
    #聊天记录表读取，并存储
    if listPostgreSwitch['messages'] == 1:
        print("Starting read postgre.chats.public.messages")
        cursor.execute(
            'select id,replace(replace(content,chr(10),\'\'),chr(13),\'\'),type,sender,receiver,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),sender_company,receiver_company '
            'from messages '
            'where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listMessages = cursor.fetchall()
        reorganizeAndSave(
            listMessages,
            listDfColName['messages'],
            listHiveTable['messages'][0],
            listHiveTable['messages'][1],
            today,
            listHiveColName['messages'],
            testHdatabase
        )

#主函数
if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    PARAMS = None
    """
    默认开始及结束日期日期={Str}
    均为昨天，只跑昨天一天数据
    """
    defualt_date = date_str(datetime.date.today())
    parser = argparse.ArgumentParser()
    parser.add_argument('--startdate', type=str, default=defualt_date,
                        help='start date')
    parser.add_argument('--enddate', type=str, default=defualt_date,
                        help='end date')
    PARAMS, _ = parser.parse_known_args()
    # 打开数据库
    if testPdatabase == 1:
        conn = open_database('chats')
    else:
        conn = open_qa_database('chats_qa')
    # 定义今天为开始日期
    today = date_str(str_date(PARAMS.startdate) - datetime.timedelta(days=1))
    # 当开始日期<=结束日期时，运行该程序。如果开始日期=结束日期，break种植
    while (today <= date_str(str_date(PARAMS.enddate) - datetime.timedelta(days=1))):
        print("start reading " + today + " datas")
        get_save_info(conn, today)
        if today == date_str(str_date(PARAMS.enddate) - datetime.timedelta(days=1)):
            break
        print("read " + today + " datas finished")
        today = date_str(str_date(today) + datetime.timedelta(days=1))
        print("")
        print("")
    # 关闭数据库
    close_database(conn)
    sc.stop()
    print("program is done at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))