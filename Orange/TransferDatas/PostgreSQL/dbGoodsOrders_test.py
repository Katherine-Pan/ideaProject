"""
by:pkh
此代码用于读取postgresql中goods_orders库数据并转化存储于hive表中
因为库最好不要频繁开关，所以打开一次处理所有表格完成再关闭
"""

import psycopg2
from pyspark import SparkContext,SparkConf,HiveContext
from pyspark.sql.functions import *
import datetime
import argparse
from colList import dbGoodsOrders_colList as listDfColName
from hiveCol import dbGoodsOrders_hiveCol as listHiveColName

#spark入口
conf = SparkConf().setAppName("Transfer_PostgreSql_GoodsOrdersDB")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

#日期处理函数群
def date_str(date):
    return date.strftime("%Y%m%d")
def str_date(str):
    return datetime.datetime.strptime(str, "%Y%m%d").date()
def date_str_sep(date):
    return date.strftime("%Y-%m-%d")
def exeHql(sql):
    return sqlContext.sql(sql)

'''
是否是测试数据库:
1：正常环境（数据库：orange
2：测试环境（数据库：test
'''
testHdatabase = 1
'''
是否是测试数据库:
1：正常环境（数据库：basic
2：测试环境（数据库：basic_qa
'''
testPdatabase = 1

"""
读取PostgreSql的表名={Dict}
key：数据源表名={Str}
value:对应读取Postgre表名创建开关,1存储={Int}
"""
listPostgreSwitch = {
    'orders' : 10,#订单表
    'orders_history' : 10,#历史订单表
    'order_logs' : 10,#订单日志表
    'order_details' : 10,#订单详情表
    'order_details_history' : 10,#订单详情历史表
    'order_detail_logs' : 1,#订单详情日志
    'logistics' : 10,#物流记录表
    'logistics_history' : 10,#物流记录历史表
    'company_stocks' : 10,#公司库存表
    'company_skus' : 10,#公司商品表
    'company_parts' : 10,#公司配件表
    'alliance_parts' : 10, #联盟配件表
    'alliance_part_ids': 10,#联盟配件id表
    'company_private_skus': 10,#未认证公司上传的配件信息及库存表
    'company_private_sku_logs' : 10,#上传的记录表
    'replenish_invitations' : 10,#补货邀约表
    'replenish_relations' : 10,#补货关系表
    'replenish_skus' : 10,#补货配件设置表
    'sku_auth_relations' : 10#配件库存授权关系表
}

"""
存储Hive的表名={Dict}
key：数据源表名={Str}
value:创建hive表开关及存为hive表名={list}
    Parem：[是否启动存储function，1存储={Int}，
            存入hiv的表名：{Str}]
"""
listHiveTable = {
    'orders' : [1,'ODS_Psql_Orders'],#订单表
    'orders_history' : [1,'ODS_Psql_Orders_History'],#历史订单表
    'order_logs' : [1,'ODS_Psql_Order_Logs'],#订单日志表
    'order_details' : [1,'ODS_Psql_Order_Details'],#订单详情表
    'order_details_history' : [1,'ODS_Psql_Order_Details_History'],#订单详情历史表
    'order_detail_logs' : [1,'ODS_Psql_Order_Detail_Logs'],#订单详情日志
    'logistics' : [1,'ODS_Psql_Logistics'],#物流记录表
    'logistics_history' : [1,'ODS_Psql_Logistics_History'],#物流记录历史表
    'company_stocks' : [1,'ODS_Psql_Company_Stocks'],#公司库存表
    'company_skus' : [1,'ODS_Psql_Company_Skus'],#公司商品表
    'company_parts' : [1,'ODS_Psql_Company_Parts'],#公司配件表
    'alliance_parts' : [1,'ODS_Psql_Alliance_Parts'], #联盟配件表
    'alliance_part_ids': [1,'ODS_Psql_Alliance_Part_Ids'], # 联盟配件ID表
    'company_private_skus': [1,'ODS_Psql_Company_Private_Skus'],#未认证公司上传的配件信息及库存表
    'company_private_sku_logs' : [1,'ODS_Psql_Company_Private_Sku_Logs'],#上传的记录表
    'replenish_invitations' : [1,'ODS_Psql_Replenish_Invitations'],#补货邀约表
    'replenish_relations' : [1,'ODS_Psql_Replenish_Relations'],#补货关系表
    'replenish_skus' : [1,'ODS_Psql_Replenish_Skus'],#补货配件设置表
    'sku_auth_relations' : [1,'ODS_Psql_Sku_Auth_Relations']#配件库存授权关系表
}

"""
打开一个链接到PostgreSQL数据库，连接成功返回一个数据对象
return: conn数据对象 = {psycopg2.extensions.connection}
"""
def open_qa_database(databaseName):
    conn = psycopg2.connect(database=databaseName,
                            user='pgdata',
                            password='Tonglianinfo2018_db',
                            host='113.31.135.250',
                            port='21770')
    print("Opened database successfull")
    print("")
    return conn
def open_database(databaseName):
    conn = psycopg2.connect(database=databaseName,
                            user='cdhuser',
                            password='Hztl&2019@238Cdh',
                            host='113.31.135.238',
                            port='21768')
    print("Opened database successfull")
    print("")
    return conn

"""
关闭数据库链接
Param: conn数据对象 = {psycopg2.extensions.connection}
"""
def close_database(conn):
    conn.close()
    print("Closed database successfull")

"""
处理读出的数据list整理成为df,并且存入hive
Param:  读取出的数据list列表={list}
        df列名={list}
        hive表存储开关={Int}
        hive表存储表名={Str}
        分区日期={Str}
        hive表列名={Str}
"""
def reorganizeAndSave(dataList,dfColName,hiveSwitch,hiveTableName,dateStr,hiveTableColName,testHdatabase):
    #判断数据集合是否为空
    if len(dataList) != 0:
        print("Postgre reading was done")
        """
        将list转化为rdd
        Param：list数据集合={list}
        Return: rdd={RDD}
        """
        rdd = sc.parallelize(dataList)
        """
        将rdd转化为df
        Param： rdd数据集合={RDD}
                listColName转化df的列名={list}
        Return: df={DataFrame}
        """
        df = sqlContext.createDataFrame(rdd,dfColName)
        if hiveSwitch == 1:
            """
            存为hive表
            Param:  hive表名={Str}
                    分区日期字符串={Str}
                    需要存储的df={DataFrame}
                    hive表内完整字段名及类型={Str}
            """
            print("start save:" + hiveTableName)
            # 设置动态分区
            sqlContext.setConf("hive.exec.dynamic.partition", "true")
            sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
            # 动态分区上线设置
            sqlContext.setConf("hive.exec.max.dynamic.partitions", "10000")
            # 使用orange库
            if testHdatabase == 1:
                sqlContext.sql("use orange")
            else:
                sqlContext.sql("use test")
            # 表若不存在新建Hive表
            sqlContext.sql(
                "CREATE TABLE IF NOT EXISTS " + hiveTableName + hiveTableColName + " PARTITIONED BY (day STRING)")
            # 增加日期分区字段
            dfTemp = df.withColumn("day", lit(dateStr))
            # 存为临时表
            dfTemp.registerTempTable("tempTable")
            # 从临时表读取数据存入hive
            sqlContext.sql("INSERT OVERWRITE TABLE " + hiveTableName + " PARTITION(day) select * from tempTable")
            print("Saving data to " + hiveTableName + " was done!")
    else:
        print("date:" + dateStr + " have no data")

"""
读取数据并存储数据主代码
Param: conn数据对象 = {psycopg2.extensions.connection}
"""
def get_save_info(conn,today):
    #创建一个光标，用于整个数据库使用Python编程
    cursor = conn.cursor()
    #订单表
    if listPostgreSwitch['orders'] == 1:
        print("Starting read postgre.goodsorders.public.orders")
        cursor.execute(
            'select id,order_code,buyer_user_id,buyer_company_id,seller_company_id,status,total_price,final_price,total_qty,out_qty,term_qty,receive_qty,remarks,COALESCE(to_char(completed_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),receiver,receiver_phone,receiver_area_id,receiver_area_name,replace(replace(receiver_address,chr(10),\'\'),chr(13),\'\'),payment_type,invoice_type,delivery_type,transport_type,pack_type,transport_company,transport_no,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by,partner_key,partner_order_code,prev_status,delivery_man,delivery_phone,delivery_number,COALESCE(to_char(delivery_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),loan_amount,repay_amount,brokerage,sync_flags,split_type,split_by,COALESCE(to_char(order_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(plan_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),customer_order_code,arrival_warehouse,pay_amount,alliance_id,final_amount,flags,type,trade_no,COALESCE(to_char(paid_time,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            'from orders '
            'where (status not in (4,5,6,7) or completed_time > \'' + today + '\') and to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listOrders = cursor.fetchall()
        reorganizeAndSave(
            listOrders,
            listDfColName['orders'],
            listHiveTable['orders'][0],
            listHiveTable['orders'][1],
            today,
            listHiveColName['orders'],
            testHdatabase
        )
    #历史订单表
    if listPostgreSwitch['orders_history'] == 1:
        print("Starting read postgre.goodsorders.public.orders")
        cursor.execute(
            'select id,order_code,buyer_user_id,buyer_company_id,seller_company_id,status,total_price,final_price,total_qty,out_qty,term_qty,receive_qty,remarks,COALESCE(to_char(completed_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),receiver,receiver_phone,receiver_area_id,receiver_area_name,replace(replace(receiver_address,chr(10),\'\'),chr(13),\'\'),payment_type,invoice_type,delivery_type,transport_type,pack_type,transport_company,transport_no,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by,partner_key,partner_order_code,prev_status,delivery_man,delivery_phone,delivery_number,COALESCE(to_char(delivery_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),loan_amount,repay_amount,brokerage,sync_flags,split_type,split_by,COALESCE(to_char(order_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(plan_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),customer_order_code,arrival_warehouse,pay_amount,alliance_id,final_amount,flags,type,trade_no,COALESCE(to_char(paid_time,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            'from orders '
            'where to_char(completed_time,\'YYYYMMDD\') = \'' + today + '\' and status in (4,5,6,7)'
        )
        conn.commit()
        listOrdersHistory = cursor.fetchall()
        reorganizeAndSave(
            listOrdersHistory,
            listDfColName['orders_history'],
            listHiveTable['orders_history'][0],
            listHiveTable['orders_history'][1],
            today,
            listHiveColName['orders_history'],
            testHdatabase
        )
    #订单日志表
    if listPostgreSwitch['order_logs'] == 1:
        print("Starting read postgre.goodsorders.public.order_logs")
        cursor.execute(
            'select id,order_id,operator,operation,COALESCE(to_char(time,\'YYYYMMDD HH24:MI:SS\'),\'\'),remarks,content,operator_type,COALESCE(to_char(operate_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),operate_place '
            'from order_logs '
            'where to_char(time,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listOrderLogs = cursor.fetchall()
        reorganizeAndSave(
            listOrderLogs,
            listDfColName['order_logs'],
            listHiveTable['order_logs'][0],
            listHiveTable['order_logs'][1],
            today,
            listHiveColName['order_logs'],
            testHdatabase
        )
    #订单详情表
    if listPostgreSwitch['order_details'] == 1:
        print("Starting read postgre.goodsorders.public.order_details")
        cursor.execute(
            'select id,order_id,order_code,buyer_company_id,seller_company_id,sku_id,property,replace(replace(std_name,chr(10),\'\'),chr(13),\'\'),veh_brand,oe_code,replace(replace(oe_name,chr(10),\'\'),chr(13),\'\'),sw_part_id,sw_oe_code,sw_brand,COALESCE(sw_production_place,\'\'),sw_oe_name,sw_veh_model,qty,COALESCE(out_qty,-99),COALESCE(term_qty,-99),COALESCE(receive_qty,-99),price,total_price,final_price,remarks,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by,replace(replace(sw_general_veh_models,chr(10),\'\'),chr(13),\'\'),sw_unit_name,std_oe_code,sw_sale_detail_id,status,alliance_part_id '
            ' from order_details '
            ' where order_id in (SELECT id from orders where (status not in (4,5,6,7) or completed_time > \'' + today + '\') and to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\') and order_code in (SELECT order_code from orders where (status not in (4,5,6,7) or completed_time > \'' + today + '\') and to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\')'
        )
        conn.commit()
        listOrderDetails = cursor.fetchall()
        reorganizeAndSave(
            listOrderDetails,
            listDfColName['order_details'],
            listHiveTable['order_details'][0],
            listHiveTable['order_details'][1],
            today,
            listHiveColName['order_details'],
            testHdatabase
        )
    #订单详情历史表
    if listPostgreSwitch['order_details_history'] == 1:
        print("Starting read postgre.goodsorders.public.order_details")
        cursor.execute(
            'select id,order_id,order_code,buyer_company_id,seller_company_id,sku_id,property,replace(replace(std_name,chr(10),\'\'),chr(13),\'\'),veh_brand,oe_code,replace(replace(oe_name,chr(10),\'\'),chr(13),\'\'),sw_part_id,sw_oe_code,sw_brand,COALESCE(sw_production_place,\'\'),sw_oe_name,sw_veh_model,qty,COALESCE(out_qty,-99),COALESCE(term_qty,-99),COALESCE(receive_qty,-99),price,total_price,final_price,remarks,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by,replace(replace(sw_general_veh_models,chr(10),\'\'),chr(13),\'\'),sw_unit_name,std_oe_code,sw_sale_detail_id,status,alliance_part_id '
            'from order_details '
            'where to_char(updated_at,\'YYYYMMDD\') = \'' + today + '\' and status in (4,5,6,7)'
        )
        conn.commit()
        listOrderDetailsHistory = cursor.fetchall()
        reorganizeAndSave(
            listOrderDetailsHistory,
            listDfColName['order_details_history'],
            listHiveTable['order_details_history'][0],
            listHiveTable['order_details_history'][1],
            today,
            listHiveColName['order_details_history'],
            testHdatabase
        )
    # 订单详情日志
    if listPostgreSwitch['order_detail_logs'] == 1:
        print("Starting read postgre.goodsorders.public.order_detail_logs")
        cursor.execute(
            'select l.id,l.order_detail_id,d.price,l.out_qty,l.term_qty,l.receive_qty,l.operator,l.operate_type,l.operation,COALESCE(to_char(l.time,\'YYYYMMDD HH24:MI:SS\'),\'\'),jsonb_out(l.related_ids),l.order_code,l.warehouse,d.order_code,d.buyer_company_id,d.seller_company_id,d.sku_id,COALESCE(to_char(d.created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),d.created_by,replace(replace(d.std_name,chr(10),\'\'),chr(13),\'\'),d.veh_brand,d.oe_code,replace(replace(d.oe_name,chr(10),\'\'),chr(13),\'\'),d.sw_brand,replace(replace(d.sw_production_place,chr(10),\'\'),chr(13),\'\'),d.sw_oe_name '
            ' from order_detail_logs as l'
            ' left join order_details as d'
            ' on l.order_detail_id = d.id'
            ' where to_char(l.time,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listOrderDetailLogs = cursor.fetchall()
        reorganizeAndSave(
            listOrderDetailLogs,
            listDfColName['order_detail_logs'],
            listHiveTable['order_detail_logs'][0],
            listHiveTable['order_detail_logs'][1],
            today,
            listHiveColName['order_detail_logs'],
            testHdatabase
        )
    # 物流记录表
    if listPostgreSwitch['logistics'] == 1:
        print("Starting read postgre.goodsorders.public.logistics")
        cursor.execute(
            'select id,logistics_code,business_type,order_code,other_logistics_code,out_order_code,out_warehouse,company_id,company_name,contact,contact_phone,plate_number,status,COALESCE(to_char(order_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(departure_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(finish_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),total_time,signer,remarks,jsonb_out(ship_list),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by,area_id,replace(replace(address,chr(10),\'\'),chr(13),\'\') '
            'from logistics '
            'where (to_char(finish_time,\'YYYYMMDD\') = \'00010101\' or finish_time > \'' + today + '\') and to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listLogistics = cursor.fetchall()
        reorganizeAndSave(
            listLogistics,
            listDfColName['logistics'],
            listHiveTable['logistics'][0],
            listHiveTable['logistics'][1],
            today,
            listHiveColName['logistics'],
            testHdatabase
        )
    # 物流记录历史表
    if listPostgreSwitch['logistics_history'] == 1:
        print("Starting read postgre.goodsorders.public.logistics")
        cursor.execute(
            'select id,logistics_code,business_type,order_code,other_logistics_code,out_order_code,out_warehouse,company_id,company_name,contact,contact_phone,plate_number,status,COALESCE(to_char(order_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(departure_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(finish_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),total_time,signer,remarks,jsonb_out(ship_list),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by,area_id,replace(replace(address,chr(10),\'\'),chr(13),\'\') '
            'from logistics '
            'where to_char(finish_time,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listLogisticsHistory = cursor.fetchall()
        reorganizeAndSave(
            listLogisticsHistory,
            listDfColName['logistics_history'],
            listHiveTable['logistics_history'][0],
            listHiveTable['logistics_history'][1],
            today,
            listHiveColName['logistics_history'],
            testHdatabase
        )
    #公司库存表
    if listPostgreSwitch['company_stocks'] == 1:
        print("Starting read postgre.goodsorders.public.company_stocks")
        cursor.execute(
            'select id,company_id,sw_part_id,property,qty,COALESCE(to_char(time,\'YYYYMMDD HH24:MI:SS\'),\'\'),locked_qty,alliance_part_id,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),jsonb_out(partners),veh_brand,sw_brand' \
            ' from company_stocks' \
            ' where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\' or to_char(updated_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listCompanyStocks = cursor.fetchall()
        reorganizeAndSave(
            listCompanyStocks,
            listDfColName['company_stocks'],
            listHiveTable['company_stocks'][0],
            listHiveTable['company_stocks'][1],
            today,
            listHiveColName['company_stocks'],
            testHdatabase
        )
    # 公司商品表
    if listPostgreSwitch['company_skus'] == 1:
        print("Starting read postgre.goodsorders.public.company_skus")
        cursor.execute(
            'select id,company_id,sw_part_id,property,price_alliance,price_allot,price_retail,price_p,price_p1,price_p2,price_p3,price_p4,price_p5,price_p6,price_p7,price_p8,price_p9,price_p10,replace(replace(remarks,chr(10),\'\'),chr(13),\'\'),COALESCE(to_char(time,\'YYYYMMDD HH24:MI:SS\'),\'\'),alliance_part_id,jsonb_out(partners),has_stock,veh_brand,oe_code,sw_brand,sw_production_place,replace(replace(oe_name,chr(10),\'\'),chr(13),\'\'),replace(replace(std_name,chr(10),\'\'),chr(13),\'\'),replace(replace(veh_series,chr(10),\'\'),chr(13),\'\'),price4s,sw_oe_name,sw_oe_code,sw_veh_model,replace(replace(sw_general_veh_models,chr(10),\'\'),chr(13),\'\'),sw_guide_price,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),flags,type,sw_model ' \
            ' from company_skus' \
            ' where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\' or to_char(updated_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listLogisticsHistory = cursor.fetchall()
        reorganizeAndSave(
            listLogisticsHistory,
            listDfColName['company_skus'],
            listHiveTable['company_stocks'][0],
            listHiveTable['company_skus'][1],
            today,
            listHiveColName['company_skus'],
            testHdatabase
        )
    # 公司配件表
    if listPostgreSwitch['company_parts'] == 1:
        print("Starting read postgre.goodsorders.public.company_parts")
        cursor.execute(
            'select id,company_id,sw_part_id,alliance_part_id,jsonb_out(partners),veh_brand,oe_code,sw_brand,sw_production_place,flags,replace(replace(oe_name,chr(10),\'\'),chr(13),\'\'),replace(replace(std_name,chr(10),\'\'),chr(13),\'\'),replace(replace(veh_series,chr(10),\'\'),chr(13),\'\'),price4s,sw_oe_name,sw_oe_code,sw_manu_code,sw_name_eng,sw_mnemonic,sw_wbh,sw_guide_price,sw_veh_model,replace(replace(sw_general_veh_models,chr(10),\'\'),chr(13),\'\'),sw_category,sw_model,sw_unit_name,sw_pos_code,sw_length,sw_width,sw_height,sw_volume,sw_weight,sw_engine_no,replace(replace(sw_remarks,chr(10),\'\'),chr(13),\'\'),sw_stop_use,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by,std_oe_code,sw_min_buy_qty,type,insur_cert_type '
            'from company_parts '
            'where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\' or to_char(updated_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listLogisticsHistory = cursor.fetchall()
        reorganizeAndSave(
            listLogisticsHistory,
            listDfColName['company_parts'],
            listHiveTable['company_parts'][0],
            listHiveTable['company_parts'][1],
            today,
            listHiveColName['company_parts'],
            testHdatabase
        )
    # 联盟配件
    if listPostgreSwitch['alliance_parts'] == 1:
        print("Starting read postgre.goods_orders.public.alliance_parts")
        cursor.execute(
            'select id,alliance_id,alliance_part_id,veh_brand,oe_code,sw_brand,sw_production_place,replace(replace(oe_name,chr(10),\'\'),chr(13),\'\'),sw_oe_name,sw_oe_code,std_oe_code,replace(replace(std_name,chr(10),\'\'),chr(13),\'\'),replace(replace(veh_series,chr(10),\'\'),chr(13),\'\'),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            ' from alliance_parts '
            ' where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listAllianceParts = cursor.fetchall()
        reorganizeAndSave(
            listAllianceParts,
            listDfColName['alliance_parts'],
            listHiveTable['alliance_parts'][0],
            listHiveTable['alliance_parts'][1],
            today,
            listHiveColName['alliance_parts'],
            testHdatabase
        )
    # 联盟配件id
    if listPostgreSwitch['alliance_part_ids'] == 1:
        print("Starting read postgre.goods_orders.public.alliance_part_ids")
        cursor.execute(
            'select id,veh_brand,oe_code,brand,production_place,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            'from alliance_part_ids '
            'where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listAlliancePartIds = cursor.fetchall()
        reorganizeAndSave(
            listAlliancePartIds,
            listDfColName['alliance_part_ids'],
            listHiveTable['alliance_part_ids'][0],
            listHiveTable['alliance_part_ids'][1],
            today,
            listHiveColName['alliance_part_ids'],
            testHdatabase
        )
    # 未认证公司上传的配件信息及库存表
    if listPostgreSwitch['company_private_skus'] == 1:
        print("Starting read postgre.goodsorders.public.company_private_skus")
        cursor.execute(
            'select id,company_id,replace(replace(oe_name,chr(10),\'\'),chr(13),\'\'),oe_code,brand,production_place,property,unit,qty,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\')'
            ' from company_private_skus'
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listCompanyPrivateSkus = cursor.fetchall()
        reorganizeAndSave(
            listCompanyPrivateSkus,
            listDfColName['company_private_skus'],
            listHiveTable['company_private_skus'][0],
            listHiveTable['company_private_skus'][1],
            today,
            listHiveColName['company_private_skus'],
            testHdatabase
        )
    # 上传的记录表
    if listPostgreSwitch['company_private_sku_logs'] == 1:
        print("Starting read postgre.goodsorders.public.company_private_sku_logs")
        cursor.execute(
            'select id,sku_id,type,opt_qty,qty,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            ' from company_private_sku_logs'
            ' where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listCompanyPrivateSkuLogs = cursor.fetchall()
        reorganizeAndSave(
            listCompanyPrivateSkuLogs,
            listDfColName['company_private_sku_logs'],
            listHiveTable['company_private_sku_logs'][0],
            listHiveTable['company_private_sku_logs'][1],
            today,
            listHiveColName['company_private_sku_logs'],
            testHdatabase
        )
    #补货邀约表
    if listPostgreSwitch['replenish_invitations'] == 1:
        print("Starting read postgre.goodsorders.public.replenish_invitations")
        cursor.execute(
            'select id,invitation_code,inviter,receiver,status,COALESCE(to_char(invite_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(receive_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\')'
            ' from replenish_invitations'
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listReplenishInvitations = cursor.fetchall()
        reorganizeAndSave(
            listReplenishInvitations,
            listDfColName['replenish_invitations'],
            listHiveTable['replenish_invitations'][0],
            listHiveTable['replenish_invitations'][1],
            today,
            listHiveColName['replenish_invitations'],
            testHdatabase
        )
    #补货关系表
    if listPostgreSwitch['replenish_relations'] == 1:
        print("Starting read postgre.goodsorders.public.replenish_relations")
        cursor.execute(
            'select id,inviter,receiver,status,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\')'
            ' from replenish_relations'
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listReplenishRelations = cursor.fetchall()
        reorganizeAndSave(
            listReplenishRelations,
            listDfColName['replenish_relations'],
            listHiveTable['replenish_relations'][0],
            listHiveTable['replenish_relations'][1],
            today,
            listHiveColName['replenish_relations'],
            testHdatabase
        )
    # 补货配件设置表
    if listPostgreSwitch['replenish_skus'] == 1:
        print("Starting read postgre.goodsorders.public.replenish_skus")
        cursor.execute(
            'select id,replenish_invitation_id,inviter,receiver,sku_id,std_oe_code,std_name,status,replenish_status,lower_limit,upper_limit,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\')'
            ' from replenish_skus'
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listReplenishSkus = cursor.fetchall()
        reorganizeAndSave(
            listReplenishSkus,
            listDfColName['replenish_skus'],
            listHiveTable['replenish_skus'][0],
            listHiveTable['replenish_skus'][1],
            today,
            listHiveColName['replenish_skus'],
            testHdatabase
        )
    # 配件库存授权关系表
    if listPostgreSwitch['sku_auth_relations'] == 1:
        print("Starting read postgre.goodsorders.public.sku_auth_relations")
        cursor.execute(
            'select id,company_id,supplier_id,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\')'
            ' from sku_auth_relations'
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listSkuAuthRelations = cursor.fetchall()
        reorganizeAndSave(
            listSkuAuthRelations,
            listDfColName['sku_auth_relations'],
            listHiveTable['sku_auth_relations'][0],
            listHiveTable['sku_auth_relations'][1],
            today,
            listHiveColName['sku_auth_relations'],
            testHdatabase
        )

#主函数
if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    PARAMS = None
    """
    默认开始及结束日期日期={Str}
    均为昨天，只跑昨天一天数据
    """
    defualt_date = date_str(datetime.date.today())
    parser = argparse.ArgumentParser()
    parser.add_argument('--startdate', type=str, default=defualt_date,
                        help='start date')
    parser.add_argument('--enddate', type=str, default=defualt_date,
                        help='end date')
    PARAMS, _ = parser.parse_known_args()
    # 打开数据库
    if testPdatabase == 1:
        conn = open_database('goods_orders')
    else:
        conn = open_qa_database('goods_orders_qa')
    # 定义今天为开始日期
    today = date_str(str_date(PARAMS.startdate) - datetime.timedelta(days=1))
    # 当开始日期<=结束日期时，运行该程序。如果开始日期=结束日期，break种植
    while (today <= date_str(str_date(PARAMS.enddate) - datetime.timedelta(days=1))):
        print("start reading " + today + " datas")
        get_save_info(conn, today)
        if today == date_str(str_date(PARAMS.enddate) - datetime.timedelta(days=1)):
            break
        print("read " + today + " datas finished")
        today = date_str(str_date(today) + datetime.timedelta(days=1))
        print("")
        print("")
    # 关闭数据库
    close_database(conn)
    sc.stop()
    print("program is done at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))