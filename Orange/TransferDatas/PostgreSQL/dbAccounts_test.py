"""
by:pkh
此代码用于读取postgresql中accounts库数据并转化存储于hive表中
因为库最好不要频繁开关，所以打开一次处理所有表格完成再关闭
"""

import psycopg2
from pyspark import SparkContext,SparkConf,HiveContext
from pyspark.sql.functions import *
import datetime
import argparse
from colList import dbAccounts_colList as listDfColName
from hiveCol import dbAccounts_hiveCol as listHiveColName


#spark入口
conf = SparkConf().setAppName("Transfer_PostgreSql_AccountsDB")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

#日期处理函数群
def date_str(date):
    return date.strftime("%Y%m%d")
def str_date(str):
    return datetime.datetime.strptime(str, "%Y%m%d").date()
def date_str_sep(date):
    return date.strftime("%Y-%m-%d")

'''
是否是测试数据库:
1：正常环境（数据库：orange
2：测试环境（数据库：test
'''
testHdatabase = 1
'''
是否是测试数据库:
1：正常环境（数据库：accounts
2：测试环境（数据库：accounts_qa
'''
testPdatabase = 1

"""
读取PostgreSql的表名={Dict}
key：数据源表名={Str}
value:对应读取Postgre表名创建开关,1存储={Int}
"""
listPostgreSwitch = {
    'users' :10,#用户信息表
    'companies' : 10,#公司信息表
    'staffs' : 10,#员工信息表
    'addresses' : 10,#联系地址表
    'alliances' : 10, #联盟信息表
    'alliance_logs' : 10,#联盟信息变更日志
    'alliance_customers' : 10,#联盟客户表
    'alliance_members' : 10,#联盟成员表
    'alliance_member_logs' : 10,#联盟成员变更日志
    'alliance_member_settings' : 10,#联盟成员设置表
    'alliance_roles' : 10,#联盟角色表
    'alliance_promoters' : 10,#联盟合格推广人
    'alliance_ally_promotes' : 10, #联盟联合推广表
    'clients' : 10, #客户端表
    'prices' : 10,#价格体系表
    'company_partners' : 10,#电商品台表
    'notices' : 10,#消息通知表
    'notice_receivers' : 10, #消息接收对象表
    'customers' : 1, #供商关系表
    'customer_logs' : 10,#供商关系日志表
    'promote_records' : 10,#推广记录表
    'accounts_orders' : 10,#账号订单表
    'accounts_orders_history' : 10,# 账号订单历史列表
    'services' : 10,#服务购买表
    'codes' : 10,#锐派R码表
    'codes_history' : 10,#锐派R码历史表
    'applications' : 10, #用户todos表
    'admin_applications' : 10, #运营后台todos表
    'alliance_payments' : 10, #联盟支付表
    'alliance_fee_class' : 1, #联盟费用类型表
}

"""
存储Hive的表名={Dict}
key：数据源表名={Str}
value:创建hive表开关及存为hive表名={list}
    Parem：[是否启动存储function，1存储={Int}，
            存入hiv的表名：{Str}]
"""
listHiveTable = {
    'users' : [1,'ODS_Psql_Users'],#用户信息表
    'companies' : [1,'ODS_Psql_Companies'],#公司信息表
    'staffs' : [1,'ODS_Psql_Staffs'],#员工信息表
    'addresses' : [1,'ODS_Psql_Addresses'],#联系地址表
    'alliances' : [1,'ODS_Psql_Alliances'],#联盟信息表
    'alliance_logs' : [1,'ODS_Psql_Alliance_Logs'],#联盟信息变更日志
    'alliance_customers' : [1,'ODS_Psql_Alliance_Customers'],#联盟客户表
    'alliance_members' : [1,'ODS_Psql_Alliances_Members'],#联盟成员表
    'alliance_member_logs' : [1,'ODS_Psql_Alliance_Member_Logs'],#联盟成员变更日志
    'alliance_member_settings' : [1,'ODS_Psql_Alliance_Member_Settings'],#联盟成员设置表
    'alliance_roles' : [1,'ODS_Psql_Alliance_Roles'],#联盟角色表
    'alliance_promoters' : [1,'ODS_Psql_Alliance_Promoters'],#联盟合格推广人
    'alliance_ally_promotes' : [1,'ODS_Psql_Alliance_Ally_Promotes'],#联盟联合推广表
    'clients' : [1,'ODS_Psql_Client'], #客户端表
    'prices' : [1,'ODS_Psql_Prices'],#价格体系表
    'company_partners' : [1,'ODS_Psql_Company_Partners'],#电商品台表
    'notices' : [1,'ODS_Psql_Notices'],#消息通知表
    'notice_receivers' : [1,'ODS_Psql_Notice_Receivers'],#消息接收对象表
    'customers': [1,'ODS_Psql_Customers'],#供商关系表
    'customer_logs': [1,'ODS_Psql_Customer_Logs'],#供商关系日志表
    'promote_records': [1,'ODS_Psql_Promote_Records'],#推广记录表
    'accounts_orders': [1,'ODS_Psql_Accounts_Orders'],#账号订单表
    'accounts_orders_history': [1,'ODS_Psql_Accounts_Orders_History'],# 账号订单历史列表
    'services': [1,'ODS_Psql_Services'],#服务购买表
    'codes': [1,'ODS_Psql_Codes'],#锐派R码表
    'codes_history' : [1,'ODS_Psql_Codes_History'],#锐派R码历史表
    'applications' : [1,'ODS_Psql_Applications'],#用户todos表
    'admin_applications' : [1,'ODS_Psql_Admin_Applications'],#运营后台todos表
    'alliance_payments' : [1,'ODS_Psql_Alliance_Payments'],#联盟支付表
    'alliance_fee_class' : [1,'ODS_Psql_Alliance_Fee_Class'], #联盟费用类型表
}

"""
打开一个链接到PostgreSQL数据库，连接成功返回一个数据对象
return: conn数据对象 = {psycopg2.extensions.connection}
"""
def open_qa_database(databaseName):
    conn = psycopg2.connect(database=databaseName,
                            user='pgdata',
                            password='Tonglianinfo2018_db',
                            host='113.31.135.250',
                            port='21770')
    print("Opened database successfull")
    print("")
    return conn
def open_database(databaseName):
    conn = psycopg2.connect(database=databaseName,
                            user='cdhuser',
                            password='Hztl&2019@238Cdh',
                            host='113.31.135.238',
                            port='21768')
    print("Opened database successfull")
    print("")
    return conn

"""
关闭数据库链接
Param: conn数据对象 = {psycopg2.extensions.connection}
"""
def close_database(conn):
    conn.close()
    print("Closed database successfull")

"""
处理读出的数据list整理成为df,并且存入hive
Param:  读取出的数据list列表={list}
        df列名={list}
        hive表存储开关={Int}
        hive表存储表名={Str}
        分区日期={Str}
        hive表列名={Str}
"""
def reorganizeAndSave(dataList,dfColName,hiveSwitch,hiveTableName,dateStr,hiveTableColName,testHdatabase):
    #判断数据集合是否为空
    if len(dataList) != 0:
        print("Postgre reading was done")
        """
        将list转化为rdd
        Param：list数据集合={list}
        Return: rdd={RDD}
        """
        rdd = sc.parallelize(dataList)
        """
        将rdd转化为df
        Param： rdd数据集合={RDD}
                listColName转化df的列名={list}
        Return: df={DataFrame}
        """
        df = sqlContext.createDataFrame(rdd,dfColName)
        if hiveSwitch == 1:
            """
            存为hive表
            Param:  hive表名={Str}
                    分区日期字符串={Str}
                    需要存储的df={DataFrame}
                    hive表内完整字段名及类型={Str}
            """
            print("start save:" + hiveTableName)
            # 设置动态分区
            sqlContext.setConf("hive.exec.dynamic.partition", "true")
            sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
            # 动态分区上线设置
            sqlContext.setConf("hive.exec.max.dynamic.partitions", "10000")
            # 使用orange库
            if testHdatabase == 1:
                sqlContext.sql("use orange")
            else:
                sqlContext.sql("use test")
            # 表若不存在新建Hive表
            sqlContext.sql(
                "CREATE TABLE IF NOT EXISTS " + hiveTableName + hiveTableColName + " PARTITIONED BY (day STRING)")
            # 增加日期分区字段
            dfTemp = df.withColumn("day", lit(dateStr))
            # 存为临时表
            dfTemp.registerTempTable("tempTable")
            # 从临时表读取数据存入hive
            sqlContext.sql("INSERT OVERWRITE TABLE " + hiveTableName + " PARTITION(day) select * from tempTable")
            print("Saving data to " + hiveTableName + " was done!")
    else:
        print("date:" + dateStr + " have no data")


def getlist(cursor,psql,conn):
    list = []
    offsetNum = 0
    while True:
        cursor.execute(psql+ " order by id limit 500000 offset " + str(offsetNum))
        conn.commit()
        listpart = cursor.fetchall()
        if len(listpart) == 0:
            break
        offsetNum = offsetNum + 500000
        list = list + listpart
    return list

"""
读取数据并存储数据主代码
Param: conn数据对象 = {psycopg2.extensions.connection}
"""
def get_save_info(conn,today):
    #创建一个光标，用于整个数据库使用Python编程
    cursor = conn.cursor()
    #用户信息表Users读取，并存储
    if listPostgreSwitch['users'] == 1:
        print("Starting read postgre.accounts.public.users")
        psql = 'select id,phone,account,name,status,register_from,area_id,wechat_open_id,wechat_nickname,COALESCE(to_char(create_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\') ' \
               ' from users ' \
               ' where to_char(create_at,\'YYYYMMDD\') <= \'' + today + '\''
        listUsers = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listUsers,
            listDfColName['users'],
            listHiveTable['users'][0],
            listHiveTable['users'][1],
            today,
            listHiveColName['users'],
            testHdatabase
        )
    #公司信息表Companies读取，并存储
    if listPostgreSwitch['companies'] == 1:
        print("Starting read postgre.accounts.public.companies")
        psql = 'select id,name,short_name,status,area_id,area_name,address,parent_id,db_account,founded_at,type,category,biz_level,biz_licence_code,opening_licence_code,credit_code,mnemonic,contacts,contacts1,contacts2,contacts3,phone,phone1,phone2,phone3,email,wechat_pa,website,remarks,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),jsonb_out(veh_brands),jsonb_out(brands),jsonb_out(sw_params),jsonb_out(veh_brand_prices),jsonb_out(brand_prices),flags,replace(replace(bank_account,chr(10),\'\'),chr(13),\'\'),replace(replace(bank_name,chr(10),\'\'),chr(13),\'\'),replace(replace(emblem,chr(10),\'\'),chr(13),\'\'),replace(replace(invoice_title,chr(10),\'\'),chr(13),\'\'),replace(replace(invoice_phone,chr(10),\'\'),chr(13),\'\'),replace(replace(staff_invite_code,chr(10),\'\'),chr(13),\'\'),init_area_id ' \
               ' from companies ' \
               ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        listCompanies = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listCompanies,
            listDfColName['companies'],
            listHiveTable['companies'][0],
            listHiveTable['companies'][1],
            today,
            listHiveColName['companies'],
            testHdatabase
        )
    #员工信息表读取，并存储
    if listPostgreSwitch['staffs'] == 1:
        print("Starting read postgre.accounts.public.staffs")
        psql = 'select id,company_id,user_id,staff_number,staff_name,type,status,sw_user_id,orders,carts,wants,flags,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(joined_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(left_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),replace(replace(real_name,chr(10),\'\'),chr(13),\'\'),replace(replace(diploma,chr(10),\'\'),chr(13),\'\'),replace(replace(cert_type,chr(10),\'\'),chr(13),\'\'),replace(replace(cert_number,chr(10),\'\'),chr(13),\'\'),gender,COALESCE(to_char(birthday,\'YYYYMMDD\'),\'\'),replace(replace(address,chr(10),\'\'),chr(13),\'\'),replace(replace(refusing_reason,chr(10),\'\'),chr(13),\'\'),replace(replace(remark,chr(10),\'\'),chr(13),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),sales ' \
               ' from staffs ' \
               ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        listStaffs = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listStaffs,
            listDfColName['staffs'],
            listHiveTable['staffs'][0],
            listHiveTable['staffs'][1],
            today,
            listHiveColName['staffs'],
            testHdatabase
        )
    #联系地址表读取，并存储
    if listPostgreSwitch['addresses']==1:
        print("Starting read postgre.accounts.public.addresses")
        psql = 'select id,object_id,object_type,is_default,area_id,area_name,address,receiver,phone,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\') ' \
               ' from addresses ' \
               ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        listAddresses = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listAddresses,
            listDfColName['addresses'],
            listHiveTable['addresses'][0],
            listHiveTable['addresses'][1],
            today,
            listHiveColName['addresses'],
            testHdatabase
        )
    #联盟信息表读取，并存储
    if listPostgreSwitch['alliances'] == 1:
        print("Starting read postgre.accounts.public.alliances")
        psql = 'select id,name,owner,area_id,area_name,COALESCE(to_char(founded_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),share_all_goods,share_all_customers,settle_type,invoice_type,settle_date,payment_days,replace(replace(requirement,chr(10),\'\'),chr(13),\'\'),replace(replace(intro,chr(10),\'\'),chr(13),\'\'),status,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),jsonb_out(veh_brands),jsonb_out(brands),invitation_code,flags,replace(replace(member_rule,chr(10),\'\'),chr(13),\'\'),replace(replace(customer_rule,chr(10),\'\'),chr(13),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,updated_by,explain ' \
               ' from alliances ' \
               ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        listAlliances = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listAlliances,
            listDfColName['alliances'],
            listHiveTable['alliances'][0],
            listHiveTable['alliances'][1],
            today,
            listHiveColName['alliances'],
            testHdatabase
        )
    #联盟信息变更日志表读取，并存储,仅取当天数据
    if listPostgreSwitch['alliance_logs'] == 1:
        print("Starting read postgre.accounts.public.alliance_logs")
        cursor.execute(
            'select id,alliance_id,COALESCE(to_char(time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(operator,-99),operation,jsonb_out(remarks) '
            'from alliance_logs '
            'where to_char(time,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        ListAlliancesLogs = cursor.fetchall()
        reorganizeAndSave(
            ListAlliancesLogs,
            listDfColName['alliance_logs'],
            listHiveTable['alliance_logs'][0],
            listHiveTable['alliance_logs'][1],
            today,
            listHiveColName['alliance_logs'],
            testHdatabase
        )
    # 联盟客户表读取，并存储
    if listPostgreSwitch['alliance_customers'] == 1:
        print("Starting read postgre.accounts.public.alliance_customers")
        psql = 'select id,customer_id,customer_type,alliance_id,status,jsonb_out(areas),COALESCE (to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),jsonb_out(ally_alliance_ids),COALESCE (to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\') ' \
               ' from alliance_customers ' \
               ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        listAlliancesCustomer = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listAlliancesCustomer,
            listDfColName['alliance_customers'],
            listHiveTable['alliance_customers'][0],
            listHiveTable['alliance_customers'][1],
            today,
            listHiveColName['alliance_customers'],
            testHdatabase
        )
    #联盟成员表读取，并存储
    if listPostgreSwitch['alliance_members'] == 1:
        print("Starting read postgre.accounts.public.alliance_members")
        psql = 'select id,alliance_id,member_id,COALESCE(to_char(join_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),price_type,jsonb_out(area_range),status,role_id,price_level_id,jsonb_out(area_sales),jsonb_out(area_buy),jsonb_out(accept_area),jsonb_out(veh_brands),jsonb_out(brands),jsonb_out(ally_alliance_ids),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,updated_by,type ' \
               ' from alliance_members ' \
               ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        listAlliancesMembers = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listAlliancesMembers,
            listDfColName['alliance_members'],
            listHiveTable['alliance_members'][0],
            listHiveTable['alliance_members'][1],
            today,
            listHiveColName['alliance_members'],
            testHdatabase
        )
    #联盟成员变更日志读取，并存储
    if listPostgreSwitch['alliance_member_logs'] == 1:
        print("Starting read postgre.accounts.public.alliance_member_logs")
        cursor.execute(
            'select id,alliance_id,member_id,COALESCE(to_char(time,\'YYYYMMDD HH24:MI:SS\'),\'\'),operator,operation,replace(replace(remarks,chr(10),\'\'),chr(13),\'\') '
            'from alliance_member_logs '
            'where to_char(time,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listAllianceMemberLogs = cursor.fetchall()
        reorganizeAndSave(
            listAllianceMemberLogs,
            listDfColName['alliance_member_logs'],
            listHiveTable['alliance_member_logs'][0],
            listHiveTable['alliance_member_logs'][1],
            today,
            listHiveColName['alliance_member_logs'],
            testHdatabase
        )
    #联盟成员设置表读取，并存储
    if listPostgreSwitch['alliance_member_settings'] == 1:
        print("Starting read postgre.accounts.public.alliance_member_settings")
        psql = 'select id,alliance_id,member_id,is_receive,zhong_sheng_id,apc_id,jsonb_out(brands),jsonb_out(city_ids),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by ' \
               ' from alliance_member_settings ' \
               ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        listAlliancesMembers = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listAlliancesMembers,
            listDfColName['alliance_member_settings'],
            listHiveTable['alliance_member_settings'][0],
            listHiveTable['alliance_member_settings'][1],
            today,
            listHiveColName['alliance_member_settings'],
            testHdatabase
        )
    #联盟角色表读取，并存储
    if listPostgreSwitch['alliance_roles'] == 1:
        print("Starting read postgre.accounts.public.alliance_roles")
        psql = 'select id,name,alliance_id,status,flags,innate,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\') ' \
               ' from alliance_roles ' \
               ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        listAlliancesRoles = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listAlliancesRoles,
            listDfColName['alliance_roles'],
            listHiveTable['alliance_roles'][0],
            listHiveTable['alliance_roles'][1],
            today,
            listHiveColName['alliance_roles'],
            testHdatabase
        )
    #联盟合格推广人读取，并存储
    if listPostgreSwitch['alliance_promoters'] == 1:
        print("Starting read postgre.accounts.public.alliance_promoters")
        cursor.execute(
            'select id,replace(replace(name,chr(10),\'\'),chr(13),\'\'),user_id,alliance_id,company_id,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            ' from alliance_promoters '
            ' where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listAlliancesPromoters = cursor.fetchall()
        reorganizeAndSave(
            listAlliancesPromoters,
            listDfColName['alliance_promoters'],
            listHiveTable['alliance_promoters'][0],
            listHiveTable['alliance_promoters'][1],
            today,
            listHiveColName['alliance_promoters'],
            testHdatabase
        )
    #联盟联合推广表读取，并存储
    if listPostgreSwitch['alliance_ally_promotes'] == 1:
        print("Starting read postgre.accounts.public.alliance_ally_promotes")
        cursor.execute(
            'select id,initiative_id,passive_id,bind_from,jsonb_out(referrer_ids),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            ' from alliance_ally_promotes '
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listAlliancesAllyPromotes = cursor.fetchall()
        reorganizeAndSave(
            listAlliancesAllyPromotes,
            listDfColName['alliance_ally_promotes'],
            listHiveTable['alliance_ally_promotes'][0],
            listHiveTable['alliance_ally_promotes'][1],
            today,
            listHiveColName['alliance_ally_promotes'],
            testHdatabase
        )
    #客户端表读取，并存储
    if listPostgreSwitch['clients'] == 1:
        print("Starting read postgre.accounts.public.clients")
        cursor.execute(
            'select id,user_id,client_id,os,alliance_id '
            'from clients'
        )
        conn.commit()
        listClients = cursor.fetchall()
        reorganizeAndSave(
            listClients,
            listDfColName['clients'],
            listHiveTable['clients'][0],
            listHiveTable['clients'][1],
            today,
            listHiveColName['clients'],
            testHdatabase
        )
    #价格体系表读取，并存储
    if listPostgreSwitch['prices'] == 1:
        print("Starting read postgre.accounts.public.prices")
        cursor.execute(
            'select id,client_id,client_type,name,jsonb_out(t_prices),payment_type,invoice_type,debt_limit,debt_day,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),brand_name,price_type,discount,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            ' from prices '
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listPrices = cursor.fetchall()
        reorganizeAndSave(
            listPrices,
            listDfColName['prices'],
            listHiveTable['prices'][0],
            listHiveTable['prices'][1],
            today,
            listHiveColName['prices'],
            testHdatabase
        )
    #电商品台表读取，并存储
    if listPostgreSwitch['company_partners'] == 1:
        print("Starting read postgre.accounts.public.company_partners")
        cursor.execute(
            'select id,company_id,replace(replace(partner_key,chr(10),\'\'),chr(13),\'\'),partner_name,dealer_id,status,financial_product_status,price_type,share_goods,jsonb_out(part_properties),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),region,warehouse '
            ' from company_partners '
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listCompanyPartners = cursor.fetchall()
        reorganizeAndSave(
            listCompanyPartners,
            listDfColName['company_partners'],
            listHiveTable['company_partners'][0],
            listHiveTable['company_partners'][1],
            today,
            listHiveColName['company_partners'],
            testHdatabase
        )
    #电商品台表读取，并存储
    if listPostgreSwitch['notices'] == 1:
        print("Starting read postgre.accounts.public.notices")
        cursor.execute(
            'select id,type,title,subtitle,content,association_type,association,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),exclusive_alliance_id '
            'from notices '
            'where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listNotices = cursor.fetchall()
        reorganizeAndSave(
            listNotices,
            listDfColName['notices'],
            listHiveTable['notices'][0],
            listHiveTable['notices'][1],
            today,
            listHiveColName['notices'],
            testHdatabase
        )
    #消息接收对象表读取，并存储
    if listPostgreSwitch['notice_receivers'] == 1:
        print("Starting read postgre.accounts.public.notice_receivers")
        cursor.execute(
            'select id,message_id,receiver_id,status,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            ' from notice_receivers '
            ' where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\' or to_char(updated_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listNoticeReceivers = cursor.fetchall()
        reorganizeAndSave(
            listNoticeReceivers,
            listDfColName['notice_receivers'],
            listHiveTable['notice_receivers'][0],
            listHiveTable['notice_receivers'][1],
            today,
            listHiveColName['notice_receivers'],
            testHdatabase
        )
    #供商关系表读取，并存储
    if listPostgreSwitch['customers'] == 1:
        print("Starting read postgre.accounts.public.customers")
        psql = 'select id,company_id,customer_id,customer_type,status,sw_customer_id,sw_supplier_id,price_type,payment_type,payment_days,reconciliation,line_credit,invoice_type,delivery_type,transport_type,discount,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),source,jsonb_out(alliances),use_level,jsonb_out(veh_brands),jsonb_out(brands),flags,jsonb_out(special_sku),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\') ' \
               ' from customers ' \
               ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        listCustomers = getlist(cursor, psql, conn)
        reorganizeAndSave(
            listCustomers,
            listDfColName['customers'],
            listHiveTable['customers'][0],
            listHiveTable['customers'][1],
            today,
            listHiveColName['customers'],
            testHdatabase
        )
    #供商关系日志表读取，并存储
    if listPostgreSwitch['customer_logs'] == 1:
        print("Starting read postgre.accounts.public.customer_logs")
        cursor.execute(
            'select id,record_id,company_id,customer_id,COALESCE(customer_type,-99),COALESCE(to_char(time,\'YYYYMMDD HH24:MI:SS\'),\'\'),operator,operation,COALESCE(remarks,\'\') '
            'from customer_logs '
            'where to_char(time,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listCustomerLogs = cursor.fetchall()
        reorganizeAndSave(
            listCustomerLogs,
            listDfColName['customer_logs'],
            listHiveTable['customer_logs'][0],
            listHiveTable['customer_logs'][1],
            today,
            listHiveColName['customer_logs'],
            testHdatabase
        )
    #推广记录表读取，并存储
    if listPostgreSwitch['promote_records'] == 1:
        print("Starting read postgre.accounts.public.promote_records")
        cursor.execute(
            'select id,alliance_id,type,promote_id,customer_type,promoter_id,to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),character '
            'from promote_records '
            'where to_char(created_at,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listPromoteRecords = cursor.fetchall()
        reorganizeAndSave(
            listPromoteRecords,
            listDfColName['promote_records'],
            listHiveTable['promote_records'][0],
            listHiveTable['promote_records'][1],
            today,
            listHiveColName['promote_records'],
            testHdatabase
        )
    #账号订单表读取，并存储
    if listPostgreSwitch['accounts_orders'] == 1:
        print("Starting read postgre.accounts.public.accounts_orders")
        cursor.execute(
            'select id,order_code,status,buyer_id,buyer_type,seller_id,type,jsonb_out(extra_info),unit_price,total_qty,total_price,COALESCE(to_char(completed_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(pay_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),replace(replace(pay_channel,chr(10),\'\'),chr(13),\'\'),replace(replace(pay_channel_id,chr(10),\'\'),chr(13),\'\'),payer_id,payer_type '
            ' from orders '
            ' where (completed_time is NULL or completed_time > \'' + today + '\') and to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listAccountsOrders = cursor.fetchall()
        reorganizeAndSave(
            listAccountsOrders,
            listDfColName['accounts_orders'],
            listHiveTable['accounts_orders'][0],
            listHiveTable['accounts_orders'][1],
            today,
            listHiveColName['accounts_orders'],
            testHdatabase
        )
    #账号订单表历史表读取，并存储
    if listPostgreSwitch['accounts_orders_history'] == 1:
        print("Starting read postgre.accounts.public.accounts_orders")
        cursor.execute(
            'select id,order_code,status,buyer_id,buyer_type,seller_id,type,jsonb_out(extra_info),unit_price,total_qty,total_price,COALESCE(to_char(completed_time, \'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(pay_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),replace(replace(pay_channel,chr(10),\'\'),chr(13),\'\'),replace(replace(pay_channel_id,chr(10),\'\'),chr(13),\'\'),payer_id,payer_type '
            'from orders '
            'where to_char(completed_time,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listAccountsOrdersHistory = cursor.fetchall()
        reorganizeAndSave(
            listAccountsOrdersHistory,
            listDfColName['accounts_orders_history'],
            listHiveTable['accounts_orders_history'][0],
            listHiveTable['accounts_orders_history'][1],
            today,
            listHiveColName['accounts_orders_history'],
            testHdatabase
        )
    #'服务购买表读取，并存储
    if listPostgreSwitch['services'] == 1:
        print("Starting read postgre.accounts.public.services")
        cursor.execute(
            'select id,type,brand,buyer_id,buyer_type,seller_id,COALESCE(to_char(start_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(end_time,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),associated_id '
            'from services '
            'where to_char(end_time,\'YYYYMMDD\') >= \'' + today + '\''
        )
        conn.commit()
        listServices = cursor.fetchall()
        reorganizeAndSave(
            listServices,
            listDfColName['services'],
            listHiveTable['services'][0],
            listHiveTable['services'][1],
            today,
            listHiveColName['services'],
            testHdatabase
        )
    #锐派R码表读取，并存储
    if listPostgreSwitch['codes'] == 1:
        print("Starting read postgre.accounts.public.codes")
        cursor.execute(
            'select id,company_id,type,status,COALESCE(to_char(use_time, \'YYYYMMDD HH24:MI:SS\'),\'\'),use_id,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            'from codes '
            'where use_time is NULL'
        )
        conn.commit()
        listCodes = cursor.fetchall()
        reorganizeAndSave(
            listCodes,
            listDfColName['codes'],
            listHiveTable['codes'][0],
            listHiveTable['codes'][1],
            today,
            listHiveColName['codes'],
            testHdatabase
        )
    #锐派R码历史表读取，并存储
    if listPostgreSwitch['codes_history'] == 1:
        print("Starting read postgre.accounts.public.codes")
        cursor.execute(
            'select id,company_id,type,status,COALESCE(to_char(use_time, \'YYYYMMDD HH24:MI:SS\'),\'\'),use_id,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\') '
            'from codes '
            'where to_char(use_time,\'YYYYMMDD\') = \'' + today + '\''
        )
        conn.commit()
        listCodesHistory = cursor.fetchall()
        reorganizeAndSave(
            listCodesHistory,
            listDfColName['codes_history'],
            listHiveTable['codes_history'][0],
            listHiveTable['codes_history'][1],
            today,
            listHiveColName['codes_history'],
            testHdatabase
        )
    #y用户todos表
    if listPostgreSwitch['applications'] == 1:
        print("Starting read postgre.accounts.public.applications")
        cursor.execute(
            'select id,type,applicant_id,auditor_id,jsonb_out(info),status,replace(replace(remark,chr(10),\'\'),chr(13),\'\'),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by '
            'from applications '
            'where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listApplications = cursor.fetchall()
        reorganizeAndSave(
            listApplications,
            listDfColName['applications'],
            listHiveTable['applications'][0],
            listHiveTable['applications'][1],
            today,
            listHiveColName['applications'],
            testHdatabase
        )
    #运营后台todos表
    if listPostgreSwitch['admin_applications'] == 1:
        print("Starting read postgre.accounts.public.admin_applications")
        cursor.execute(
            'select id,type,applicant_id,jsonb_out(info),status,replace(replace(remark,chr(10),\'\'),chr(13),\'\'),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by '
            'from admin_applications '
            'where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listAdminApplications = cursor.fetchall()
        reorganizeAndSave(
            listAdminApplications,
            listDfColName['admin_applications'],
            listHiveTable['admin_applications'][0],
            listHiveTable['admin_applications'][1],
            today,
            listHiveColName['admin_applications'],
            testHdatabase
        )
    # 联盟支付表
    if listPostgreSwitch['alliance_payments'] == 1:
        print("Starting read postgre.accounts.public.alliance_payments")
        cursor.execute(
            'select id,alliance_id,type,out_app_id,channel,scene,jsonb_out(params),COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by '
            'from alliance_payments '
            'where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listAlliancePayments = cursor.fetchall()
        reorganizeAndSave(
            listAlliancePayments,
            listDfColName['alliance_payments'],
            listHiveTable['alliance_payments'][0],
            listHiveTable['alliance_payments'][1],
            today,
            listHiveColName['alliance_payments'],
            testHdatabase
        )
    #联盟费用类型表
    if listPostgreSwitch['alliance_fee_class'] == 1:
        print("Starting read postgre.accounts.public.alliance_fee_class")
        cursor.execute(
            'select id,alliance_id,type,name,amount,status,COALESCE(to_char(created_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),created_by,COALESCE(to_char(updated_at,\'YYYYMMDD HH24:MI:SS\'),\'\'),updated_by'
            ' from alliance_fee_class '
            ' where to_char(created_at,\'YYYYMMDD\') <= \'' + today + '\''
        )
        conn.commit()
        listAlliancePayments = cursor.fetchall()
        reorganizeAndSave(
            listAlliancePayments,
            listDfColName['alliance_fee_class'],
            listHiveTable['alliance_fee_class'][0],
            listHiveTable['alliance_fee_class'][1],
            today,
            listHiveColName['alliance_fee_class'],
            testHdatabase
        )

#主函数
if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    PARAMS = None
    """
    默认开始及结束日期日期={Str}
    均为昨天，只跑昨天一天数据
    """
    defualt_date = date_str(datetime.date.today())
    parser = argparse.ArgumentParser()
    parser.add_argument('--startdate', type=str, default=defualt_date,
                        help='start date')
    parser.add_argument('--enddate', type=str, default=defualt_date,
                        help='end date')
    PARAMS, _ = parser.parse_known_args()
    # 打开数据库
    if testPdatabase == 1:
        conn = open_database('accounts')
    else:
        conn = open_qa_database('accounts_qa')
    # 定义今天为开始日期
    today = date_str(str_date(PARAMS.startdate) - datetime.timedelta(days=1))
    # 当开始日期<=结束日期时，运行该程序。如果开始日期=结束日期，break种植
    while (today <= date_str(str_date(PARAMS.enddate) - datetime.timedelta(days=1))):
        print("start reading " + today + " datas")
        get_save_info(conn, today)
        if today == date_str(str_date(PARAMS.enddate) - datetime.timedelta(days=1)):
            break
        print("read " + today + " datas finished")
        today = date_str(str_date(today) + datetime.timedelta(days=1))
        print("")
        print("")
    # 关闭数据库
    close_database(conn)
    sc.stop()
    print("program is done at: "  + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))