'''
by:pkh
'''
from pyspark import SparkContext,SparkConf,HiveContext
from pyspark.sql.functions import *
import datetime
import argparse
import ast

#spark入口
conf = SparkConf().setAppName("DataPanel_Alliance_Collection")
sc = SparkContext(conf=conf)
sqlContext = HiveContext(sc)

def exeHql(sql):
    return sqlContext.sql(sql)
def aggWithKey(x,y):
    x.extend(y)
    return x

#日期处理函数群
def date_str(date):
    return date.strftime("%Y%m%d")
def str_date(str):
    return datetime.datetime.strptime(str, "%Y%m%d").date()
def date_str_sep(date):
    return date.strftime("%Y-%m-%d")

'''
是否是测试数据库:
1：正常环境（数据库：orange_dm
2：测试环境（数据库：test
'''
testHdatabase = 1

hiveCol = {
    'dm_alliance_basic_info' : "(alliance_id            INT,"
                                "total_member           INT,"
                                "new_member             INT,"
                                "active_member_count    INT,"
                                "active_customer_count  INT,"
                                "alliance_onshelve_sum  DECIMAL(18,2),"
                                "alliance_outshelve_sum DECIMAL(18,2))",
    'dm_company_alliance_sku': "(company_skus_id    INT,"
                               "company_id          INT,"
                               "oe_code             STRING,"
                               "brand               STRING,"
                               "veh_brand           STRING,"
                               "sw_brand            STRING,"
                               "sku_name            STRING,"
                               "sw_production_place STRING,"
                               "alliance_part_id    INT,"
                               "property            STRING,"
                               "price_retail        DECIMAL(18,2),"
                               "alliance_id         INT)",
    'dm_sku_valid_check' : "(seller_company_id   INT,"
                           "sku_id               INT,"
                           "buyer_user_id        INT,"
                           "price                DECIMAL(18,2),"
                           "carts_qty_sum        INT,"
                           "checkcount           INT,"
                           "buyer_company_id     INT,"
                           "buyer_company_name   STRING,"
                           "buyer_company_type   STRING,"
                           "oe_code              STRING,"
                           "brand                STRING,"
                           "veh_brand            STRING,"
                           "sw_brand             STRING,"
                           "sku_name             STRING,"
                           "sw_production_place  INT,"
                           "alliance_part_id     INT,"
                           "property             STRING,"
                           "price_retail         DECIMAL(18,2),"
                           "alliance_id          INT)",
    'dm_panel_alliance_garage_check' : "(alliance_id            INT,"
                                       "seller_company_id       INT,"
                                       "customer_id             INT,"
                                       "garage_name             STRING,"
                                       "garage_check_num        INT,"
                                       "max_sku_id              INT,"
                                       "max_oe_code             STRING,"
                                       "max_brand               STRING,"
                                       "max_sku_name            STRING,"
                                       "max_sw_production_place STRING,"
                                       "max_garage_sku_count    INT)",
    'dm_alliance_member_customer' : "(alliance_id          INT,"
                                    "member_id             INT,"
                                    "join_time             STRING,"
                                    "company_customer_id   INT,"
                                    "company_customer_type INT,"
                                    "created_at            STRING)",
    'dm_alliance_member_base_info' : "(alliance_id             INT,"
                                     "member_id                INT,"
                                     "total_customer           INT,"
                                     "new_customer             INT,"
                                     "active_customer_count    INT,"
                                     "customer_num             INT,"
                                     "member_name              STRING,"
                                     "alliance_member_sale_qty DECIMAL(18,2))",
    'dm_orders_details_info' : '(order_detail_id           INT,'
                               'price                      DECIMAL(18,2),'
                               'out_qty                    DECIMAL(18,2),'
                               'receive_qty                DECIMAL(18,2),'
                               'time                       STRING,'
                               'order_code                 STRING,'
                               'buyer_company_id           INT,'
                               'seller_company_id          INT,'
                               'sku_id                     INT,'
                               'created_at                 STRING,'
                               'buyer_user_id              INT,'
                               'name                       STRING,'
                               'brand                      STRING,'
                               'oe_code                    STRING,'
                               'sw_production_place        STRING,'
                               'alliance_part_id           INT,'
                               'property                   STRING,'
                               'seller_company_area_id     INT,'
                               'buyer_company_area_id      INT,'
                               'buyer_user_area_id         INT,'
                               'seller_alliance_id         INT,'
                               'company_buyer_alliance_id  INT,'
                               'personel_buyer_alliance_id INT)',
    'dm_orders_details_info_all' : '(order_detail_id           INT,'
                                   'price                      DECIMAL(18,2),'
                                   'out_qty                    DECIMAL(18,2),'
                                   'receive_qty                DECIMAL(18,2),'
                                   'time                       STRING,'
                                   'order_code                 STRING,'
                                   'buyer_company_id           INT,'
                                   'seller_company_id          INT,'
                                   'sku_id                     INT,'
                                   'created_at                 STRING,'
                                   'buyer_user_id              INT,'
                                   'name                       STRING,'
                                   'brand                      STRING,'
                                   'oe_code                    STRING,'
                                   'sw_production_place        STRING,'
                                   'alliance_part_id           INT,'
                                   'property                   INT,'
                                   'seller_company_area_id     INT,'
                                   'buyer_company_area_id      INT,'
                                   'buyer_user_area_id         INT,'
                                   'seller_alliance_id         INT,'
                                   'company_buyer_alliance_id  INT,'
                                   'personel_buyer_alliance_id INT)',
    'dm_city_provice_qty_price' : '(order_detail_id               INT,'
                                  'out_qty                        DECIMAL(18,2),'
                                  'receive_qty                    DECIMAL(18,2),'
                                  'total_out_price                DECIMAL(18,2),'
                                  'total_receive_price            DECIMAL(18,2),'
                                  'seller_alliance_id             INT,'
                                  'seller_company_id              INT,'
                                  'seller_company_area_id         INT,'
                                  'buyer_user_area_id             INT,'
                                  'buyer_company_area_id          INT,'
                                  'oe_code                        STRING,'
                                  'name                           STRING,'
                                  'brand                          STRING,'
                                  'sw_production_place            STRING,'
                                  'sku_id                         INT,'
                                  'property                       STRING,'
                                  'seller_company_city_id         STRING,'
                                  'seller_company_province_id     INT,'
                                  'buyer_company_city_id          INT,'
                                  'buyer_company_province_id      INT,'
                                  'buyer_user_city_id             INT,'
                                  'buyer_user_province_id         INT,'
                                  'city_flag                      INT,'
                                  'province_flag                  INT)',
    'dm_price_analysis' : '(seller_alliance_id INT,'
                          'alliance_part_id    INT,'
                          'name                STRING,'
                          'brand               STRING,'
                          'oe_code             STRING,'
                          'sw_production_place STRING,'
                          'max_sell_price      DECIMAL(18,2),'
                          'min_sell_price      DECIMAL(18,2),'
                          'ave_sell_price      DECIMAL(18,2),'
                          'max_receive_price   DECIMAL(18,2),'
                          'min_receive_price   DECIMAL(18,2),'
                          'avg_receive_price   DECIMAL(18,2),'
                          'sale_qty            DECIMAL(18,2))',
    'dm_purchase_detail' : '(buyer_alliance_id  INT,'
                           'name                STRING,'
                           'oe_code             STRING,'
                           'brand               STRING,'
                           'sw_production_place STRING,'
                           'purchase_qty        DECIMAL(18,2),'
                           'purchase_price      DECIMAL(18,2))',
    'dm_alliance_company_parts_saleqty_check' : '(alliance_id        INT,'
                                                'seller_company_id   INT,'
                                                'sku_id              INT,'
                                                'oe_code             STRING,'
                                                'name                STRING,'
                                                'brand               STRING,'
                                                'sw_production_place STRING,'
                                                'parts_qty           DECIMAL(18,2),'
                                                'parts_checkcount    DECIMAL(18,2))',
    'dm_city_qty_price' : '(seller_alliance_id  INT,'
                          'seller_company_id    INT,'
                          'oe_code              STRING,'
                          'brand                STRING,'
                          'sw_production_place  STRING,'
                          'city_id              INT,'
                          'seller_city_qty      DECIMAL(18,2),'
                          'seller_city_price    DECIMAL(18,2),'
                          'buyer_city_qty       DECIMAL(18,2),'
                          'buyer_city_price     DECIMAL(18,2))',
    'dm_province_qty_price' : '(seller_alliance_id   INT,'
                              'seller_company_id     INT,'
                              'oe_code               STRING,'
                              'brand                 STRING,'
                              'sw_production_place   STRING,'
                              'province_id           INT,'
                              'seller_province_qty   DECIMAL(18,2),'
                              'seller_province_price DECIMAL(18,2),'
                              'buyer_province_qty    DECIMAL(18,2),'
                              'buyer_province_price  DECIMAL(18,2))',
    'dm_on_sale_part_num' : '(alliance_id        INT,'
                            'company_id          INT,'
                            'oe_code             STRING,'
                            'brand               STRING,'
                            'sku_name            STRING,'
                            'sw_production_place STRING,'
                            'onsale_num          DECIMAL(18,2))',
    'dm_on_sale_stockprice' : '(alliance_id         INT,'
                              'company_id          INT,'
                              'oe_code             STRING,'
                              'brand               STRING,'
                              'sku_name            STRING,'
                              'sw_production_place STRING,'
                              'stock_price         DECIMAL(18,2))'
}

def saveHiveDatabase(tableName,df,hiveCol,dateStr):
    print("start save:" + tableName)
    # 设置动态分区
    sqlContext.setConf("hive.exec.dynamic.partition", "true")
    sqlContext.setConf("hive.exec.dynamic.partition.mode", "nonstrict")
    # 动态分区上线设置
    sqlContext.setConf("hive.exec.max.dynamic.partitions", "10000")
    # 使用orange库
    if testHdatabase == 1:
        sqlContext.sql("use orange_dm")
    else:
        sqlContext.sql("use test")
    # 表若不存在新建Hive表
    sqlContext.sql("CREATE TABLE IF NOT EXISTS " + tableName + hiveCol + " PARTITIONED BY (day STRING)")
    dfTemp = df.withColumn("day", lit(dateStr))
    #存为临时表
    dfTemp.registerTempTable("tempTable")
    #从临时表读取数据存入hive
    sqlContext.sql("INSERT OVERWRITE TABLE " + tableName + " PARTITION(day) select * from tempTable")
    print("Saving data to " + tableName + " was done!")

"""
查询
成员相关数据，客户相关数据，所有联盟，公司相关数据，员工相关数据
dfMember            = alliance_id  member_id     join_time                       （orange.ods_psql_alliances_members）      
dfAlliance          = alliance_id  brands        veh_brands                      （orange.ods_psql_alliances）     
dfCustomer          = company_id   customer_id   customer_type   created_at       (orange.ods_psql_customers)
dfAllianceCustomer  = alliance_id  customer_id   created_at      customer_type    (orange.ods_psql_alliance_customers)    
dfStaffs            = company_id   user_id                                       （ods_psql_staffs）   
dfCompanies         = company_id   company_name  company_type    area_id          (ods_psql_companies）    
"""
def getBasicInfo(dateStrS):
    # 联盟成员相关数据获取
    hql = "select alliance_id,member_id,join_time " \
          " from orange.ods_psql_alliances_members " \
          " where status = 0 and day = '" + dateStrS + "'"
    dfMember = exeHql(hql)
    # 获取所有联盟
    hql = "select alliance_id,brands,veh_brands " \
          "from orange.ods_psql_alliances " \
          "where status = 3 and day = '" + dateStrS + "'"
    dfAlliance = exeHql(hql)
    # 客户信息查询
    hql = "select company_id,customer_id,customer_type,created_at " \
          " from orange.ods_psql_customers " \
          " where status = 1 and day = '" + dateStrS + "'"
    dfCustomer = exeHql(hql)
    # 联盟客户相关数据获取
    hql = "select alliance_id,customer_id,created_at,customer_type " \
          " from orange.ods_psql_alliance_customers " \
          " where status = 0 and day = '" + dateStrS + "'"
    dfAllianceCustomer = exeHql(hql)
    # 获取员工表信息
    hql = 'select company_id,user_id ' \
          ' from orange.ods_psql_staffs' \
          ' where day = \'' + dateStrS + '\' and status = 1'
    dfStaffs = exeHql(hql)
    # 公司数据获取
    hql = "select company_id,name as company_name,type as company_type,area_id" \
          " from orange.ods_psql_companies" \
          " where day = '" + dateStrS + "' and status = 0"
    dfCompanies = exeHql(hql)
    # 获取区域信息
    hql = "select areas_id,type,parent_id" \
          " from orange.ods_psql_areas"
    dfAreas = exeHql(hql)
    # 获取用户区域信息
    hql = 'select user_id,area_id' \
          ' from orange.ods_psql_users' \
          ' where day = \'' + dateStrS + '\''
    dfUsers = exeHql(hql)
    return dfMember,dfAlliance,dfCustomer,dfAllianceCustomer,dfStaffs,dfCompanies,dfUsers,dfAreas

"""
处理
获取sku相关信息,并保存方便alliance_panel使用
dfAllianceCompanySku = company_skus_id  company_id  oe_code              brand             veh_brand
                       sw_brand         sku_name    sw_production_place  alliance_part_id  property
                       price_retail     alliance_id

保存（dm_company_alliance_sku)
"""
def getCompanySku(dfAlliance, dfMember,dateStrS):
    # 获取sku相关信息
    # dfCompanySku = company_skus_id，company_id， oe_code， veh_brand，          std_name，
    #                oe_name，        sw_oe_name， sw_brand，sw_production_place，day
    #                alliance_part_id property    price_retail
    hql = 'select company_skus_id,company_id,oe_code,veh_brand,std_name,' \
          'oe_name,sw_oe_name,sw_brand,sw_production_place,day,' \
          'alliance_part_id,property,price_retail' \
          ' from orange.ods_psql_company_skus' \
          ' where day <= \'' + dateStrS + '\''
    dfCompanySku = exeHql(hql)
    dfCompanySku = dfCompanySku.distinct()
    # 按照day，查找到最新的数据，并且过滤名字和品牌
    mapCompanySkuInfo = dfCompanySku \
        .rdd \
        .map(lambda p: ((p.company_skus_id), [(p.company_id, p.oe_code, p.veh_brand, p.std_name, p.oe_name, p.sw_oe_name,
                                              p.sw_brand, p.sw_production_place, p.day, p.alliance_part_id, p.property,
                                              p.price_retail)])) \
        .reduceByKey(aggWithKey) \
        .map(lambda x: (x[0], x[1])) \
        .map(mapCompanySku)
    # dfCompanySku = company_skus_id  company_id  oe_code              brand               veh_brand
    #               sw_brand         sku_name    sw_production_place  alliance_part_id    property
    #               price_retail
    dfCompanySku = sqlContext.createDataFrame(mapCompanySkuInfo,
                                              ['company_skus_id', 'company_id', 'oe_code', 'brand', 'veh_brand',
                                               'sw_brand', 'sku_name', 'sw_production_place', 'alliance_part_id',
                                               'property',
                                               'price_retail'])
    # 得到配件商所属联盟
    # dfMemberSku = company_skus_id  company_id  oe_code              brand             veh_brand
    #              sw_brand         sku_name    sw_production_place  alliance_part_id  property
    #              price_retail     alliance_id alliance_flag
    dfMemberSku = dfCompanySku.join(dfMember, [dfCompanySku.company_id == dfMember.member_id], 'leftouter') \
        .drop(dfMember.member_id) \
        .drop(dfMember.join_time) \
        .withColumn('alliance_flag', when(col('alliance_id').isNull(), 0).otherwise(1))
    # dfMemberSku = company_skus_id  company_id  oe_code              brand             veh_brand
    #              sw_brand         sku_name    sw_production_place  alliance_part_id  property
    #              price_retail     alliance_id
    dfMemberSku = dfMemberSku \
        .where(dfMemberSku.alliance_flag == 1) \
        .drop('alliance_flag')
    # 得到联盟对应brand
    # deMemberSkuBrandList = company_skus_id  company_id     oe_code              brand             veh_brand
    #                       sw_brand         sku_name       sw_production_place  alliance_part_id  property
    #                       price_retail     alliance_id    brand                veh_brand
    deMemberSkuBrandList = dfMemberSku \
        .join(dfAlliance, [dfMemberSku.alliance_id == dfAlliance.alliance_id], 'left') \
        .drop(dfAlliance.alliance_id)
    mapAllianceCompanySku = deMemberSkuBrandList \
        .rdd \
        .map(mapAllianceBrandFlag)
    # 得到是否属于联盟配件标识
    # dfAllianceCompanySku = company_skus_id  company_id  oe_code              brand             veh_brand
    #                       sw_brand         sku_name    sw_production_place  alliance_part_id  property
    #                       price_retail     alliance_id      flag
    dfAllianceCompanySku = sqlContext.createDataFrame(mapAllianceCompanySku,
                                                      ['company_skus_id', 'company_id', 'oe_code', 'brand', 'veh_brand',
                                                       'sw_brand', 'sku_name', 'sw_production_place',
                                                       'alliance_part_id', 'property',
                                                       'price_retail', 'alliance_id', 'flag'])
    # dfAllianceCompanySku = company_skus_id  company_id  oe_code              brand             veh_brand
    #                        sw_brand         sku_name    sw_production_place  alliance_part_id  property
    #                        price_retail     alliance_id
    dfAllianceCompanySku = dfAllianceCompanySku.where(dfAllianceCompanySku.flag == 1).drop("flag")
    saveHiveDatabase('dm_company_alliance_sku', dfAllianceCompanySku, hiveCol['dm_company_alliance_sku'], yesterday)
    return dfAllianceCompanySku

"""
调用位置：getCompanySku()
作用：确定商品是否属于联盟
return: company_skus_id,company_id,oe_code,brand,veh_brand,sw_brand,sku_name,sw_production_place,alliance_part_id,property,price_retail,alliance_id,flag
"""
def mapAllianceBrandFlag(p):
    company_skus_id,company_id,oe_code,brand,veh_brand,sw_brand,sku_name,sw_production_place,alliance_part_id,property,price_retail,alliance_id,brand_list,veh_brand_list = p
    if veh_brand in ast.literal_eval(veh_brand_list) or brand in ast.literal_eval(brand_list):
        flag = 1
    else:
        flag = 0
    return company_skus_id,company_id,oe_code,brand,veh_brand,sw_brand,sku_name,sw_production_place,alliance_part_id,property,price_retail,alliance_id,flag

"""
调用位置：getCompanySku()
作用：查找最新的数据，并过滤brand和sku_name
return: company_skus_id,company_id,oe_code,brand,sku_name,sw_production_place,alliance_part_id,property,price_retail
"""
def mapCompanySku(p):
    company_skus_id = p[0]
    infoList = p[1]
    maxTime = ""
    #获取商品最新信息
    for item in infoList:
        if maxTime <= item[8]:
            company_id = item[0]
            oe_code = item[1]
            sw_brand = item[6]
            veh_brand = item[2]
            alliance_part_id = item[9]
            property = item[10]
            price_retail = item[11]
            if sw_brand != "":
                brand = sw_brand
            else:
                brand = veh_brand
            if item[3] != "":
                sku_name = item[3]
            elif item[4] != "":
                sku_name = item[4]
            else:
                sku_name = item[5]
            sw_production_place = item[7]
            maxTime = item[8]
    return company_skus_id,company_id,oe_code,brand,veh_brand,sw_brand,sku_name,sw_production_place,alliance_part_id,property,price_retail

"""
处理
获取意向查询数据（包括sku信息)
dfSkuValidCheckInfo = seller_company_id  sku_id            buyer_user_id       price               carts_qty_sum
                      checkcount         buyer_company_id  buyer_company_name  buyer_company_type  oe_code
                      brand              veh_brand         sw_brand            sku_name            sw_production_place
                      alliance_part_id   property          price_retail        alliance_id
保存（dm_sku_valid_check）
"""
def getSkuValidCheckInfo(dfStaffs,dfAllianceCompanySku,dfCompanies,dateStrS):
    # 获取查询信息
    hql = 'select seller_company_id,sku_id,buyer_user_id,price,carts_qty_sum,checkcount' \
          ' from orange.ods_es_valid_check' \
          ' where day = \'' + dateStrS + '\''
    dfValidCheck = exeHql(hql)
    # 获得查询者的公司id
    # dfValidCheckWithBuyerCompanyId = seller_company_id  sku_id            buyer_user_id  price  carts_qty_sum
    #                                  checkcount         buyer_company_id
    dfValidCheckWithBuyerCompanyId = dfValidCheck \
        .join(dfStaffs, [dfValidCheck.buyer_user_id == dfStaffs.user_id], 'left') \
        .drop(dfStaffs.user_id) \
        .withColumnRenamed('company_id', 'buyer_company_id')
    # 得到查询公司的公司属性
    # dfValidCheckWithBuyerCompanyType = seller_company_id  sku_id            buyer_user_id       price              carts_qty_sum
    #                                    checkcount         buyer_company_id  buyer_company_name  buyer_company_type
    dfValidCheckWithBuyerCompanyType = dfValidCheckWithBuyerCompanyId \
        .join(dfCompanies, [dfCompanies.company_id == dfValidCheckWithBuyerCompanyId.buyer_company_id], 'left') \
        .drop(dfCompanies.company_id) \
        .withColumnRenamed('company_name', 'buyer_company_name')\
        .withColumnRenamed('company_type', 'buyer_company_type')\
        .drop(dfCompanies.area_id)
    # 得到被查询商品信息
    # dfSkuValidCheckInfo = seller_company_id  sku_id            buyer_user_id       price               carts_qty_sum
    #                       checkcount         buyer_company_id  buyer_company_name  buyer_company_type  oe_code
    #                       brand              veh_brand         sw_brand            sku_name            sw_production_place
    #                       alliance_part_id   property          price_retail        alliance_id
    dfSkuValidCheckInfo = dfValidCheckWithBuyerCompanyType \
        .join(dfAllianceCompanySku, [dfAllianceCompanySku.company_skus_id == dfValidCheckWithBuyerCompanyType.sku_id,
                                     dfAllianceCompanySku.company_id == dfValidCheckWithBuyerCompanyType.seller_company_id],'leftouter') \
        .drop(dfAllianceCompanySku.company_skus_id) \
        .drop(dfAllianceCompanySku.company_id)\
        .distinct()
    saveHiveDatabase('dm_sku_valid_check', dfSkuValidCheckInfo, hiveCol['dm_sku_valid_check'], yesterday)
    return dfSkuValidCheckInfo

"""
处理
订单数据，增加买卖双方联盟id
dfOrdersDetailsInfo = order_detail_id            price                      out_qty             receive_qty            time  
                      order_code                 buyer_company_id           seller_company_id   sku_id                 created_at
                      buyer_user_id              name                       brand               oe_code                sw_production_place
                      alliance_part_id           seller_company_area_id     buyer_company_area_id  buyer_user_area_id  seller_alliance_id    
                      company_buyer_alliance_id  personel_buyer_alliance_id   
dfOrdersDetailsInfoAll = order_detail_id            price                      out_qty             receive_qty            time  
                         order_code                 buyer_company_id           seller_company_id   sku_id                 created_at
                         buyer_user_id              name                       brand               oe_code                sw_production_place
                         alliance_part_id           seller_company_area_id     buyer_company_area_id  buyer_user_area_id  seller_alliance_id    
                         company_buyer_alliance_id  personel_buyer_alliance_id                  
"""
def getOrderDetailsInfo(dfAllianceCustomer,dfAllianceCompanySku,dfCompanies,dfUsers,dateStrS):
    #取出订单信息
    # dfOrdersDetails = order_detail_id  price             out_qty              receive_qty  time
    #                   order_code       buyer_company_id  seller_company_id    sku_id       created_at
    #                   buyer_user_id    std_name          veh_brand            oe_code      oe_name
    #                   sw_brand         sw_production_place  sw_oe_name
    hql = 'select order_detail_id,price,out_qty,receive_qty,time,order_code,buyer_company_id,seller_company_id,sku_id,created_at,created_by buyer_user_id,std_name,veh_brand,oe_code,oe_name,sw_brand,sw_production_place,sw_oe_name' \
          ' from orange.ods_psql_order_detail_logs' \
          ' where day = \'' + dateStrS + '\' and (out_qty > 0 or receive_qty > 0)'
    dfOrdersDetails = exeHql(hql)
    #加入卖方地区id
    # dfOrdersDetails = order_detail_id  price                out_qty            receive_qty             time
    #                   order_code       buyer_company_id     seller_company_id  sku_id                  created_at
    #                   buyer_user_id    std_name             veh_brand          oe_code                 oe_name
    #                   sw_brand         sw_production_place  sw_oe_name         seller_company_area_id
    dfOrdersDetails = dfOrdersDetails\
        .join(dfCompanies,[dfOrdersDetails.seller_company_id == dfCompanies.company_id],'left')\
        .drop(dfCompanies.company_name)\
        .drop(dfCompanies.company_type)\
        .drop(dfCompanies.company_id)\
        .withColumnRenamed('area_id','seller_company_area_id')
    # dfOrdersDetails = order_detail_id  price                out_qty            receive_qty             time
    #                   order_code       buyer_company_id     seller_company_id  sku_id                  created_at
    #                   buyer_user_id    std_name             veh_brand          oe_code                 oe_name
    #                   sw_brand         sw_production_place  sw_oe_name         seller_company_area_id  buyer_company_area_id
    #加入买方地区id
    dfOrdersDetails = dfOrdersDetails\
        .join(dfCompanies,[dfOrdersDetails.buyer_company_id == dfCompanies.company_id],'left')\
        .drop(dfCompanies.company_name)\
        .drop(dfCompanies.company_type)\
        .drop(dfCompanies.company_id)\
        .withColumnRenamed('area_id','buyer_company_area_id')
    # dfOrdersDetails = order_detail_id  price             out_qty            receive_qty             time
    #                   order_code       buyer_company_id  seller_company_id  sku_id                  created_at
    #                   buyer_user_id    std_name          veh_brand          oe_code                 oe_name
    #                   sw_brand         sw_production_place  sw_oe_name      seller_company_area_id  buyer_company_area_id
    #                   buyer_user_area_id
    dfOrdersDetails = dfOrdersDetails\
        .join(dfUsers,[dfOrdersDetails.buyer_user_id == dfUsers.user_id],'left')\
        .drop(dfUsers.user_id)\
        .withColumnRenamed('area_id','buyer_user_area_id')
    # 公司买家
    # dfCompanyBuyer = alliance_id customer_id  created_at   customer_type
    dfCompanyBuyer = dfAllianceCustomer \
        .where(dfAllianceCustomer.customer_type == 1)
    # 个人买家
    # dfPersonelBuyer = alliance_id customer_id  created_at   customer_type
    dfPersonelBuyer = dfAllianceCustomer \
        .where(dfAllianceCustomer.customer_type == 0)
    # 联盟 公司 商品信息(如果join过后没有seller_alliance_id的就是非联盟商品订单）
    # dfAllianceCompanySku = company_skus_id  company_id  oe_code              brand            veh_brand
    #                        sw_brand         sku_name    sw_production_place  alliance_part_id property
    #                        alliance_id
    dfAllianceCompanySku = dfAllianceCompanySku \
        .select('company_skus_id', 'company_id','alliance_part_id','property', 'alliance_id')
    # 得到卖双方所属联盟
    # dfOrdersDetailsWithSellerAlliance = order_detail_id        price                out_qty            receive_qty            time
    #                                     order_code             buyer_company_id     seller_company_id  sku_id                 created_at
    #                                     buyer_user_id          std_name             veh_brand          oe_code                oe_name
    #                                     sw_brand               sw_production_place  sw_oe_name         seller_company_area_id
    #                                     buyer_company_area_id  buyer_user_area_id   alliance_part_id   property               seller_alliance_id
    dfOrdersDetailsWithSellerAlliance = dfOrdersDetails \
        .join(dfAllianceCompanySku, [dfAllianceCompanySku.company_id == dfOrdersDetails.seller_company_id,
                                     dfAllianceCompanySku.company_skus_id == dfOrdersDetails.sku_id], 'leftouter') \
        .drop(dfAllianceCompanySku.company_id) \
        .drop(dfAllianceCompanySku.company_skus_id) \
        .withColumnRenamed('alliance_id', 'seller_alliance_id')
    # 得到买方联盟
    # dfOrdersDetailsWithSellerAlliance = order_detail_id        price                out_qty              receive_qty                time
    #     #                               order_code             buyer_company_id     seller_company_id    sku_id                     created_at
    #     #                               buyer_user_id          std_name             veh_brand            oe_code                    oe_name
    #     #                               sw_brand               sw_production_place  sw_oe_name           seller_company_area_id
    #     #                               buyer_company_area_id  buyer_user_area_id   alliance_part_id     property                   seller_alliance_id
    #                                     company_buyer_alliance_id
    dfOrdersDetailsWithCompanyBuyerAllianceId = dfOrdersDetailsWithSellerAlliance \
        .join(dfCompanyBuyer, [dfCompanyBuyer.customer_id == dfOrdersDetailsWithSellerAlliance.buyer_company_id],'leftouter') \
        .drop(dfCompanyBuyer.customer_id) \
        .drop(dfCompanyBuyer.created_at) \
        .drop(dfCompanyBuyer.customer_type) \
        .withColumnRenamed('alliance_id', 'company_buyer_alliance_id')
    # dfOrdersDetailsWithSellerAlliance = order_detail_id            price                out_qty              receive_qty             time
    #                                     order_code                 buyer_company_id     seller_company_id    sku_id                  created_at
    #                                     buyer_user_id              std_name             veh_brand            oe_code                 oe_name
    #                                     sw_brand                   sw_production_place  sw_oe_name           seller_company_area_id
    #                                     buyer_company_area_id      buyer_user_area_id   alliance_part_id     property                seller_alliance_id
    #                                     company_buyer_alliance_id  personel_buyer_alliance_id
    dfOrdersDetailsInfo = dfOrdersDetailsWithCompanyBuyerAllianceId \
        .join(dfPersonelBuyer,
              [dfPersonelBuyer.customer_id == dfOrdersDetailsWithCompanyBuyerAllianceId.buyer_user_id], 'leftouter') \
        .drop(dfPersonelBuyer.customer_id) \
        .drop(dfPersonelBuyer.created_at) \
        .drop(dfPersonelBuyer.customer_type) \
        .withColumnRenamed('alliance_id', 'personel_buyer_alliance_id')\
        .na.fill(0)\
        .na.fill("")\
        .na.fill(0.00)
    mapOrderDetails = dfOrdersDetailsInfo.rdd.map(mapNameBrand)
    dfOrdersDetailsInfoAll = sqlContext.createDataFrame(mapOrderDetails,
                                                 ['order_detail_id','price','out_qty','receive_qty','time',
                                                  'order_code','buyer_company_id','seller_company_id','sku_id','created_at',
                                                  'buyer_user_id','name','brand','oe_code','sw_production_place',
                                                  'alliance_part_id','property','seller_company_area_id','buyer_company_area_id','buyer_user_area_id',
                                                  'seller_alliance_id','company_buyer_alliance_id','personel_buyer_alliance_id'])
    saveHiveDatabase('dm_orders_details_info_all', dfOrdersDetailsInfoAll, hiveCol['dm_orders_details_info_all'], dateStrS)
    dfOrdersDetailsInfoC = dfOrdersDetailsInfoAll \
        .where(col('seller_alliance_id') == col('company_buyer_alliance_id'))
    dfOrdersDetailsInfoP = dfOrdersDetailsInfoAll \
        .where(col('seller_alliance_id') == col('personel_buyer_alliance_id'))
    dfOrdersDetailsInfo = dfOrdersDetailsInfoC \
        .unionAll(dfOrdersDetailsInfoP)\
        .distinct()
    saveHiveDatabase('dm_orders_details_info',dfOrdersDetailsInfo,hiveCol['dm_orders_details_info'],dateStrS)
    return  dfOrdersDetailsInfo,dfOrdersDetailsInfoAll

"""
调用位置：getGarageInfo()
作用：找出查询次数最多的商品
return: order_detail_id     price                      out_qty                    receive_qty         time  
        order_code          buyer_company_id           seller_company_id          sku_id              created_at
        buyer_user_id       name                       brand                      oe_code             sw_production_place
        alliance_part_id    property                   seller_company_area_id     buyer_company_area_id  buyer_user_area_id  
        seller_alliance_id  company_buyer_alliance_id  personel_buyer_alliance_id
"""
def mapNameBrand(p):
    order_detail_id,price,out_qty,receive_qty,time,order_code,buyer_company_id,seller_company_id,sku_id,created_at,buyer_user_id,std_name,veh_brand,oe_code,oe_name,sw_brand,sw_production_place,sw_oe_name,seller_company_area_id,buyer_company_area_id,buyer_user_area_id,alliance_part_id,property,seller_alliance_id,company_buyer_alliance_id,personel_buyer_alliance_id = p
    if std_name != "":
        name = std_name
    elif oe_name != "":
        name = oe_name
    else:
        name = sw_oe_name
    if veh_brand != "":
        brand = veh_brand
    else:
        brand = sw_brand
    return order_detail_id,price,out_qty,receive_qty,time,order_code,buyer_company_id,seller_company_id,sku_id,created_at,buyer_user_id,name,brand,oe_code,sw_production_place,alliance_part_id,property,seller_company_area_id,buyer_company_area_id,buyer_user_area_id,seller_alliance_id,company_buyer_alliance_id,personel_buyer_alliance_id

"""
处理
处理订单数据，获取卖方及买方所属区域
dfCityProvince = order_detail_id     out_qty                 receive_qty                  total_out_price          total_receive_price
                 seller_alliance_id  seller_company_id       seller_company_area_id       buyer_user_area_id       buyer_company_area_id
                 oe_code             name                    brand                        sw_production_place      sku_id
                 property            seller_company_city_id  seller_company_province_id   buyer_company_city_id    buyer_company_province_id
                 buyer_user_city_id  buyer_user_province_id  flag
"""
def getOrderCityProvinceInfo(dfOrdersDetailsInfo,dfAreas,dateStrS):
    #获取销售金额
    dfOrdersDetails = dfOrdersDetailsInfo \
        .withColumn('total_out_price', col('price') * col('out_qty'))\
        .withColumn('total_receive_price', col('price') * col('receive_qty'))
    # 获得卖家公司区域
    # dfOrdersDetails = order_detail_id     out_qty                   receive_qty             total_out_price      total_receive_price
    #                   seller_alliance_id  seller_company_id         seller_company_area_id  buyer_user_area_id   buyer_company_area_id
    #                   oe_code             name                      brand                   sw_production_place  sku_id
    #                   property            seller_company_area_type  seller_company_city_id
    dfSellerAreas = dfOrdersDetails \
        .select('order_detail_id','out_qty','receive_qty', 'total_out_price', 'total_receive_price', 'seller_alliance_id', 'seller_company_id', 'seller_company_area_id',
                'buyer_user_area_id', 'buyer_company_area_id','oe_code','name','brand','sw_production_place','sku_id','property') \
        .join(dfAreas, [dfAreas.areas_id == dfOrdersDetails.seller_company_area_id], 'left') \
        .drop(dfAreas.areas_id) \
        .withColumnRenamed('type', 'seller_company_area_type') \
        .withColumnRenamed('parent_id', 'seller_company_city_id')
    # 获得买家公司区域
    # dfBuyerCompanyAreas = order_detail_id     out_qty                   receive_qty             total_out_price         total_receive_price
    #                       seller_alliance_id  seller_company_id         seller_company_area_id  buyer_user_area_id      buyer_company_area_id
    #                       oe_code             name                      brand                   sw_production_place     sku_id
    #                       property            seller_company_area_type  seller_company_city_id  buyer_company_area_type buyer_company_city_id
    dfBuyerCompanyAreas = dfSellerAreas \
        .join(dfAreas, [dfAreas.areas_id == dfSellerAreas.buyer_company_area_id], 'left') \
        .drop(dfAreas.areas_id) \
        .withColumnRenamed('type', 'buyer_company_area_type') \
        .withColumnRenamed('parent_id', 'buyer_company_city_id')
    # 获得买家用户区域
    # dfBuyerUserAreas = order_detail_id       out_qty                   receive_qty             total_out_price         total_receive_price
    #                    seller_alliance_id    seller_company_id         seller_company_area_id  buyer_user_area_id      buyer_company_area_id
    #                    oe_code               name                      brand                   sw_production_place     sku_id
    #                    property              seller_company_area_type  seller_company_city_id  buyer_company_area_type buyer_company_city_id
    #                    buyer_user_area_type  buyer_user_city_id
    dfBuyerUserAreas = dfBuyerCompanyAreas \
        .join(dfAreas, [dfAreas.areas_id == dfBuyerCompanyAreas.buyer_user_area_id], 'left') \
        .drop(dfAreas.areas_id) \
        .withColumnRenamed('type', 'buyer_user_area_type') \
        .withColumnRenamed('parent_id', 'buyer_user_city_id')
    # 获取卖方省级id
    # dfSellerProvince = order_detail_id       out_qty                   receive_qty                    total_out_price             total_receive_price
    #                    seller_alliance_id    seller_company_id         seller_company_area_id         buyer_user_area_id          buyer_company_area_id
    #                    oe_code               name                      brand                          sw_production_place         sku_id
    #                    property              seller_company_area_type  seller_company_city_id         buyer_company_area_type     buyer_company_city_id
    #                    buyer_user_area_type  buyer_user_city_id        seller_company_province_type   seller_company_province_id
    dfSellerProvince = dfBuyerUserAreas \
        .join(dfAreas, [dfAreas.areas_id == dfBuyerUserAreas.seller_company_city_id], 'left') \
        .drop(dfAreas.areas_id) \
        .withColumnRenamed('type', 'seller_company_province_type') \
        .withColumnRenamed('parent_id', 'seller_company_province_id')
    # 获得买方公司省级id
    # dfBuyerCompanyProvince = order_detail_id            out_qty                   receive_qty                    total_out_price             total_receive_price
    #                          seller_alliance_id         seller_company_id         seller_company_area_id         buyer_user_area_id          buyer_company_area_id
    #                          oe_code                    name                      brand                          sw_production_place         sku_id
    #                          property                   seller_company_area_type  seller_company_city_id         buyer_company_area_type     buyer_company_city_id
    #                          buyer_user_area_type       buyer_user_city_id        seller_company_province_type   seller_company_province_id  buyer_company_province_type
    #                          buyer_company_province_id
    dfBuyerCompanyProvince = dfSellerProvince \
        .join(dfAreas, [dfAreas.areas_id == dfSellerProvince.buyer_company_city_id], 'left') \
        .drop(dfAreas.areas_id) \
        .withColumnRenamed('type', 'buyer_company_province_type') \
        .withColumnRenamed('parent_id', 'buyer_company_province_id')
    # 连续获得买方用户地区信息
    # dfBuyerUserAreas1 = order_detail_id            out_qty                   receive_qty                    total_out_price             total_receive_price
    #                     seller_alliance_id         seller_company_id         seller_company_area_id         buyer_user_area_id          buyer_company_area_id
    #                     oe_code                    name                      brand                          sw_production_place         sku_id
    #                     property                   seller_company_area_type  seller_company_city_id         buyer_company_area_type     buyer_company_city_id
    #                     buyer_user_area_type       buyer_user_city_id        seller_company_province_type   seller_company_province_id  buyer_company_province_type
    #                     buyer_company_province_id  buyer_user_province_type  buyer_user_province_id
    dfBuyerUserProvince = dfBuyerCompanyProvince \
        .join(dfAreas, [dfAreas.areas_id == dfBuyerCompanyProvince.buyer_user_city_id], 'left') \
        .drop(dfAreas.areas_id) \
        .withColumnRenamed('type', 'buyer_user_province_type') \
        .withColumnRenamed('parent_id', 'buyer_user_province_id')
    #买卖双方城市基础信息
    #dfCityProvince = order_detail_id     out_qty                 receive_qty                  total_out_price          total_receive_price
    #                 seller_alliance_id  seller_company_id       seller_company_area_id       buyer_user_area_id       buyer_company_area_id
    #                 oe_code             name                    brand                        sw_production_place      sku_id
    #                 property            seller_company_city_id  seller_company_province_id   buyer_company_city_id    buyer_company_province_id
    #                 buyer_user_city_id  buyer_user_province_id  city_flag                    province_flag
    mapCityProvince = dfBuyerUserProvince \
        .select('order_detail_id','out_qty','receive_qty','total_out_price','total_receive_price',
                'seller_alliance_id','seller_company_id','seller_company_area_id','buyer_user_area_id','buyer_company_area_id',
                'oe_code','name','brand','sw_production_place','sku_id',
                'property','seller_company_city_id','seller_company_province_id','buyer_company_city_id','buyer_company_province_id',
                'buyer_user_city_id','buyer_user_province_id') \
        .na.fill(0) \
        .rdd.map(mapAreaFlag)
    dfCityProvince = sqlContext.createDataFrame(mapCityProvince,[
        'order_detail_id','out_qty','receive_qty','total_out_price','total_receive_price',
        'seller_alliance_id','seller_company_id','seller_company_area_id','buyer_user_area_id','buyer_company_area_id',
        'oe_code','name','brand','sw_production_place','sku_id',
        'property','seller_company_city_id','seller_company_province_id','buyer_company_city_id','buyer_company_province_id',
        'buyer_user_city_id','buyer_user_province_id','city_flag','province_flag'])
    saveHiveDatabase('dm_city_provice_qty_price', dfCityProvince, hiveCol['dm_city_provice_qty_price'], dateStrS)
    return dfCityProvince

"""
调用位置：getOrderCityProvinceInfo()
作用：找出区域flag
return: order_detail_id     out_qty                 receive_qty                  total_out_price          total_receive_price
        seller_alliance_id  seller_company_id       seller_company_area_id       buyer_user_area_id       buyer_company_area_id
        oe_code             name                    brand                        sw_production_place      sku_id
        property            seller_company_city_id  seller_company_province_id   buyer_company_city_id    buyer_company_province_id
        buyer_user_city_id  buyer_user_province_id  city_flag                    province_flag
"""
def mapAreaFlag(p):
    order_detail_id, out_qty, receive_qty, total_out_price, total_receive_price,seller_alliance_id, seller_company_id, seller_company_area_id, buyer_user_area_id, buyer_company_area_id,oe_code, name, brand, sw_production_place, sku_id,property, seller_company_city_id, seller_company_province_id, buyer_company_city_id, buyer_company_province_id,buyer_user_city_id, buyer_user_province_id = p
    # flag
    # 买房 公司有   用户有，不同区域  = 1
    # 买房 公司有   用户有，同区域    = 2
    # 买房 公司有   用户无          = 3
    # 买房 公司无   用户有          = 4
    # 买房 公司无   用户无          = 0
    if buyer_user_city_id != 0 and buyer_company_city_id != 0 :
        if buyer_user_city_id != buyer_company_city_id :
            city_flag = 1
        else:
            city_flag = 2
    else:
        if buyer_user_city_id == 0 and buyer_company_city_id != 0:
            city_flag = 3
        elif buyer_user_city_id != 0 and buyer_company_city_id == 0:
            city_flag = 4
        else:
            city_flag =0
    if buyer_user_province_id != 0 and buyer_company_province_id != 0 :
        if buyer_user_province_id != buyer_company_province_id :
            province_flag = 1
        else:
            province_flag = 2
    else:
        if buyer_user_province_id == 0 and buyer_company_province_id != 0:
            province_flag = 3
        elif buyer_user_province_id != 0 and buyer_company_province_id == 0:
            province_flag = 4
        else:
            province_flag =0
    return order_detail_id, out_qty, receive_qty, total_out_price, total_receive_price,seller_alliance_id, seller_company_id, seller_company_area_id, buyer_user_area_id, buyer_company_area_id,oe_code, name, brand, sw_production_place, sku_id,property, seller_company_city_id, seller_company_province_id, buyer_company_city_id, buyer_company_province_id,buyer_user_city_id, buyer_user_province_id,city_flag,province_flag

"""
统计
客户与成员数据概览：（联盟维度） 图1 总成员量，图2 新增成员量
dfMemberAll = alliance_id  total_member
dfMemberNew = alliance_id  new_member
"""
def getMemberAllNew(dfMember,dateStrS,dateStrE):
    # 联盟成员总数统计
    dfMemberAll = dfMember\
        .groupBy('alliance_id') \
        .agg({'member_id': 'count'})\
        .withColumnRenamed('count(member_id)', 'total_member').na.fill(0)
    # 联盟新成员统计（加入时间，在昨天）
    dfMemberNew = dfMember\
        .where(dfMember.join_time <= dateStrE) \
        .where(dfMember.join_time >= dateStrS) \
        .groupBy('alliance_id') \
        .agg({'member_id': 'count'}) \
        .withColumnRenamed('count(member_id)', 'new_member').na.fill(0)
    return dfMemberAll,dfMemberNew

"""
统计
客户与成员数据概览： 图1 总客户量，图2 新增客户量
dfCustomerAll = alliance_id  member_id  total_customer
dfCustomerNew = alliance_id  member_id  new_customer
"""
def getCustomerAllNew(dfCustomer,dfAllianceCustomer,dfMember,dateStrS,dateStrE):
    #查找有联盟的成员公司所有客户信息
    #dfMemberCustomer = alliance_id  member_id  join_time  company_customer_id  company_customer_type
    #                   created_at
    dfMemberCustomer = dfMember\
        .join(dfCustomer,[dfMember.member_id == dfCustomer.company_id],'leftouter')\
        .drop(dfCustomer.company_id)\
        .withColumnRenamed('customer_id','company_customer_id')\
        .withColumnRenamed('customer_type','company_customer_type')
    #筛选掉非联盟客户
    # dfAllianceMemberCustomer = alliance_id  member_id  join_time  company_customer_id  company_customer_type
    #                            created_at
    dfAllianceMemberCustomer = dfMemberCustomer\
        .join(dfAllianceCustomer,[dfMemberCustomer.alliance_id == dfAllianceCustomer.alliance_id,
                                  dfMemberCustomer.company_customer_id == dfAllianceCustomer.customer_id,
                                  dfMemberCustomer.company_customer_type == dfAllianceCustomer.customer_type],'inner')\
        .drop(dfAllianceCustomer.alliance_id)\
        .drop(dfAllianceCustomer.customer_id)\
        .drop(dfAllianceCustomer.customer_type)\
        .drop(dfAllianceCustomer.created_at)
    saveHiveDatabase('dm_alliance_member_customer',dfMemberCustomer,hiveCol['dm_alliance_member_customer'],dateStrS)
    # 联盟客户总数统计
    # dfCustomerAll = alliance_id  member_id  total_customer
    dfCustomerAll = dfAllianceMemberCustomer\
        .groupBy('alliance_id','member_id')\
        .agg(count('company_customer_id').alias('total_customer'))
    # 联盟新客户统计
    # dfCustomerNew = alliance_id  member_id  new_customer
    dfCustomerNew = dfAllianceMemberCustomer\
        .where(dfAllianceMemberCustomer.created_at <= dateStrE)\
        .where(dfAllianceMemberCustomer.created_at >= dateStrS)\
        .groupBy('alliance_id', 'member_id') \
        .agg(count('company_customer_id').alias('new_customer'))
    return dfCustomerAll,dfCustomerNew

"""
统计
客户与成员数据概览：   图3 活跃客户及活跃成员量
dfActiveMember           = alliance_id  active_member_count
dfAllianceActiveCustomer = alliance_id  active_customer_count
dfMemberActiveCustomer   = alliance_id  company_id             active_customer_count
保存 （dm_avtive_customer）
"""
def getActiveMemberCustomer(dfMember,dfAllianceCustomer,dfStaffs,dfCustomer,dateStrS):
    # 联盟活跃客户及成员数据获取
    #dfActive = user_id
    hql = "select int(user_id) as user_id " \
          " from orange.ods_es_active_customer " \
          " where day = '" + dateStrS + "'"
    dfActive = exeHql(hql)
    # 查找活跃用户所在公司
    #dfActive = user_id  company_id
    dfActive = dfActive \
        .join(dfStaffs, [dfActive.user_id == dfStaffs.user_id], 'left') \
        .drop(dfStaffs.user_id)\
        .na.fill(0)

    # 获取活跃成员
    # dfActiveAllianceCompany = user_id  company_id  alliance_id
    dfActiveAllianceCompany = dfActive \
        .join(dfMember, [dfActive.company_id == dfMember.member_id], 'left') \
        .drop(dfMember.member_id)\
        .drop(dfMember.join_time)
    #dfActiveMember = alliance_id  active_member_count
    dfActiveMember = dfActiveAllianceCompany \
        .groupBy("alliance_id") \
        .agg({'user_id': 'count'}) \
        .withColumnRenamed('count(user_id)', 'active_member_count').na.fill(0)

    # 获取活跃客户(分别找出公司客户和个人客户合并)
    # 查找出公司客户
    # dfCompanyCustomer = alliance_id customer_id  created_at   customer_type
    dfCompanyCustomer = dfAllianceCustomer \
        .where(dfAllianceCustomer.customer_type == 1)
    #查出活跃用户中，公司客户所在联盟
    # dfActiveCompany = user_id company_id alliance_id customer_type
    dfActiveCompany = dfActive\
        .join(dfCompanyCustomer, [dfActive.company_id == dfCompanyCustomer.customer_id], 'leftouter') \
        .select(dfActive.user_id,dfActive.company_id,'alliance_id','customer_type')
    # 个人客户
    #dfPersonelCustomer = alliance_id customer_id  created_at   customer_type
    dfPersonelCustomer = dfAllianceCustomer \
        .where(dfAllianceCustomer.customer_type == 0)
    # 查出活跃用户中，个人客户所在联盟
    # dfActiveCompany = user_id company_id alliance_id
    dfActivPersonel = dfActive \
        .join(dfPersonelCustomer, [dfActive.user_id == dfPersonelCustomer.customer_id], 'leftouter') \
        .select(dfActive.user_id,dfActive.company_id,'alliance_id','customer_type')
    # 活跃联盟客户合并公司及个人
    #dfActiveCustomer = user_id company_id alliance_id customer_type
    dfActiveCustomer = dfActiveCompany \
        .unionAll(dfActivPersonel)\
        .na.fill(0)

    # 求出联盟活跃客户
    # dfAllianceActiveCustomer = alliance_id  active_customer_count
    dfAllianceActiveCustomer = dfActiveCustomer\
        .select('alliance_id','company_id','user_id')\
        .distinct()\
        .groupBy("alliance_id") \
        .agg({'user_id': 'count'}) \
        .withColumnRenamed('count(user_id)', 'active_customer_count').na.fill(0)
    # 找出公司客户
    # dfActiveCustomer = company_id customer_company_id user_id alliance_id
    dfActiveCustomer = dfActiveCustomer \
        .join(dfCustomer, [dfActiveCustomer.company_id == dfCustomer.customer_id,
                           dfActiveCustomer.customer_type == dfCustomer.customer_type], 'leftouter') \
        .select(dfCustomer.company_id,dfActiveCustomer.company_id.alias('customer_company_id'),dfActiveCustomer.user_id,dfActiveCustomer.alliance_id)\
        .distinct()
    # 求出联盟成员活跃客户
    # dfMemberActiveCustomer =  alliance_id  company_id  active_customer_count
    dfMemberActiveCustomer = dfActiveCustomer \
        .groupBy("alliance_id", "company_id") \
        .agg(count("alliance_id").alias("active_customer_count"))
    return dfActiveMember,dfAllianceActiveCustomer,dfMemberActiveCustomer

"""
统计
上下架商品数          商品信息概览: (联盟维度）  图3 上下架商品量
dfAllianceOn  = alliance_id  alliance_onshelve_sum
dfAllianceOut = alliance_id  alliance_outshelve_sum
"""
def getoutonShelve(dateStrS):
    # 获取当天上下架数据
    hql = 'select int(company_id) as company_id,int(alliance_id) as alliance_id,onshelve_sum' \
          ' from orange.ods_es_onshelve' \
          ' where day = \'' + dateStrS + '\''
    dfOnShelve = exeHql(hql)
    hql = 'select int(company_id) as company_id,int(alliance_id) as alliance_id,outshelve_sum' \
          ' from orange.ods_es_outshelve' \
          ' where day = \'' + dateStrS + '\''
    dfOutShelve = exeHql(hql)
    dfAllianceOn = dfOnShelve\
        .groupBy("alliance_id") \
        .agg({'onshelve_sum': 'count'}) \
        .withColumnRenamed('count(onshelve_sum)', 'alliance_onshelve_sum').na.fill(0)
    dfAllianceOut = dfOutShelve \
        .groupBy("alliance_id") \
        .agg({'outshelve_sum': 'count'}) \
        .withColumnRenamed('count(outshelve_sum)', 'alliance_outshelve_sum').na.fill(0)
    return dfAllianceOn,dfAllianceOut

"""
统计
下单客户量    图4 下单客户量
dfOrderCustomerNum = seller_company_id seller_alliance_id customer_num
"""
def getOrderCustomerNum(dfMember,dfAllianceCustomer,dateStrS,dateStrE):
    #获得订单购买双方信息
    #dfOrdersInfo = seller_company_id  buyer_user_id  buyer_company_id
    hql = 'select seller_company_id,buyer_user_id,buyer_company_id' \
          ' from orange.ods_psql_orders' \
          ' where day = \'' + dateStrS + '\' and created_at >= \'' + dateStrS + '\' and created_at <= \'' + dateStrE + '\''
    dfOrders = exeHql(hql)
    hql = 'select seller_company_id,buyer_user_id,buyer_company_id' \
          ' from orange.ods_psql_orders_history' \
          ' where day = \'' + dateStrS + '\' and created_at >= \'' + dateStrS + '\' and created_at <= \'' + dateStrE + '\''
    dfOrdersHistory = exeHql(hql)
    dfOrdersInfo = dfOrders\
        .unionAll(dfOrdersHistory)\
        .distinct()\
        .na.fill(0)
    #获得卖方联盟
    #dfMembersOrders = seller_company_id  buyer_user_id  buyer_company_id  seller_alliance_id
    dfMembersOrders = dfOrdersInfo\
        .join(dfMember,[dfMember.member_id == dfOrdersInfo.seller_company_id],'inner')\
        .select(dfOrdersInfo.seller_company_id,dfOrdersInfo.buyer_user_id,dfOrdersInfo.buyer_company_id,dfMember.alliance_id.alias('seller_alliance_id'))
    #得到买方联盟
    dfAllianceCustomerCompany = dfAllianceCustomer \
        .where(dfAllianceCustomer.customer_type == 1)
    dfAllianceCustomerPerson = dfAllianceCustomer \
        .where(dfAllianceCustomer.customer_type == 0)
    #获得买卖双方在统一联盟订单信息
    # dfMembersOrders = seller_company_id  buyer_user_id  buyer_company_id  seller_alliance_id buyer_alliance_id
    dfCompanyCustomerOrder = dfMembersOrders\
        .where(dfMembersOrders.buyer_company_id != 0)\
        .join(dfAllianceCustomerCompany,[dfMembersOrders.buyer_company_id == dfAllianceCustomerCompany.customer_id],'leftouter')\
        .select(dfMembersOrders.seller_company_id,dfMembersOrders.buyer_user_id,dfMembersOrders.buyer_company_id,dfMembersOrders.seller_alliance_id,dfAllianceCustomerCompany.alliance_id.alias('buyer_alliance_id'))
    dfPersonCustomerOrder = dfMembersOrders \
        .where(dfMembersOrders.buyer_company_id == 0) \
        .join(dfAllianceCustomerCompany, [dfMembersOrders.buyer_user_id == dfAllianceCustomerCompany.customer_id],'leftouter') \
        .select(dfMembersOrders.seller_company_id, dfMembersOrders.buyer_user_id, dfMembersOrders.buyer_company_id,
                dfMembersOrders.seller_alliance_id, dfAllianceCustomerCompany.alliance_id.alias('buyer_alliance_id'))
    dfOrdersInfo = dfCompanyCustomerOrder\
        .unionAll(dfPersonCustomerOrder)\
        .distinct()\
        .where(dfCompanyCustomerOrder.seller_alliance_id == dfCompanyCustomerOrder.buyer_alliance_id)
    #求出下单客户量
    #dfOrderCustomerNum = seller_company_id seller_alliance_id customer_num
    dfOrderCustomerNum = dfOrdersInfo\
        .groupBy('seller_company_id','seller_alliance_id')\
        .agg(count('seller_alliance_id').alias('customer_num'))
    return dfOrderCustomerNum

"""
统计
统计修理厂查询数据         客户与成员数据概览: (联盟维度）  图5 修理厂查询排行 
dfGarageCheckInfo  =  alliance_id  buyer_company_id  buyer_company_name  garage_check_num         max_sku_id
                      max_oe_code  max_brand         max_sku_name        max_sw_production_place  max_garage_sku_count
保存（dm_panel_alliance_garage_check）
"""
def getGarageInfo(dfSkuValidCheckInfo,dfCompanies,dateStrS):
    # 获取修理查询总次数
    #dfGarageValidCheck = alliance_id  seller_company_id  buyer_company_id  garage_check_num
    dfGarageValidCheck = dfSkuValidCheckInfo \
        .where(dfSkuValidCheckInfo.buyer_company_type == 'garage') \
        .groupBy('alliance_id','seller_company_id','buyer_company_id') \
        .agg(sum('checkcount').alias("garage_check_num"))
    # 获取联盟公司商品查询次数
    #dfGarageValidCheckSkuMaxSum = alliance_id  seller_company_id   buyer_company_id     sku_id            oe_code
    #                              brand        sku_name            sw_production_place  garage_sku_count
    dfGarageValidCheckSkuMaxSum = dfSkuValidCheckInfo \
        .where(dfSkuValidCheckInfo.buyer_company_type == 'garage') \
        .groupBy('alliance_id','seller_company_id','buyer_company_id', 'sku_id', 'oe_code', 'brand', 'sku_name', 'sw_production_place') \
        .agg(sum('checkcount').alias('garage_sku_count'))\
        .na.fill("")\
        .na.fill(0)
    #合并修理厂查询次数最多sku信息
    #判断rdd是否为空（排错用）
    #dfGarageCheckInfo = alliance_id  seller_company_id buyer_company_id         max_sku_id            max_oe_code
    #                    max_brand    max_sku_name      max_sw_production_place  max_garage_sku_count
    if dfGarageValidCheckSkuMaxSum.rdd.isEmpty():
        #rdd为空，直接并入空值
        dfGarageCheckInfo = dfGarageValidCheck\
            .withColumn('max_sku_id',lit(0))\
            .withColumn('max_oe_code',lit(""))\
            .withColumn('max_brand',lit(""))\
            .withColumn('max_sku_name',lit(""))\
            .withColumn('max_sw_production_place',lit(""))\
            .withColumn('max_garage_sku_count',lit(0))
    else:
        #找出该修理厂查询次数最多的sku
        mapGarageCheckSku = dfGarageValidCheckSkuMaxSum \
            .rdd \
            .map(lambda p: ((p.alliance_id,p.seller_company_id,p.buyer_company_id), [(p.sku_id, p.oe_code, p.brand, p.sku_name, p.sw_production_place,
                                                                                      p.garage_sku_count)])) \
            .reduceByKey(aggWithKey) \
            .map(lambda x: (x[0][0],x[0][1],x[0][2], x[1])) \
            .map(mapMaxGarageSkuCount)
        dfGarageCheckSkuMax = sqlContext.createDataFrame(mapGarageCheckSku,['alliance_id','seller_company_id','buyer_company_id', 'max_sku_id', 'max_oe_code',
                                                                            'max_brand', 'max_sku_name','max_sw_production_place', 'max_garage_sku_count'])
        #合并修理厂查询次数以及该修理厂查询次数最多的sku
        dfGarageCheckInfo = dfGarageValidCheck\
            .join(dfGarageCheckSkuMax,[dfGarageValidCheck.buyer_company_id == dfGarageCheckSkuMax.buyer_company_id,
                                       dfGarageValidCheck.alliance_id == dfGarageCheckSkuMax.alliance_id,
                                       dfGarageValidCheck.seller_company_id == dfGarageCheckSkuMax.seller_company_id],'left')\
            .drop(dfGarageCheckSkuMax.buyer_company_id)\
            .drop(dfGarageCheckSkuMax.alliance_id) \
            .drop(dfGarageCheckSkuMax.seller_company_id)
    #获取修理厂全量信息
    #dfAllianceMemberCustomer = alliance_id seller_company_id  company_customer_id
    hql = 'select alliance_id,member_id seller_company_id,company_customer_id customer_id' \
          ' from orange_dm.dm_alliance_member_customer' \
          ' where day = \'' +dateStrS + '\''
    dfAllianceMemberCustomer = exeHql(hql)
    ##dfAllianceMemberCustomer = alliance_id  seller_company_id  customer_id  garage_name
    dfAllianceMemberCustomer = dfAllianceMemberCustomer\
        .join(dfCompanies,[dfCompanies.company_id == dfAllianceMemberCustomer.customer_id],'left')\
        .where(dfCompanies.company_type == 'garage') \
        .drop(dfCompanies.company_id) \
        .drop(dfCompanies.company_type) \
        .drop(dfCompanies.area_id) \
        .withColumnRenamed('company_name', 'garage_name')
    #dfAllianceMemberCustomer = alliance_id  seller_company_id  customer_id    garage_name                max_sku_id
    #                           max_oe_code  max_brand          max_sku_name   max_sw_production_place    max_garage_sku_count
    dfGarageCheckInfo = dfAllianceMemberCustomer \
        .join(dfGarageCheckInfo, [dfAllianceMemberCustomer.seller_company_id == dfGarageCheckInfo.seller_company_id,
                                  dfAllianceMemberCustomer.alliance_id == dfGarageCheckInfo.alliance_id,
                                  dfAllianceMemberCustomer.customer_id == dfGarageCheckInfo.buyer_company_id], 'left') \
        .drop(dfGarageCheckInfo.seller_company_id) \
        .drop(dfGarageCheckInfo.alliance_id) \
        .drop(dfGarageCheckInfo.buyer_company_id)\
        .na.fill(0)
    saveHiveDatabase('dm_panel_alliance_garage_check', dfGarageCheckInfo, hiveCol['dm_panel_alliance_garage_check'], dateStrS)

"""
调用位置：getGarageInfo()
作用：找出查询次数最多的商品
return: alliance_id  buyer_company_id,  buyer_company_id          max_sku_id,           max_oe_code, 
        max_brand,   max_sku_name       max_sw_production_place,  max_garage_sku_count
"""
def mapMaxGarageSkuCount(p):
    alliance_id = p[0]
    seller_company_id = p[1]
    buyer_company_id = p[2]
    list = p[3]
    max_garage_sku_count = 0
    for item in list:
        if max_garage_sku_count <= item[5]:
            max_sku_id = item[0]
            max_oe_code = item[1]
            max_brand = item[2]
            max_sku_name = item[3]
            max_sw_production_place = item[4]
            max_garage_sku_count = item[5]
    return alliance_id,seller_company_id,buyer_company_id,max_sku_id,max_oe_code,max_brand,max_sku_name,max_sw_production_place,max_garage_sku_count

"""
统计
成员销售排行数据         客户与成员数据概览：（联盟维度/配件商） 图6 配件商排行榜
dfAllianceMemberSaleQty = seller_company_id  seller_alliance_id  alliance_member_sale_qty
"""
def getAllianceMemberSaleQty(dfOrdersDetailsInfo):
    dfAllianceMemberSaleQty = dfOrdersDetailsInfo\
        .where(dfOrdersDetailsInfo.out_qty > 0)\
        .groupBy('seller_company_id','seller_alliance_id')\
        .agg(sum('out_qty').alias('alliance_member_sale_qty'))
    return dfAllianceMemberSaleQty

"""
统计
统计价格分析     商品信息概览（联盟维度）    图5（下钻）
dfPriceAnalysis = seller_alliance_id   alliance_part_id   name            brand           oe_code
                  sw_production_place  max_sell_price     min_sell_price  avg_sell_price  max_receive_price
                  min_receive_price    avg_receive_price  sale_qty
"""
def getPriceAnalysis(dfOrdersDetailsInfo,dateStrS):
    #获得售价数据
    #dfPriceSellAnalysis = seller_alliance_id   alliance_part_id   name            brand              oe_code
    #                      sw_production_place  max_sell_price     min_sell_price  avg_sell_price
    dfPriceSellAnalysis = dfOrdersDetailsInfo\
        .where(dfOrdersDetailsInfo.out_qty > 0)\
        .groupBy('seller_alliance_id','alliance_part_id','name','brand','oe_code','sw_production_place')\
        .agg(max('price').alias('max_sell_price'),
             min('price').alias('min_sell_price'),
             avg('price').alias('avg_sell_price'))
    #获得进价数据
    # dfPricePurchaseAnalysis = seller_alliance_id   alliance_part_id    name               brand              oe_code
    #                           sw_production_place  max_receive_price   min_receive_price  avg_receive_price
    dfPricePurchaseAnalysis = dfOrdersDetailsInfo \
        .where(dfOrdersDetailsInfo.receive_qty > 0) \
        .groupBy('seller_alliance_id','alliance_part_id', 'name', 'brand', 'oe_code', 'sw_production_place') \
        .agg(max('price').alias('max_receive_price'),
             min('price').alias('min_receive_price'),
             avg('price').alias('avg_receive_price'))
    #获得销量数据
    # dfPricePurchaseAnalysis = seller_alliance_id   alliance_part_id  name  brand  oe_code
    #                           sw_production_place  sale_qty
    dfQty = dfOrdersDetailsInfo\
        .groupBy('seller_alliance_id','alliance_part_id', 'name', 'brand', 'oe_code', 'sw_production_place')\
        .agg(sum('out_qty').alias('sale_qty'))
    #合并价格分析数据
    #dfPriceAnalysis = seller_alliance_id   alliance_part_id   name            brand           oe_code
    #                  sw_production_place  max_sell_price     min_sell_price  avg_sell_price  max_receive_price
    #                  min_receive_price    avg_receive_price  sale_qty
    dfPriceAnalysis = dfOrdersDetailsInfo\
        .select('seller_alliance_id','alliance_part_id', 'name', 'brand', 'oe_code', 'sw_production_place')\
        .distinct()\
        .join(dfPriceSellAnalysis,[dfOrdersDetailsInfo.seller_alliance_id == dfPriceSellAnalysis.seller_alliance_id,
                                   dfOrdersDetailsInfo.name == dfPriceSellAnalysis.name,
                                   dfOrdersDetailsInfo.brand == dfPriceSellAnalysis.brand,
                                   dfOrdersDetailsInfo.oe_code == dfPriceSellAnalysis.oe_code,
                                   dfOrdersDetailsInfo.alliance_part_id == dfPriceSellAnalysis.alliance_part_id,
                                   dfOrdersDetailsInfo.sw_production_place == dfPriceSellAnalysis.sw_production_place],'left') \
        .drop(dfPriceSellAnalysis.seller_alliance_id) \
        .drop(dfPriceSellAnalysis.name)\
        .drop(dfPriceSellAnalysis.brand)\
        .drop(dfPriceSellAnalysis.oe_code) \
        .drop(dfPriceSellAnalysis.alliance_part_id) \
        .drop(dfPriceSellAnalysis.sw_production_place)
    dfPriceAnalysis = dfPriceAnalysis\
        .join(dfPricePurchaseAnalysis,[dfPriceAnalysis.seller_alliance_id == dfPricePurchaseAnalysis.seller_alliance_id,
                                       dfPriceAnalysis.name == dfPricePurchaseAnalysis.name,
                                       dfPriceAnalysis.brand == dfPricePurchaseAnalysis.brand,
                                       dfPriceAnalysis.oe_code == dfPricePurchaseAnalysis.oe_code,
                                       dfPriceAnalysis.alliance_part_id == dfPricePurchaseAnalysis.alliance_part_id,
                                       dfPriceAnalysis.sw_production_place == dfPricePurchaseAnalysis.sw_production_place],'left') \
        .drop(dfPricePurchaseAnalysis.seller_alliance_id) \
        .drop(dfPricePurchaseAnalysis.name)\
        .drop(dfPricePurchaseAnalysis.brand)\
        .drop(dfPricePurchaseAnalysis.oe_code) \
        .drop(dfPricePurchaseAnalysis.alliance_part_id)\
        .drop(dfPricePurchaseAnalysis.sw_production_place)
    dfPriceAnalysis = dfPriceAnalysis\
        .join(dfQty,[dfPriceAnalysis.seller_alliance_id == dfQty.seller_alliance_id,
                     dfPriceAnalysis.name == dfQty.name,
                     dfPriceAnalysis.brand == dfQty.brand,
                     dfPriceAnalysis.oe_code == dfQty.oe_code,
                     dfPriceAnalysis.alliance_part_id == dfQty.alliance_part_id,
                     dfPriceAnalysis.sw_production_place == dfQty.sw_production_place],'left') \
        .drop(dfQty.seller_alliance_id) \
        .drop(dfQty.name)\
        .drop(dfQty.brand)\
        .drop(dfQty.oe_code) \
        .drop(dfQty.alliance_part_id)\
        .drop(dfQty.sw_production_place)
    saveHiveDatabase('dm_price_analysis',dfPriceAnalysis,hiveCol['dm_price_analysis'],dateStrS)

"""
统计
统计经营指标： （联盟维度）  图5（下钻）每日采购量+金额（总和，分类）
dfPurchaseDetail = company_buyer_alliance_id  alliance_parts_id  name           oe_code             brand  
                   sw_production_place        purchase_qty       purchase_price
"""
def getPurchaseInfo(dfOrdersDetailsInfoAll,dateStrS):
    #找到买家联盟id
    # dfOrdersDetails = order_details_id     company_buyer_alliance_id  name         oe_code  brand
    #                   sw_production_place  receive_qty                total_price
    dfOrdersDetails = dfOrdersDetailsInfoAll\
        .where(dfOrdersDetailsInfoAll.receive_qty>0)\
        .withColumn('total_price',col('price')*col('receive_qty'))
    #每日采购详情
    #dfPurchaseDetail = buyer_alliance_id    name            oe_code  brand
    #                   sw_production_place  purchase_qty    purchase_price
    dfPurchaseDetail = dfOrdersDetails\
        .groupBy('company_buyer_alliance_id','name','oe_code','brand','sw_production_place')\
        .agg(sum('receive_qty').alias('purchase_qty'),sum('total_price').alias('purchase_price'))
    saveHiveDatabase('dm_purchase_detail',dfPurchaseDetail,hiveCol['dm_purchase_detail'],dateStrS)

"""
统计
统计配件意向查询量及销量  经营指标概览（联盟维度）  图2，3，4 查销比例排行，意向查询量排行，销量排行
dfPartsQtyCheck = alliance_id  seller_company_id  sku_id  oe_code  name  brand  sw_production_place  parts_qty  parts_checkcount
"""
def getPartsQtyCheck(dfOrdersDetailsInfo,dfSkuValidCheckInfo,dateStrS):
    dfPartsQty = dfOrdersDetailsInfo\
        .groupBy('seller_alliance_id','seller_company_id','sku_id','oe_code','name','brand','sw_production_place')\
        .agg(sum('out_qty').alias('parts_qty'))
    dfPartsCheck = dfSkuValidCheckInfo\
        .groupBy('alliance_id','seller_company_id','sku_id','oe_code','sku_name','brand','sw_production_place')\
        .agg(sum('checkcount').alias('parts_checkcount'))
    dfPartsQtyCheck = dfPartsQty\
        .join(dfPartsCheck,[dfPartsQty.seller_alliance_id == dfPartsCheck.alliance_id,
                            dfPartsQty.sku_id == dfPartsCheck.sku_id,
                            dfPartsQty.seller_company_id == dfPartsCheck.seller_company_id],'leftouter')\
        .select(when(dfPartsQty.seller_alliance_id.isNull(),dfPartsCheck.alliance_id).otherwise(dfPartsQty.seller_alliance_id).alias('alliance_id'),
                when(dfPartsQty.seller_company_id.isNull(), dfPartsCheck.seller_company_id).otherwise(dfPartsQty.seller_company_id).alias('seller_company_id'),
                when(dfPartsQty.sku_id.isNull(),dfPartsCheck.sku_id).otherwise(dfPartsQty.sku_id).alias('sku_id'),
                when(dfPartsQty.oe_code.isNull(),dfPartsCheck.oe_code).otherwise(dfPartsQty.oe_code).alias('oe_code'),
                when(dfPartsQty.name.isNull(),dfPartsCheck.sku_name).otherwise(dfPartsQty.name).alias('name'),
                when(dfPartsQty.brand.isNull(),dfPartsCheck.brand).otherwise(dfPartsQty.brand).alias('brand'),
                when(dfPartsQty.sw_production_place.isNull(),dfPartsCheck.sw_production_place).otherwise(dfPartsQty.sw_production_place).alias('sw_production_place'),
                dfPartsQty.parts_qty,dfPartsCheck.parts_checkcount)\
        .na.fill(0)
    saveHiveDatabase('dm_alliance_company_parts_saleqty_check',dfPartsQtyCheck,hiveCol['dm_alliance_company_parts_saleqty_check'],dateStrS)

"""
统计
统计成交数量分布   经营指标概览（联盟维度）    图6，7
dfBuyerProvince  = seller_alliance_id  buyer_province_area_id         buyer_province_out_qty    buyer_province_total_price
dfBuyerCity      = seller_alliance_id  buyer_city_area_id             buyer_city_out_qty        buyer_city_total_price
dfSellerProvince = seller_alliance_id  seller_company_area_parent_id  seller_province_out_qty   seller_province_total_price
dfSellerCity     = seller_alliance_id  seller_company_province_id     seller_city_out_qty       seller_city_total_price


dfCityProvince = order_detail_id     out_qty                 receive_qty                  total_out_price          total_receive_price
                 seller_alliance_id  seller_company_id       seller_company_area_id       buyer_user_area_id       buyer_company_area_id
                 oe_code             name                    brand                        sw_production_place      sku_id
                 property            seller_company_city_id  seller_company_province_id   buyer_company_city_id    buyer_company_province_id
                 buyer_user_city_id  buyer_user_province_id  city_flag                    province_flag
"""
def getOrderAreaQtyPrice(dfCityProvince,dateStrS):
    #统计卖方：市级
    #dfSellerCity = seller_alliance_id  seller_company_id  oe_code  brand  sw_production_place  city_id  seller_city_out_qty  seller_city_total_price
    dfSellerCity = dfCityProvince\
        .groupBy('seller_alliance_id','seller_company_id','oe_code','brand','sw_production_place','seller_company_city_id')\
        .agg(sum('out_qty').alias('seller_city_qty'),
             sum('total_out_price').alias('seller_city_price'))\
        .withColumnRenamed('seller_company_city_id','city_id')\
        .withColumn('buyer_city_qty',lit(0.0))\
        .withColumn('buyer_city_price', lit(0.0))
    #统计卖方：省级
    #dfSellerProvince = seller_alliance_id seller_company_id oe_code  brand  sw_production_place province_id  seller_province_out_qty  seller_province_total_price
    dfSellerProvince = dfCityProvince\
        .groupBy('seller_alliance_id','seller_company_id','oe_code','brand','sw_production_place', 'seller_company_province_id')\
        .agg(sum('out_qty').alias('seller_province_qty'),
             sum('total_out_price').alias('seller_province_price'))\
        .withColumnRenamed('seller_company_province_id','province_id')\
        .withColumn('buyer_province_qty', lit(0.0)) \
        .withColumn('buyer_province_price', lit(0.0))
    #统计买方市级：
    #dfBuyerCityUser = seller_alliance_id seller_company_id oe_code  brand  sw_production_place province_id city_id buyer_city_out_qty buyer_city_total_price
    dfBuyerCityUser = dfCityProvince\
        .where((dfCityProvince.city_flag == 1) | (dfCityProvince.city_flag == 2) | (dfCityProvince.city_flag == 4))\
        .groupBy('seller_alliance_id','seller_company_id','oe_code','brand','sw_production_place','buyer_user_city_id')\
        .agg(sum('receive_qty').alias('buyer_city_qty'),
             sum('total_receive_price').alias('buyer_city_price'))\
        .withColumnRenamed('buyer_user_city_id','city_id')
    dfBuyerCityCompany = dfCityProvince \
        .where((dfCityProvince.city_flag == 1) | (dfCityProvince.city_flag == 3)) \
        .groupBy('seller_alliance_id', 'seller_company_id','oe_code','brand','sw_production_place','buyer_user_city_id') \
        .agg(sum('receive_qty').alias('buyer_city_qty'),
             sum('total_receive_price').alias('buyer_city_price')) \
        .withColumnRenamed('buyer_user_city_id', 'city_id')
    dfBuyerCity = dfBuyerCityUser\
        .unionAll(dfBuyerCityCompany)\
        .withColumn('seller_city_qty',lit(0.0)) \
        .withColumn('seller_city_price',lit(0.0)) \
        .groupBy('seller_alliance_id','seller_company_id','oe_code','brand','sw_production_place', 'city_id') \
        .agg(sum('seller_city_qty').alias('seller_city_qty'),
             sum('seller_city_price').alias('seller_city_price'),
             sum('buyer_city_qty').alias('buyer_city_qty'),
             sum('buyer_city_price').alias('buyer_city_price'))
    # 统计买方省级：
    # dfBuyerCityUser = seller_alliance_id seller_company_id oe_code  brand  sw_production_place province_id buyer_province_out_qty buyer_province_total_price
    dfBuyerProvinceUser = dfCityProvince \
        .where((dfCityProvince.province_flag == 1) | (dfCityProvince.province_flag == 2) | (dfCityProvince.province_flag == 4)) \
        .groupBy('seller_alliance_id','seller_company_id','oe_code','brand','sw_production_place', 'buyer_user_province_id') \
        .agg(sum('receive_qty').alias('buyer_province_qty'),
             sum('total_receive_price').alias('buyer_province_price')) \
        .withColumnRenamed('buyer_user_province_id', 'province_id')
    dfBuyerProvinceCompany = dfCityProvince \
        .where((dfCityProvince.province_flag == 1) | (dfCityProvince.province_flag == 3)) \
        .groupBy('seller_alliance_id','seller_company_id','oe_code','brand','sw_production_place', 'buyer_user_province_id') \
        .agg(sum('receive_qty').alias('buyer_province_qty'),
             sum('total_receive_price').alias('buyer_province_price')) \
        .withColumnRenamed('buyer_user_province_id', 'province_id')
    dfBuyerProvince = dfBuyerProvinceUser \
        .unionAll(dfBuyerProvinceCompany)\
        .withColumn('seller_province_qty',lit(0.0)) \
        .withColumn('seller_province_price',lit(0.0)) \
        .groupBy('seller_alliance_id','seller_company_id','oe_code','brand','sw_production_place', 'province_id') \
        .agg(sum('seller_province_qty').alias('seller_province_qty'),
             sum('seller_province_price').alias('seller_province_price'),
             sum('buyer_province_qty').alias('buyer_province_qty'),
             sum('buyer_province_price').alias('buyer_province_price'))
    #合并市级信息
    dfCity = dfSellerCity\
        .union(dfBuyerCity)\
        .groupBy('seller_alliance_id','seller_company_id','oe_code','brand','sw_production_place', 'city_id') \
        .agg(sum('seller_city_qty').alias('seller_city_qty'),
             sum('seller_city_price').alias('seller_city_price'),
             sum('buyer_city_qty').alias('buyer_city_qty'),
             sum('buyer_city_price').alias('buyer_city_price'))
    #合并省级信息
    dfProvince = dfSellerProvince \
        .union(dfBuyerProvince) \
        .groupBy('seller_alliance_id', 'seller_company_id', 'oe_code', 'brand', 'sw_production_place', 'province_id') \
        .agg(sum('seller_province_qty').alias('seller_province_qty'),
             sum('seller_province_price').alias('seller_province_price'),
             sum('buyer_province_qty').alias('buyer_province_qty'),
             sum('buyer_province_price').alias('buyer_province_price'))
    saveHiveDatabase('dm_city_qty_price',dfCity,hiveCol['dm_city_qty_price'],dateStrS)
    saveHiveDatabase('dm_province_qty_price', dfCity, hiveCol['dm_province_qty_price'], dateStrS)

"""
合并数据function
"""

"""
合并仅以联盟id为主键，无成员属性的数据
dfAllianceInfo = alliance_id          total_member  new_member  active_member_count  active_customer_count
                alliance_onshelve_sum  alliance_outshelve_sum
"""
def AllianceInfo(dfAlliance,dfMemberAll,dfMemberNew,dfActiveMember,dfAllianceActiveCustomer,dfAllianceOn, dfAllianceOut,dateStrS):
    #获取所有联盟(完整联盟信息）
    #dfAlliances = dfAlliance_id
    dfAlliances = dfAlliance\
        .select('alliance_id')\
        .distinct()
    #合并联盟的成员统计数据
    #dfAllianceMemberNew = dfAlliance_id  total_member  new_member
    dfAlliancesMemberAll = dfAlliances\
        .join(dfMemberAll,[dfMemberAll.alliance_id == dfAlliances.alliance_id],'left')\
        .drop(dfMemberAll.alliance_id)\
        .na.fill(0)
    dfAllianceMemberNew = dfAlliancesMemberAll\
        .join(dfMemberNew,[dfMemberNew.alliance_id == dfAlliancesMemberAll.alliance_id],'left')\
        .drop(dfMemberNew.alliance_id)\
        .na.fill(0)
    #合并活跃信息
    #dfAllianceActiveCustomers = dfAlliance_id  total_member  new_member  active_member_count  active_customer_count
    dfAllianceActiveMember = dfAllianceMemberNew\
        .join(dfActiveMember,[dfActiveMember.alliance_id == dfAllianceMemberNew.alliance_id],'left')\
        .drop(dfActiveMember.alliance_id)\
        .na.fill(0)
    dfAllianceActiveCustomers =dfAllianceActiveMember\
        .join(dfAllianceActiveCustomer,[dfAllianceActiveCustomer.alliance_id == dfAllianceActiveMember.alliance_id],'left')\
        .drop(dfAllianceActiveCustomer.alliance_id)\
        .na.fill(0)
    # 合并上下架信息
    # dfAllianceActiveCustomers = dfAlliance_id          total_member            new_member  active_member_count  active_customer_count
    #                             alliance_onshelve_sum  alliance_outshelve_sum
    dfAllianceOnShelve = dfAllianceActiveCustomers \
        .join(dfAllianceOn, [dfAllianceOn.alliance_id == dfAllianceActiveCustomers.alliance_id], 'left') \
        .drop(dfAllianceOn.alliance_id) \
        .na.fill(0)
    dfAllianceInfo = dfAllianceOnShelve \
        .join(dfAllianceOut, [dfAllianceOut.alliance_id == dfAllianceOnShelve.alliance_id],
              'left') \
        .drop(dfAllianceOut.alliance_id) \
        .na.fill(0)
    saveHiveDatabase('dm_alliance_basic_info', dfAllianceInfo, hiveCol['dm_alliance_basic_info'], dateStrS)

"""
合并以联盟id、成员id为主键的数据
dfAllianceMemberInfo = alliance_id   member_id              total_customer new_customer active_customer_count 
                       customer_num  alliance_member_sale_qty
"""
def AllianceMemberInfo(dfMember,dfCustomerAll,dfCustomerNew,dfMemberActiveCustomer,dfOrderCustomerNum,dfCompanies,dfAllianceMemberSaleQty,dateStrS):
    #获取全量联盟，成员id信息
    #dfAllianceMember = alliance_id  member_id
    dfAllianceMember = dfMember\
        .select('alliance_id','member_id')\
        .distinct()
    # 合并联盟成员的客户信息
    # dfAllianceMemberCustomerAll = alliance_id  member_id  total_customer  new_customer
    dfAllianceMemberCustomerAll = dfAllianceMember\
        .join(dfCustomerAll,[dfAllianceMember.alliance_id == dfCustomerAll.alliance_id,
                             dfAllianceMember.member_id == dfCustomerAll.member_id],'left')\
        .select(dfAllianceMember.alliance_id,dfAllianceMember.member_id,'total_customer')\
        .na.fill(0)
    dfAllianceMemberCustomerNew = dfAllianceMemberCustomerAll \
        .join(dfCustomerNew, [dfAllianceMemberCustomerAll.alliance_id == dfCustomerNew.alliance_id,
                              dfAllianceMemberCustomerAll.member_id == dfCustomerNew.member_id], 'left') \
        .select(dfAllianceMemberCustomerAll.alliance_id,dfAllianceMemberCustomerAll.member_id,'total_customer','new_customer')\
        .na.fill(0)
    #合并联盟活跃客户数据
    dfallianceMemberActiveCustomer = dfAllianceMemberCustomerNew\
        .join(dfMemberActiveCustomer, [dfAllianceMemberCustomerNew.alliance_id == dfMemberActiveCustomer.alliance_id,
                                       dfAllianceMemberCustomerNew.member_id == dfMemberActiveCustomer.company_id], 'left') \
        .select(dfAllianceMemberCustomerNew.alliance_id,dfAllianceMemberCustomerNew.member_id,'total_customer','new_customer','active_customer_count')\
        .na.fill(0)
    #合并联盟成员客户下单量
    dfAllianceMemberCustomerNum = dfallianceMemberActiveCustomer \
        .join(dfOrderCustomerNum, [dfallianceMemberActiveCustomer.alliance_id == dfOrderCustomerNum.seller_alliance_id,
                                       dfallianceMemberActiveCustomer.member_id == dfOrderCustomerNum.seller_company_id],'left') \
        .select(dfallianceMemberActiveCustomer.alliance_id, dfallianceMemberActiveCustomer.member_id, 'total_customer','new_customer', 'active_customer_count','customer_num') \
        .na.fill(0)
    #成员名
    dfAllianceMemberName = dfAllianceMemberCustomerNum \
        .join(dfCompanies, [dfCompanies.company_id == dfAllianceMemberCustomerNum.member_id], 'left') \
        .drop(dfCompanies.company_id) \
        .drop(dfCompanies.company_type) \
        .drop(dfCompanies.area_id) \
        .withColumnRenamed('company_name', 'member_name')
    #合并成员销量
    dfAllianceMemberName = dfAllianceMemberName\
        .join(dfAllianceMemberSaleQty,[dfAllianceMemberName.member_id == dfAllianceMemberSaleQty.seller_company_id,
                                       dfAllianceMemberName.alliance_id == dfAllianceMemberSaleQty.seller_alliance_id],'left')\
        .drop(dfAllianceMemberSaleQty.seller_company_id)\
        .drop(dfAllianceMemberSaleQty.seller_alliance_id)
    dfAllianceMemberInfo = dfAllianceMemberName
    saveHiveDatabase('dm_alliance_member_base_info', dfAllianceMemberInfo, hiveCol['dm_alliance_member_base_info'], dateStrS)

"""
统计，
统计在售商品数及库存   商品信息概览（联盟维度） 图1（及下钻），图2：覆盖分类数，在售商品数
dfOnSaleStockPrice = alliance_id          alliance_part_id  oe_code       brand  sku_name
                     sw_production_place  onsele_num        stock_price
"""
def getOnSaleNumStockPrice(dfAllianceCompanySku,dateStrS):
    # 获取库存信息S
    #dfCompanyStockInfo = company_id property alliance_part_id veh_brand  sw_brand
    #                     stocks_qty
    hql = "select company_id,property,alliance_part_id,veh_brand,sw_brand,qty stocks_qty,day" \
          " from orange.ods_psql_company_stocks" \
          " where day <= '" + dateStrS + "'"
    dfCompanyStocksInfo= exeHql(hql)
    mapCompanyStocks = dfCompanyStocksInfo\
        .rdd\
        .map(lambda p: ((p.company_id,p.property,p.alliance_part_id,p.veh_brand,p.sw_brand), [(p.stocks_qty, p.day)])) \
        .reduceByKey(aggWithKey) \
        .map(lambda x: (x[0][0],x[0][1],x[0][2],x[0][3],x[0][4], x[1])) \
        .map(mapCompanyStocksInfo)
    dfCompanyStocksInfo = sqlContext.createDataFrame(mapCompanyStocks,['company_id','property','alliance_part_id','veh_brand','sw_brand','stocks_qty'])
    #获取联盟商品库存
    #dfAllianceSkuStocks = company_skus_id  company_id   oe_code              brand              veh_brand
    #                      sw_brand         sku_name     sw_production_place  alliance_part_id   property
    #                      price_retail     alliance_id  stocks_qty
    dfAllianceSkuStocks = dfAllianceCompanySku \
        .join(dfCompanyStocksInfo,[dfCompanyStocksInfo.company_id == dfAllianceCompanySku.company_id,
                                   dfCompanyStocksInfo.property == dfAllianceCompanySku.property,
                                   dfCompanyStocksInfo.alliance_part_id == dfAllianceCompanySku.alliance_part_id],'left')\
        .select('company_skus_id',dfAllianceCompanySku.company_id,'oe_code','brand',dfAllianceCompanySku.veh_brand,
                dfAllianceCompanySku.sw_brand,'sku_name','sw_production_place',dfAllianceCompanySku.alliance_part_id,dfAllianceCompanySku.property,
                'price_retail','alliance_id','stocks_qty')
    #在售商品数
    #dfOnSaleNumInfo = alliance_id          alliance_part_id  oe_code  brand  sku_name
    #                  sw_production_place  onsale_num
    dfOnSaleNumInfo = dfAllianceSkuStocks\
        .where(dfAllianceSkuStocks.stocks_qty > 0)\
        .select('alliance_id','company_id','alliance_part_id','oe_code','brand','sku_name','sw_production_place')\
        .distinct()\
        .groupBy('alliance_id','company_id','oe_code','brand','sku_name','sw_production_place')\
        .agg(count('alliance_part_id').alias('onsale_num'))
    #获取配件库存金额
    # dfOnSaleStockPrice = alliance_id          alliance_part_id  oe_code  brand  sku_name
    #                      sw_production_place  stock_price
    dfOnSaleStockPrice = dfAllianceSkuStocks\
        .withColumn('stock_price',dfAllianceSkuStocks.price_retail*dfAllianceSkuStocks.stocks_qty)\
        .groupBy('alliance_id','company_id','oe_code','brand','sku_name','sw_production_place') \
        .agg(sum('stock_price').alias('stock_price'))\
        .where(col('stock_price') > 0)
    saveHiveDatabase('dm_on_sale_part_num',dfOnSaleNumInfo,hiveCol['dm_on_sale_part_num'],dateStrS)
    saveHiveDatabase('dm_on_sale_stockprice',dfOnSaleStockPrice,hiveCol['dm_on_sale_stockprice'],dateStrS)

"""
调用位置：getOnSaleNumStockPrice()
作用：找出查询次数最多的商品
return: company_id   property   alliance_part_id  veh_brand sw_brand stocks_qty
"""
def mapCompanyStocksInfo(p):
    company_id = p[0]
    property = p[1]
    alliance_part_id = p[2]
    veh_brand = p[3]
    sw_brand = p[4]
    stocks_qty_list = p[5]
    max_day = ""
    for item in stocks_qty_list:
        if max_day < item[1]:
            stocks_qty = item[0]
            max_day = item[1]
    return  company_id,property,alliance_part_id,veh_brand,sw_brand,stocks_qty


#主函数
if __name__ == '__main__':
    print("main function")
    print("program begin to run at: " + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))
    PARAMS = None
    """
    默认开始及结束日期日期={Str}
    均为昨天，只跑昨天一天数据
    """
    defualt_date = date_str(datetime.date.today())
    parser = argparse.ArgumentParser()
    parser.add_argument('--startdate', type=str, default=defualt_date,
                        help='start date')
    parser.add_argument('--enddate', type=str, default=defualt_date,
                        help='end date')
    PARAMS, _ = parser.parse_known_args()
    # 定义昨天为开始日期
    today = PARAMS.startdate
    # 当开始日期<=结束日期时，运行该程序。如果开始日期=结束日期，break种植
    while (today <= PARAMS.enddate):
        print("start preparing " + today + " datas")
        yesterday = date_str(str_date(today) - datetime.timedelta(days=1))
        """
        根据查询对应表格获取相应信息
        """
        #获取成员数据信息
        dfMember,dfAlliance,dfCustomer,dfAllianceCustomer,dfStaffs,dfCompanies,dfUsers,dfAreas = getBasicInfo(yesterday)
        """
        处理数据
        """
        # 获取公司配件信息
        #dfAllianceCompanySku = getCompanySku(dfAlliance, dfMember,yesterday)
        dfAllianceCompanySku = sqlContext.sql('select company_skus_id,company_id,oe_code,brand,veh_brand,'
                                              'sw_brand,sku_name,sw_production_place,alliance_part_id,property,'
                                              'price_retail,alliance_id'
                                              ' from orange_dm.dm_company_alliance_sku'
                                              ' where day = \'' + yesterday + '\'')
        # 处理意向查询数据
        #dfSkuValidCheckInfo = getSkuValidCheckInfo(dfStaffs,dfAllianceCompanySku,dfCompanies,yesterday)
        dfSkuValidCheckInfo = sqlContext.sql('select seller_company_id,sku_id,buyer_user_id,price,carts_qty_sum,'
                                             'checkcount,buyer_company_id,buyer_company_name,buyer_company_type,oe_code,'
                                             'brand,veh_brand,sw_brand,sku_name,sw_production_place,'
                                             'alliance_id,alliance_part_id'
                                             ' from orange_dm.dm_sku_valid_check'
                                             ' where day = \'' + yesterday + '\'')
        # 处理订单数据，增加买卖双方联盟id
        #dfOrdersDetailsInfo,dfOrdersDetailsInfoAll = getOrderDetailsInfo(dfAllianceCustomer, dfAllianceCompanySku,dfCompanies,dfUsers, yesterday)
        dfOrdersDetailsInfo = sqlContext.sql('select * from orange_dm.dm_orders_details_info where day = \'' + yesterday + '\'')
        dfOrdersDetailsInfoAll = sqlContext.sql('select * from orange_dm.dm_orders_details_info_all where day = \'' + yesterday + '\'')
        # 处理订单数据，获取卖方及买方所属区域
        #dfCityProvince =getOrderCityProvinceInfo(dfOrdersDetailsInfo,dfAreas,yesterday)
        dfCityProvince = sqlContext.sql('select order_detail_id,out_qty,receive_qty,total_out_price,total_receive_price,seller_alliance_id,seller_company_id,seller_company_area_id,buyer_user_area_id,buyer_company_area_id,oe_code,name,brand,sw_production_place,sku_id,property,seller_company_city_id,seller_company_province_id,buyer_company_city_id,buyer_company_province_id,buyer_user_city_id,buyer_user_province_id,city_flag,province_flag '
                                        ' from orange_dm.dm_city_provice_qty_price'
                                        ' where day = \'' + yesterday + '\'')
        """
        统计数据
        """
        # 统计成交数量分布   经营指标概览（联盟维度）    图6，7
        #getOrderAreaQtyPrice(dfCityProvince,yesterday)
        # 统计配件意向查询量及销量  经营指标概览（联盟维度）  图2，3，4 查销比例排行，意向查询量排行，销量排行
        #getPartsQtyCheck(dfOrdersDetailsInfo, dfSkuValidCheckInfo,yesterday)
        # 统计经营指标            经营指标概览： （联盟维度）  图5（下钻）每日采购量+金额（总和，分类）
        #getPurchaseInfo(dfOrdersDetailsInfoAll,yesterday)
        # 统计价格分析     商品信息概览（联盟维度）    图5（下钻）
        #getPriceAnalysis(dfOrdersDetailsInfo,yesterday)
        #统计在售商品数及库存   商品信息概览（联盟维度） 图1（及下钻），图2：覆盖分类数，在售商品数
        #getOnSaleNumStockPrice(dfAllianceCompanySku, yesterday)
        # 统计联盟总成员及新成员     客户与成员数据概览：（联盟维度） 图1 总成员量，图2 新增成员量
        dfMemberAll, dfMemberNew = getMemberAllNew(dfMember, yesterday, today)
        # 统计总客户及新增客户       客户与成员数据概览： 图1 总客户量，图2 新增客户量
        dfCustomerAll, dfCustomerNew = getCustomerAllNew(dfCustomer, dfAllianceCustomer, dfMember, yesterday, today)
        # 统计活跃客户及活跃成员      客户与成员数据概览： 图3 活跃客户及活跃成员量
        dfActiveMember,dfAllianceActiveCustomer,dfMemberActiveCustomer = getActiveMemberCustomer(dfMember, dfAllianceCustomer, dfStaffs, dfCustomer, yesterday)
        # 统计上下架商品数          商品信息概览:  图3 上下架商品量
        dfAllianceOn, dfAllianceOut = getoutonShelve(yesterday)
        # 统计下单客户量            客户与成员数据概览   图4 下单客户量
        dfOrderCustomerNum = getOrderCustomerNum(dfMember,dfAllianceCustomer,yesterday,today)
        # 成员销售排行数据          客户与成员数据概览：（联盟维度/配件商） 图6 配件商排行榜
        dfAllianceMemberSaleQty = getAllianceMemberSaleQty(dfOrdersDetailsInfo)
        # 统计修理厂查询数据        客户与成员数据概览: (联盟维度）  图5 修理厂查询排行
        getGarageInfo(dfSkuValidCheckInfo, dfCompanies, yesterday)
        """
        合并数据
        """
        # 合并仅以联盟id为主键，无成员属性的数据
        AllianceInfo(dfAlliance,dfMemberAll,dfMemberNew,dfActiveMember,dfAllianceActiveCustomer,dfAllianceOn,dfAllianceOut,yesterday)
        # 合并以联盟id、成员id为主键的数据
        AllianceMemberInfo(dfMember,dfCustomerAll,dfCustomerNew,dfMemberActiveCustomer,dfOrderCustomerNum,dfCompanies,dfAllianceMemberSaleQty,yesterday)



        if today == PARAMS.enddate:
            break
        print("read " + today + " datas finished")
        today = date_str(str_date(today) + datetime.timedelta(days=1))
        print("")
        print("")
    sc.stop()
    print("program is done at: "  + datetime.datetime.now().strftime('%b-%d-%y %H:%M:%S'))