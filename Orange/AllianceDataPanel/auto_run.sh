#!/bin/sh
FILE=$1
START_Month="--startdate 20190411"
END_Month="--enddate 20190411"


DATE=`date +%Y%m%d`
echo ${DATE}

FILE_HOME="./"
echo "`date`----------Run pyspark app ${FILE}"


command="spark-submit --conf spark.blacklist.enabled=false --jars /home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-common.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-client.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/lib/hbase/hbase-server.jar,/home/opt/cloudera/parcels/CDH-6.1.1-1.cdh6.1.1.p0.875250/jars/spark-examples_2.11-2.4.0-cdh6.1.1.jar  --driver-cores 16 --driver-memory 20480M --num-executors 40 --executor-cores 16 --executor-memory 20480M"


command+=" "${FILE_HOME}${FILE}" "${START_Month}" "${END_Month}


echo "$command"
eval $command